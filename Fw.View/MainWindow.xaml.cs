﻿using System.Windows;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Base;
using Fw.Model.Master.Item;
using MahApps.Metro.Controls;
using Fw.View.FwTreeView.ViewModels;
using System.Windows.Controls;
using Fw.View.HelperViews;
using Fw.View.Master.Item;
using Fw.View.Configuration;
using Fw.View.Master.Contact;
using MahApps.Metro;
using Fw.View.Master.Finance;
using Fw.View.Transaction.Purchase;

namespace Fw.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [System.ComponentModel.DesignerCategory("Form")]
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            DataContext = GetSettingViewModel();
        }

        public MainWindow(int id)
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private MetroTabItem NewItem(string headerName, ContentControl content)
        {
            var tab = new MetroTabItem();

            tab.Header = headerName;
            tab.CloseButtonEnabled = true;
            tab.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
            tab.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
            tab.Content = content;

            return tab;
        }

        private SettingsViewModel GetSettingViewModel()
        {
            string itemQuery = "select IM.Id as Id, IM.Code as 'Item Code', IM.Name as 'Item Name', IM.CreationDate as 'Creation Date', IM.Packing as Packing, IM.Unit as Unit from ItemMaster as IM";
            string genericQuery = "select * from GenericMaster";
            string picklistQuery = "select * from PickListMaster";
            string categoryQuery = "select * from CategoryMaster";
            string taxTypeQuery = "select * from TaxTypeMaster";
            string companyQuery = "select * from CompanyMaster";
            string contactMaster = "select * from ContactMaster";
            string doctorQuery = "select * from DoctorMaster";

            var n1 = new TreeNodeViewModel("Sale");

            var n2 = new TreeNodeViewModel("Purchase", new[]
                {
                    new TreeNodeViewModel("Purchase Invoice",typeof(CatalogView),
                    typeof(PurchaseInvoiceView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items,itemQuery, "All Purchase Invoice"),
                });

            var n3 = new TreeNodeViewModel("Inventory");
            var n4 = new TreeNodeViewModel("Finance");
            var n5 = new TreeNodeViewModel("Administrator", new[] 
            {
                new TreeNodeViewModel("Foundation", new [] 
                {
                    new TreeNodeViewModel("Item", new [] 
                    {
                        new TreeNodeViewModel("Item Master",typeof(CatalogView),
                        typeof(ItemMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items,itemQuery, "All Item"),

                        new TreeNodeViewModel("Generic Master",typeof(CatalogView),
                        typeof(GenericMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items,genericQuery, "All Generic"),

                        new TreeNodeViewModel("Category Master",typeof(CatalogView),
                        typeof(CategoryMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, categoryQuery, "All Category"),

                        new TreeNodeViewModel("Tax Type Master",typeof(CatalogView),
                        typeof(TaxTypeMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, taxTypeQuery, "All Tax Type" ),
                    }),
                    new TreeNodeViewModel("Contact", new [] 
                    {
                        new TreeNodeViewModel("Contact Master",typeof(CatalogView),
                        typeof(ContactMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, contactMaster, "All Contact"),

                        new TreeNodeViewModel("Company Master",typeof(CatalogView),
                        typeof(CompanyMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, companyQuery, "All Company"),

                        new TreeNodeViewModel("Doctor Master",typeof(CatalogView),
                        typeof(DoctorMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, doctorQuery, "All Doctor"),
                    }),
                    new TreeNodeViewModel("Finance", new [] 
                    {
                        new TreeNodeViewModel("Ledger Master",typeof(CatalogView),
                        typeof(LedgerMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, contactMaster, "All Ledger"),

                        new TreeNodeViewModel("Ledger Group",typeof(CatalogView),
                        typeof(LedgerGroupView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, companyQuery, "All Ledger Group"),
                    })
                }),

                new TreeNodeViewModel("Configuration", new [] {
                    new TreeNodeViewModel("PickList Master",typeof(CatalogView),
                        typeof(PickListMasterView),typeof(HelperViews.MasterWindow), tabMultiWindow.Items, picklistQuery, "All PickList"),

                    }),
             });

            return new SettingsViewModel(new[] { n1, n2, n3, n4, n5 });
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            //ThemeManager.ChangeAppStyle(Application.Current, new Accent(), new AppTheme());
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {

        }

        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {

        }
    }
}
