﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fw.Controller.EntityCommand;
using Fw.Controller.Extentions;
using Fw.View.Interfaces;

namespace Fw.View.HelperViews
{
    /// <summary>
    /// Interaction logic for CatalogView.xaml
    /// </summary>
    public partial class CatalogView : UserControl
    {
        //private SimpleCommand<
        ICommand _NewEntityCommand;
        ICommand _RefreshCommand;
        ICommand _CloseCommand;
        ICommand _EditEntityCommand;

        public ICommand NewEntityCommand
        {
            get
            {
                if (null == _NewEntityCommand)
                    _NewEntityCommand = new RelayCommand<object>(NewEntity);

                return _NewEntityCommand;
            }
        }

        public ICommand RefreshCommand
        {
            get
            {
                if (null == _RefreshCommand)
                    _RefreshCommand = new RelayCommand<object>(Refresh);

                return _RefreshCommand;
            }
        }

        public ICommand CloseCommand
        {
            get
            {
                if (null == _CloseCommand)
                    _CloseCommand = new RelayCommand<object>(CloseWindow);

                return _CloseCommand;
            }
        }

        public ICommand EditEntityCommand
        {
            get
            {
                if (null == _EditEntityCommand)
                    _EditEntityCommand = new RelayCommand<object>(EditEntity, CanEditEntity);

                return _EditEntityCommand;
            }
        }

        public string SqlQuery { get; private set; }

        public Type ContentType { get; private set; }

        public Type EntityContentType { get; private set; }

        public CatalogView(string sqlQuery, Type entityContentType, Type contentType)
        {
            InitializeComponent();

            SqlQuery = sqlQuery;
            ContentType = contentType;
            
            EntityContentType = entityContentType;

            DataContext = this;

            Refresh(null);
        }

        private void NewEntity(object notUsed)
        {
            var entityContainer = Activator.CreateInstance(EntityContentType);

            if (entityContainer is IEntityContainer)
            {
                var window = Activator.CreateInstance(ContentType, (entityContainer as IEntityContainer).WindowName, entityContainer, (entityContainer as IEntityContainer).Entity);

                if (window is Window)
                    (window as Window).Show();
            }
        }

        private void Refresh(object notUsed)
        {
            dgvCatelogView.DataContext = (new Fw.Controller.Data.MySqlConnectionManager()).Fill(SqlQuery).DefaultView;
        }

        private void CloseWindow(object notUsed)
        {
            if (Parent is MahApps.Metro.Controls.MetroTabItem)
                (Parent as MahApps.Metro.Controls.MetroTabItem).Close();
        }

        private void EditEntity(object notUsed)
        {
            var a = (DataRowView)(dgvCatelogView.SelectedItem);
            var id = (int)a.Row["Id"];

            var entityContainer = Activator.CreateInstance(EntityContentType, id);

            if (entityContainer is IEntityContainer)
            {
                var window = Activator.CreateInstance(ContentType, (entityContainer as IEntityContainer).WindowName, entityContainer, (entityContainer as IEntityContainer).Entity);

                if (window is Window)
                    (window as Window).Show();
            }
        }

        private bool CanEditEntity(object notUsed)
        {
            if (dgvCatelogView.IsNull() || dgvCatelogView.Items.IsNull() || dgvCatelogView.SelectedItem.IsNull()) return false;

            return true;
        }
    }
}
