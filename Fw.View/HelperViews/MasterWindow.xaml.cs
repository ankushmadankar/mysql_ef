﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Base;
using Fw.Controller.EntityCommand;

namespace Fw.View.HelperViews
{
    /// <summary>
    /// Interaction logic for MasterWindow.xaml
    /// </summary>
    public partial class MasterWindow : MetroWindow
    {
        private readonly EntityObject _Entity;
        private readonly ContentControl _EntityUserControl;
        private ICommand _NewEntityCommand;
       
        private EntityObject Entity { get { return _Entity; } }
        private ContentControl EntityUserControl { get { return _EntityUserControl; } }

        private ICommand NewEntityCommand { get { return _NewEntityCommand == null ? (_NewEntityCommand = new RelayCommand<object>(NewEntity)) : _NewEntityCommand; } }

        public MasterWindow(string windowName, ContentControl entityWindow, EntityObject entity)
        {
            InitializeComponent();
            //
            _Entity = entity;
            //
            _EntityUserControl = entityWindow;
            //
            Title = windowName;
            //
            BindCommand();
            //
            mainContentControl.Content = entityWindow;
        }

        private void BindCommand()
        {
            if (Entity == null || Entity.EntityManager == null) return;

            btnNew.Command = NewEntityCommand;
            btnSave.Command = Entity.EntityManager.SaveEntityCommand;
            btnDelete.Command = Entity.EntityManager.DeleteEntityCommand;
            btnRefresh.Command = Entity.EntityManager.RefreshEntityCommand;
            btnClose.Command = Entity.EntityManager.CloseEntityCommand;
        }

        private void NewEntity(object notUsed)
        {
            var entityControl = Activator.CreateInstance(EntityUserControl.GetType()) as UserControl;
            var entityObject = Activator.CreateInstance(Entity.GetType(), new EntityManager(entityControl)) as EntityObject;
            var masterWindow = Activator.CreateInstance(this.GetType(), Title, entityControl, entityObject) as Window;

            masterWindow.Show();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
