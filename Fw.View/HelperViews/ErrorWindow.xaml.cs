﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MahApps.Metro.Controls;

namespace Fw.View.HelperViews
{
    /// <summary>
    /// Interaction logic for ErrorWindow.xaml
    /// </summary>
    public partial class ErrorWindow : MetroWindow
    {
        private readonly string _ErrorMsg;

        public ErrorWindow()
        {
            InitializeComponent();
        }

        public ErrorWindow(string errorMsg)
        {
            InitializeComponent();
            _ErrorMsg = errorMsg;
        }

        private void ErrorWindow_Load(object sender, EventArgs e)
        {
            txtErrorMsg.Text = _ErrorMsg;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void btnCopy_Click(object sender, RoutedEventArgs e)
        {
            Clipboard.Clear();
            Clipboard.SetText(_ErrorMsg);
        }
    }
}
