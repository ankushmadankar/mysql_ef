﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;

namespace Fw.View.Interfaces
{
    public interface IEntityContainer
    {
        string WindowName { get; }
        EntityObject Entity { get; }
    }
}
