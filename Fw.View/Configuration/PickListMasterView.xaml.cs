﻿using System.Windows;
using System.Windows.Controls;
using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Model.Configuration;
using Fw.View.Interfaces;

namespace Fw.View.Configuration
{
    /// <summary>
    /// Interaction logic for PickListMasterView.xaml
    /// </summary>
    public partial class PickListMasterView : UserControl , IEntityContainer
    {
        private PickListMaster _Entity;
        private EntityManager _EntityManager;

        public string WindowName
        {
            get { return "Pick list master"; }
        }

        public EntityObject Entity
        {
            get
            {
                return (_Entity ?? (_Entity = new PickListMaster(_EntityManager)));
            }
        }

        public PickListMasterView()
        {
            InitializeComponent();

            _EntityManager = new EntityManager(this);
            //
            _Entity = new PickListMaster(_EntityManager);
        }

        public PickListMasterView(int id)
        {
            InitializeComponent();

            _EntityManager = new EntityManager(this);
            //
            _Entity = new PickListMaster(_EntityManager, id);
        }

        private void PicklistMasterView_Loaded(object sender, RoutedEventArgs e)
        {
            dgvPickListValues.DataContext = _Entity.Lines;
            //
            dgvPickListValues.ItemsSource = _Entity.Lines;
        }
    }
}
