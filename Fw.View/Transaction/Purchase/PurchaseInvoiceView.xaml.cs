﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Item;
using Fw.View.Interfaces;

namespace Fw.View.Transaction.Purchase
{
    /// <summary>
    /// Interaction logic for PurchaseInvoiceView.xaml
    /// </summary>
    public partial class PurchaseInvoiceView : UserControl, IEntityContainer
    {
        ItemMaster _Entity;

        public PurchaseInvoiceView()
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ItemMaster(manger);
        }

        public PurchaseInvoiceView(int id)
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ItemMaster(manger, id);
        }

        public string WindowName
        {
            get
            {
                return "Purchae Invoice";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }
    }
}
