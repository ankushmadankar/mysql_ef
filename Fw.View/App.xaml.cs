﻿using System;
using System.Windows;

namespace Fw.View
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            if (e.Exception is Fw.Controller.Base.ValidationError)
            {
                e.Handled = true;
                return;
            }

            Fw.Controller.Extentions.ApplicationErrorLogger.LogApplicationError(e.Exception);
            
            new HelperViews.ErrorWindow(e.Exception.ToString()).ShowDialog();

            e.Handled = true;
        }

        private void Application_Startup(object sender, StartupEventArgs e)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
        }

        void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            if (e.ExceptionObject is Fw.Controller.Base.ValidationError) return;

            Exception ex = e.ExceptionObject as Exception;

            Fw.Controller.Extentions.ApplicationErrorLogger.LogApplicationError(ex);

            new HelperViews.ErrorWindow(ex.ToString()).ShowDialog();
        }
    }
}
