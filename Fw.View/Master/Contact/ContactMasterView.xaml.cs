﻿using System.Windows.Controls;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Contact;
using Fw.View.Interfaces;

namespace Fw.View.Master.Contact
{
    /// <summary>
    /// Interaction logic for ContactMasterView.xaml
    /// </summary>
    public partial class ContactMasterView : UserControl, IEntityContainer
    {
        ContactMaster _Entity;

        public ContactMasterView()
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ContactMaster(manger);
        }

        public ContactMasterView(int id)
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ContactMaster(manger, id);
        }

        public string WindowName
        {
            get
            {
                return "Contact Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }
    }
}
