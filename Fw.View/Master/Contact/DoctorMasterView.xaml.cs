﻿using System.Windows.Controls;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Contact;
using Fw.View.Interfaces;

namespace Fw.View.Master.Contact
{
    /// <summary>
    /// Interaction logic for DoctorMasterView.xaml
    /// </summary>
    public partial class DoctorMasterView : UserControl, IEntityContainer
    {
        ContactMaster _Entity;

        public DoctorMasterView()
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ContactMaster(manger, ContactGroupType.Doctor);
        }

        public DoctorMasterView(int id)
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ContactMaster(manger, id, ContactGroupType.Doctor);
        }

        public string WindowName
        {
            get
            {
                return "Doctor Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }
    }
}
