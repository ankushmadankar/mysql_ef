﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Contact;
using Fw.View.Interfaces;

namespace Fw.View.Master.Contact
{
    /// <summary>
    /// Interaction logic for CompanyMasterView.xaml
    /// </summary>
    public partial class CompanyMasterView : UserControl, IEntityContainer
    {
        ContactMaster _Entity;

        public CompanyMasterView()
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ContactMaster(manger, ContactGroupType.Party);
        }

        public CompanyMasterView(int id)
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ContactMaster(manger, id, ContactGroupType.Party);
        }

        public string WindowName
        {
            get
            {
                return "Party Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }
    }
}
