﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Microsoft.Windows.Controls.Ribbon;
using MahApps.Metro.Controls;
using System.Collections;

namespace Fw.View.Master
{
    /// <summary>
    /// Interaction logic for RibbonWindow1.xaml
    /// </summary>
    public partial class RibbonWindow1 : MetroWindow
    {
        public RibbonWindow1()
        {
            InitializeComponent();
            control();
            // Insert code required on object creation below this point.
        }

        private void control()
        {
            dgvSuppliers.DataContext = (new Fw.Controller.Data.MySqlConnectionManager()).Fill("select Code,Name,MobileNo from ContactMaster").DefaultView;

            //dgvTaxConf.ItemsSource = (new Fw.Controller.Data.MySqlConnectionManager()).Fill("select * from ItemMaster").Rows.Cast<System.Data.DataRow>().ToList();
        }
    }

    public class MultiColumnResultsProvider //: FeserWard.Controls.IIntelliboxResultsProvider
    {
        private List<Person> _results;

        private void ConstructResultSet()
        {
            _results = Enumerable.Range(0, 30).Select(i =>
            {
                var r = new Random(i);

                return new Person()
                {
                    PersonID = i,
                    FirstName = r.Next().ToString() + "'s firstname",
                    LastName = r.Next().ToString() + "'s lastname",
                    Age = r.Next(10, 200),
                    NetWorth = r.Next(10000, 10000000),
                    Weight = r.Next(100, 400)
                };
            }).ToList();
        }


        public IEnumerable DoSearch(string searchTerm, int maxResults, object tag)
        {
            ConstructResultSet();
            return _results;
        }

        private class Person
        {
            public int PersonID
            {
                get;
                set;
            }

            public string FirstName
            {
                get;
                set;
            }

            public string LastName
            {
                get;
                set;
            }

            public int Age
            {
                get;
                set;
            }

            public decimal NetWorth
            {
                get;
                set;
            }

            public int Weight
            {
                get;
                set;
            }

            public override string ToString()
            {
                return string.Format("ID:{0}, FName:{1}, LName:{2}, Age:{3}, NetWorth:{4}, Weight:{5}",
                    PersonID,
                    FirstName ?? string.Empty,
                    LastName ?? string.Empty,
                    Age,
                    NetWorth,
                    Weight);
            }
        }

    }
}
