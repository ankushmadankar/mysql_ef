﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fw.View.account
{
    /// <summary>
    /// Interaction logic for account5_cr5.xaml
    /// </summary>
    public partial class account5_cr5 : Window
    {
        public List<tax1> pur1 = new List<tax1>();
        public account5_cr5()
        {
            InitializeComponent();
            dgpur1.Columns.Clear();
            tax1 p1 = new tax1
            {
                Tax_Rate = "5%",
                Gross_Amount = "",
                Taxable_amt = "",
                Tax = ""
            };
            pur1.Add(p1);
            tax1 p2 = new tax1
            {
                Tax_Rate = "12.5%",
                Gross_Amount = "",
                Taxable_amt = "",
                Tax = ""
            };
            pur1.Add(p2);
            dgpur1.ItemsSource = pur1;

        }
    }
    public class tax1
    {
        public string Tax_Rate { get; set; }
        public string Gross_Amount { get; set; }
        public string Taxable_amt { get; set; }
        public string Tax { get; set; }

    }
}
