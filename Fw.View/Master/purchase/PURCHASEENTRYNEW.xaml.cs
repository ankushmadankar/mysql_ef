﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fw.View
{
    /// <summary>
    /// Interaction logic for PURCHASEENTRYNEW.xaml
    /// </summary>
    public partial class PURCHASEENTRYNEW : Window
    {
        public List<purchaseentry> pur = new List<purchaseentry>();
        public List<purchaseentry1> pur1 = new List<purchaseentry1>();
        public PURCHASEENTRYNEW()
        {
            InitializeComponent();
            for (int i = 1; i <= 9; i++)
            {
                purchaseentry p = new purchaseentry
                {
                    srno = ""
                };
                pur.Add(p);
                dgpur.ItemsSource = pur;
               
             //   dgpur1.Columns.Remove(dgpur1.Columns[0]);
                //adding row to second grid
               

            }
            dgpur1.Columns.Clear();
            purchaseentry1 p1 = new purchaseentry1
            {
                Tax_Rate = "5%",
                Gross_Amount = "",
                Taxable_amt = "",
                Tax = ""
            };
            pur1.Add(p1);
            purchaseentry1 p2 = new purchaseentry1
            {
                Tax_Rate = "12.5%",
                Gross_Amount = "",
                Taxable_amt = "",
                Tax = ""
            };
            pur1.Add(p2);
            dgpur1.ItemsSource = pur1;
        }
    }
    public class purchaseentry
    {
        public string srno { get; set; }
        
    }
    public class purchaseentry1
    {
        public string Tax_Rate { get; set; }
        public string Gross_Amount { get; set; }
        public string Taxable_amt { get; set; }
        public string Tax { get; set; }

    }


}
