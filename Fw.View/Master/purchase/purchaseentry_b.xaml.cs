﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fw.View
{
    /// <summary>
    /// Interaction logic for purchaseentry_b.xaml
    /// </summary>
    public partial class purchaseentry_b : Window
    {
        public List<purchaseentry_b1> pur = new List<purchaseentry_b1>();
        public purchaseentry_b()
        {
            InitializeComponent();
           // dgpur.Columns.Clear();
            for (int i = 1; i <= 3; i++)
            {
                purchaseentry_b1 p = new purchaseentry_b1
                {
                    product_code = "",
                    product_name = "",
                    packing = "",
                    company = "",
                    stock = "",
                    unit = ""
                };
                pur.Add(p);
            }
                dgpur.ItemsSource = pur;
           
        }
        public class purchaseentry_b1
        {
            public string product_code { get; set; }
            public string product_name { get; set; }
            public string packing { get; set; }
            public string company { get; set; }

            public string stock { get; set; }
            public string unit { get; set; }

        }
    }
}
