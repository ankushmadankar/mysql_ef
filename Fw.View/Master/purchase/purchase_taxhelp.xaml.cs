﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fw.View
{
    /// <summary>
    /// Interaction logic for purchase_taxhelp.xaml
    /// </summary>
    public partial class purchase_taxhelp : Window
    {
        public List<purchaseentrytax> pur = new List<purchaseentrytax>();
        public purchase_taxhelp()
        {
            InitializeComponent();
            dgpur.Columns.Clear();
            for (int i = 1; i <= 8; i++)
            {
                purchaseentrytax p = new purchaseentrytax
                {
                    name = "CST 5%"
                };
                pur.Add(p);
                dgpur.ItemsSource = pur;

            }
        }
        public class purchaseentrytax
        {
            public string name { get; set; }

        }
    }
}