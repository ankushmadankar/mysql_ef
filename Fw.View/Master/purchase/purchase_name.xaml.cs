﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Fw.View.Master.purchase
{
    /// <summary>
    /// Interaction logic for purchase_name.xaml
    /// </summary>
    public partial class purchase_name : Window
    {
        public List<ledger1> pur = new List<ledger1>();
        public purchase_name()
        {
            InitializeComponent();
           // dgpur.Columns.Clear();
            for (int i = 1; i <= 4; i++)
            {
                ledger1 p1 = new ledger1
                {
                    acname = ""
                };
                pur.Add(p1);
            }
            dgpur.ItemsSource = pur;
        }
    }
    public class ledger1
    {
        public string acname { get; set; }
        

    }
}
