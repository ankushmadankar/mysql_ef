﻿using System.Windows.Controls;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Finance;
using Fw.Model.Master.Item;
using Fw.View.Interfaces;

namespace Fw.View.Master.Finance
{
    /// <summary>
    /// Interaction logic for UserMasterView.xaml
    /// </summary>
    public partial class LedgerMasterView : UserControl, IEntityContainer
    {
        LedgerMaster _Entity;

        public LedgerMasterView()
        {
            InitializeComponent();

            //EntityManager manger = new EntityManager(this);

            //_Entity = new LedgerMaster(manger);
        }

        public LedgerMasterView(int id)
        {
            InitializeComponent();

            //EntityManager manger = new EntityManager(this);

            //_Entity = new LedgerMaster(manger, id);
        }

        public string WindowName
        {
            get
            {
                return "Ledger Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }
    }
}
