﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Finance;
using Fw.View.Interfaces;

namespace Fw.View.Master.Finance
{
    /// <summary>
    /// Interaction logic for LedgerGroupView.xaml
    /// </summary>
    public partial class LedgerGroupView : UserControl, IEntityContainer
    {
        LedgerGroupMaster _Entity;

        public LedgerGroupView()
        {
            InitializeComponent();

            //EntityManager manger = new EntityManager(this);

            //_Entity = new LedgerGroupMaster(manger);
        }

        public LedgerGroupView(int id)
        {
            InitializeComponent();

            //EntityManager manger = new EntityManager(this);

            //_Entity = new LedgerGroupMaster(manger, id);
        }

        public string WindowName
        {
            get
            {
                return "Ledger Group Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }
    }
}
