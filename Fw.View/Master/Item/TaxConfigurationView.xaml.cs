﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Fw.Model.Master.Item;
using Fw.Controller.DocumentEntity;

namespace Fw.View.Master.Item
{
    /// <summary>
    /// Interaction logic for TaxConfigurationView.xaml
    /// </summary>
    public partial class TaxConfigurationView
    {
        private TaxConfiguration _EntityInternal;
        private EntityManager _EntityManagerInternal;

        private TaxConfiguration EntityInternal
        {
            get
            {
                return (_EntityInternal ?? (_EntityInternal = new TaxConfiguration(EntityManager)));
            }
        }

        private EntityManager EntityManager
        {
            get
            {
                return _EntityManagerInternal;
            }
        }

        public TaxConfigurationView()
        {
            InitializeComponent();

            LoadEntityManager();
            //
            _EntityInternal = new TaxConfiguration(EntityManager);
        }

        public TaxConfigurationView(int id)
        {
            InitializeComponent();

            LoadEntityManager();
            //
            _EntityInternal = new TaxConfiguration(EntityManager, id);
        }

        private void LoadEntityManager()
        {
            _EntityManagerInternal = new EntityManager(this, menuDefault);
        }

        /*
        Picklist Info:
        Tax Type 9
        */
    }
}
