﻿using System.Windows.Controls;
using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Item;
using Fw.View.Interfaces;

namespace Fw.View.Master.Item
{
    /// <summary>
    /// Interaction logic for TaxTypeMasterView.xaml
    /// </summary>
    public partial class TaxTypeMasterView : UserControl, IEntityContainer
    {
        private TaxTypeMaster _Entity;
        private EntityManager _EntityManager;

        public string WindowName
        {
            get { return "Category Master"; }
        }

        public EntityObject Entity
        {
            get
            {
                return (_Entity ?? (_Entity = new TaxTypeMaster(_EntityManager)));
            }
        }

        public TaxTypeMasterView()
        {
            InitializeComponent();

            _EntityManager = new EntityManager(this);
            //
            _Entity = new TaxTypeMaster(_EntityManager);
        }

        public TaxTypeMasterView(int id)
        {
            InitializeComponent();

            _EntityManager = new EntityManager(this);
            //
            _Entity = new TaxTypeMaster(_EntityManager, id);
        }
    }
}
