﻿using System.Windows.Controls;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Item;
using Fw.View.Interfaces;

namespace Fw.View.Master.Item
{
    /// <summary>
    /// Interaction logic for CategoryMasterView.xaml
    /// </summary>
    public partial class CategoryMasterView : UserControl, IEntityContainer
    {
        private CategoryMaster _Entity;
        private EntityManager _EntityManager;

        public string WindowName
        {
            get { return "Category Master"; }
        }

        public Controller.Base.EntityObject Entity
        {
            get
            {
                return (_Entity ?? (_Entity = new CategoryMaster(_EntityManager)));
            }
        }

        public CategoryMasterView()
        {
            InitializeComponent();

            _EntityManager = new EntityManager(this);
            //
            _Entity = new CategoryMaster(_EntityManager);
        }

        public CategoryMasterView(int id)
        {
            InitializeComponent();

            _EntityManager = new EntityManager(this);
            //
            _Entity = new CategoryMaster(_EntityManager, id);
        }
    }
}
