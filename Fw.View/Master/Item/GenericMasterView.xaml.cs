﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Item;
using Fw.View.Interfaces;

namespace Fw.View.Master.Item
{
    /// <summary>
    /// Interaction logic for GenericMasterView.xaml
    /// </summary>
    public partial class GenericMasterView : UserControl, IEntityContainer
    {
        GenericMaster _Entity;

        public GenericMasterView()
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new GenericMaster(manger);
        }

        public GenericMasterView(int id)
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new GenericMaster(manger, id);
        }

        public string WindowName
        {
            get
            {
                return "Generic Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }

        private void GenericMasterView_Loaded(object sender, RoutedEventArgs e)
        {
            dgvItems.ItemsSource = _Entity.ListOfItems;
        }
    }
}
