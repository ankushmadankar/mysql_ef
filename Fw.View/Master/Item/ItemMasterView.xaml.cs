﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Fw.Controller.DocumentEntity;
using Fw.Model.Master.Item;
using Fw.View.Interfaces;

namespace Fw.View.Master.Item
{
    /// <summary>
    /// Interaction logic for UserControl1.xaml
    /// </summary>
    public partial class ItemMasterView : UserControl, IEntityContainer
    {
        ItemMaster _Entity;

        public ItemMasterView()
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ItemMaster(manger);
        }

        public ItemMasterView(int id)
        {
            InitializeComponent();

            EntityManager manger = new EntityManager(this);

            _Entity = new ItemMaster(manger, id);
        }

        public string WindowName
        {
            get
            {
                return "Item Master";
            }
        }

        public Fw.Controller.Base.EntityObject Entity
        {
            get
            {
                return _Entity as Fw.Controller.Base.EntityObject;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            dgvSuppliers.DataContext = (new Fw.Controller.Data.MySqlConnectionManager()).Fill("select Code,Name,MobileNo from ContactMaster").DefaultView;
        }
    }
}
