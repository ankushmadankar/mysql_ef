﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Fw.View.FwTreeView.Interaction;

namespace Fw.View.FwTreeView.ViewModels
{
    public class SettingsViewModel : Notifier
    {
        private readonly ICommand _StoreInPreviousCommand;
        private readonly ICommand _ClearTextCommand;

        private readonly ObservableCollection<TreeNodeViewModel> roots = new ObservableCollection<TreeNodeViewModel>();
        private readonly ObservableCollection<string> previousCriteria = new ObservableCollection<string>();
        private string selectedCriteria = String.Empty;
        private string currentCriteria = String.Empty;

        public SettingsViewModel(IEnumerable<TreeNodeViewModel> roots)
        {
            foreach (var node in roots)
                this.roots.Add(node);

            _StoreInPreviousCommand = new Command(StoreInPrevious);

            _ClearTextCommand = new Command(OnClearText);
        }

        private void StoreInPrevious(object dummy)
        {
            if (String.IsNullOrEmpty(CurrentCriteria))
                return;

            if (!previousCriteria.Contains(CurrentCriteria))
                previousCriteria.Add(CurrentCriteria);

            SelectedCriteria = CurrentCriteria;
        }

        private void OnClearText(object dummy)
        {
            if (String.IsNullOrEmpty(CurrentCriteria))
                return;

            CurrentCriteria = null;
        }

        private void ApplyFilter()
        {
            foreach (var node in roots)
                node.ApplyCriteria(CurrentCriteria, new Stack<TreeNodeViewModel>());
        }

        public ICommand StoreInPreviousCommand
        {
            get { return _StoreInPreviousCommand; }
        }

        public ICommand ClearTextCommand
        {
            get { return _ClearTextCommand; }
        }

        public IEnumerable<TreeNodeViewModel> Roots
        {
            get { return roots; }
        }

        public IEnumerable<string> PreviousCriteria
        {
            get { return previousCriteria; }
        }

        public string SelectedCriteria
        {
            get { return selectedCriteria; }
            set
            {
                if (value == selectedCriteria)
                    return;

                selectedCriteria = value;
                OnPropertyChanged("SelectedCriteria");
            }
        }

        public string CurrentCriteria
        {
            get { return currentCriteria; }
            set
            {
                if (value == currentCriteria)
                    return;

                currentCriteria = value;
                OnPropertyChanged("CurrentCriteria");
                ApplyFilter();
            }
        }
    }
}
