﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows.Data;
using System.Windows.Threading;
using MahApps.Metro.Controls;

namespace Fw.View.FwTreeView.ViewModels
{
    public class PanoramaViewModel : Notifier
    {
        private Random rand = new Random(DateTime.Now.Millisecond);
        private List<TileData> dummyData = new List<TileData>();

        
        public PanoramaViewModel()
        {
           
            //create some dummy data
            dummyData.Add(new TileData("Item Master"));
            dummyData.Add(new TileData("Contact Master"));
            dummyData.Add(new TileData("Sale Bill"));
            dummyData.Add(new TileData("Purchase Bill"));
            

            //Great some dummy groups
            List<PanoramaGroup> data = new List<PanoramaGroup>();
            List<IPanoramaTile> tiles = new List<IPanoramaTile>();

            //for (int i = 0; i < 4; i++)
            //{
                tiles.Add(CreateTile(false));
				tiles.Add(CreateTile(false));
				tiles.Add(CreateTile(false));
				tiles.Add(CreateTile(false));
				tiles.Add(CreateTile(false));

            //}

			data.Add(new PanoramaGroup("Settings", CollectionViewSource.GetDefaultView(tiles)));

            PanoramaItems = data;

        }

        private PanoramaTileViewModel CreateTile(bool isDoubleWidth)
        {
            TileData dummyTileData = dummyData[rand.Next(dummyData.Count)];
            return new PanoramaTileViewModel( 
                dummyTileData.Text, dummyTileData.ImageUrl, isDoubleWidth);
        }

        private IEnumerable<PanoramaGroup> panoramaItems;

        public IEnumerable<PanoramaGroup> PanoramaItems
        {
            get { return this.panoramaItems; }

            set
            {
                if (value != this.panoramaItems)
                {
                    this.panoramaItems = value;
                    OnPropertyChanged("PanoramaItems");
                }
            }
        }
    }

    public class TileData
    {
        public string Text { get; private set; }

        public System.Windows.Media.ImageSource ImageUrl
        {
            get
            {
                return new System.Windows.Media.Imaging.BitmapImage(new Uri(String.Format("pack://application:,,,/FwTreeView/Resources/Images/{0}", "document_icon.png")));
            }
        }

        //public string ImageUrl { get; private set; }

        //public DummyTileData(string text, string imageUrl)
        //{
        //    this.Text = text;
        //    this.ImageUrl = imageUrl;
        //}

        public TileData(string text)
        {
            this.Text = text;
        }
    }
}
