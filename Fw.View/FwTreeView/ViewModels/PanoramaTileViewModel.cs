﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.ComponentModel;
using System.Windows.Threading;
using System.Timers;
using MahApps.Metro.Controls;


namespace Fw.View.FwTreeView.ViewModels
{
    public class PanoramaTileViewModel : Notifier, IPanoramaTile
    {
        //public PanoramaTileViewModel( string text, string imageUrl, bool isDoubleWidth)
        //{
        //    this.Text = text;
        //    this.ImageUrl = imageUrl;
        //    this.IsDoubleWidth = isDoubleWidth;
        //    //this.TileClickedCommand = new SimpleCommand<object, object>(ExecuteTileClickedCommand);
        //}

        public PanoramaTileViewModel(string text, System.Windows.Media.ImageSource imageUrl, bool isDoubleWidth)
        {
            this.Text = text;
            this.ImageUrl = imageUrl;
            this.IsDoubleWidth = isDoubleWidth;
            //this.TileClickedCommand = new SimpleCommand<object, object>(ExecuteTileClickedCommand);
        }

        void LiveUpdateTileTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            if (Counter < 10)
                Counter++;
            else
                Counter = 0;
            OnPropertyChanged("Counter");
        }

        public int Counter { get; set; }
        public string Text { get; private set; }
        public System.Windows.Media.ImageSource ImageUrl { get; private set; }
        public bool IsDoubleWidth { get; private set; }
        public ICommand TileClickedCommand { get; private set; }
 
        public void ExecuteTileClickedCommand(object parameter)
        {
           // messageBoxService.ShowMessage(string.Format("you clicked {0}", this.Text));
        }

		// JH Start - See IPanoramaTile for details
		public bool IsPressed
		{
			get { return _isPressed; }
			set
			{
				if (_isPressed != value)
				{
					_isPressed = value;
                    OnPropertyChanged("IsPressed");
				}
			}
		}
		private bool _isPressed;
		// JH End - See IPanoramaTile for details
	}
}
