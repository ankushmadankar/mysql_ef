﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows.Input;
using Fw.Controller.Extentions;
using Fw.Controller.EntityCommand;

namespace Fw.View.FwTreeView.ViewModels
{
    public class TreeNodeViewModel : Notifier
    {
        private readonly ObservableCollection<TreeNodeViewModel> children;
        private readonly string name;
        public ICommand _NodeActionCommand;

        private bool expanded;
        private bool selected;
        private bool match = true;

        public TreeNodeViewModel(string name, IEnumerable<TreeNodeViewModel> children)
        {
            this.name = name;
            this.children = new ObservableCollection<TreeNodeViewModel>(children);

            _NodeActionCommand = new RelayCommand<object>(PerformAction, CanPerformAction);
        }

        public TreeNodeViewModel(string name)
            : this(name, Enumerable.Empty<TreeNodeViewModel>())
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="contentType"></param>
        /// <param name="entityContentType"></param>
        /// <param name="entityMasterType"></param>
        /// <param name="tabItem"></param>
        /// <param name="sqlQuery"></param>
        /// <param name="tabHeaderName"></param>
        public TreeNodeViewModel(string name, Type contentType, Type entityContentType, Type entityMasterType, ItemCollection tabItem,
            string sqlQuery, string tabHeaderName = null)
            : this(name, Enumerable.Empty<TreeNodeViewModel>())
        {
            ContentType = contentType;
            TabItems = tabItem;
            TabHeaderName = tabHeaderName;
            CatalogQuery = sqlQuery;

            EntityContentType = entityContentType;
            EntityMasterType = entityMasterType;
        }

        public override string ToString()
        {
            return name;
        }

        public ICommand NodeActionCommand
        {
            get { return _NodeActionCommand; }
        }

        private bool IsCriteriaMatched(string criteria)
        {
            return String.IsNullOrEmpty(criteria) || name.Contains(criteria, StringComparison.OrdinalIgnoreCase);
        }

        public void ApplyCriteria(string criteria, Stack<TreeNodeViewModel> ancestors)
        {
            if (IsCriteriaMatched(criteria))
            {
                IsMatch = true;
                foreach (var ancestor in ancestors)
                {
                    ancestor.IsMatch = true;
                    ancestor.IsExpanded = !String.IsNullOrEmpty(criteria);
                }
            }
            else
                IsMatch = false;

            ancestors.Push(this);
            foreach (var child in Children)
                child.ApplyCriteria(criteria, ancestors);

            ancestors.Pop();
        }

        public IEnumerable<TreeNodeViewModel> Children
        {
            get { return children; }
        }

        public string Name
        {
            get { return name; }
        }

        public bool IsExpanded
        {
            get { return expanded; }
            set
            {
                if (value == expanded)
                    return;

                expanded = value;
                if (expanded)
                {
                    foreach (var child in Children)
                        child.IsMatch = true;
                }
                OnPropertyChanged("IsExpanded");
            }
        }

        public bool IsMatch
        {
            get { return match; }
            set
            {
                if (value == match)
                    return;

                match = value;
                OnPropertyChanged("IsMatch");
            }
        }

        public bool IsLeaf
        {
            get { return !Children.Any(); }
        }

        public bool IsSelected
        {
            get { return selected; }
            set
            {
                if (value == selected)
                    return;

                selected = value;

                OnPropertyChanged("IsSelected");

                //if (IsSelected && IsLeaf)
                //{
                //    PurformAction();
                //}
            }
        }

        public System.Windows.Media.ImageSource NodeImage
        {
            get
            {
                return IsLeaf ? new System.Windows.Media.Imaging.BitmapImage(new Uri(String.Format("pack://application:,,,/FwTreeView/Resources/Images/{0}", "document_icon.png")))
                : new System.Windows.Media.Imaging.BitmapImage(new Uri(String.Format("pack://application:,,,/FwTreeView/Resources/Images/{0}", "folder_icon.png")));
            }
        }

        public Type ContentType { get; private set; }

        public Type EntityContentType { get; private set; }

        public Type EntityMasterType { get; private set; }

        public ItemCollection TabItems { get; private set; }

        public string TabHeaderName { get; private set; }

        public string CatalogQuery { get; private set; }

        private bool CanPerformAction(object obj)
        {
            if (ContentType == null || TabItems == null) return false;

            return true;
        }

        private void PerformAction(object dummy)
        {
            if (ContentType == null || TabItems == null) return;

            TabItems.Add(NewTab());
        }

        private MahApps.Metro.Controls.MetroTabItem NewTab()
        {
            var tab = new MahApps.Metro.Controls.MetroTabItem();
            //
            tab.HorizontalContentAlignment = System.Windows.HorizontalAlignment.Stretch;
            tab.VerticalContentAlignment = System.Windows.VerticalAlignment.Stretch;
            tab.VerticalAlignment = System.Windows.VerticalAlignment.Top;
            tab.HorizontalAlignment = System.Windows.HorizontalAlignment.Left;
            //
            tab.CloseButtonEnabled = true;
            tab.IsSelected = true;
            //
            tab.Header = string.IsNullOrEmpty(TabHeaderName) ? Name : TabHeaderName;
            tab.Content = Activator.CreateInstance(ContentType, CatalogQuery, EntityContentType, EntityMasterType);

            return tab;
        }
    }
}
