﻿#pragma checksum "..\..\..\..\Master\RibbonWindow1.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "46093348ABAF0D6522C1F9E6DB203799"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18063
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Fw.Controller.ControlsWpf;
using MahApps.Metro.Controls;
using Microsoft.Windows.Controls;
using Microsoft.Windows.Controls.Ribbon;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Fw.View.Master {
    
    
    /// <summary>
    /// RibbonWindow1
    /// </summary>
    public partial class RibbonWindow1 : MahApps.Metro.Controls.MetroWindow, System.Windows.Markup.IComponentConnector {
        
        
        #line 9 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.View.Master.RibbonWindow1 Window;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Windows.Controls.Ribbon.Ribbon Ribbon;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Windows.Controls.Ribbon.RibbonTab ribbonTab;
        
        #line default
        #line hidden
        
        
        #line 26 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Microsoft.Windows.Controls.Ribbon.RibbonButton btnNew;
        
        #line default
        #line hidden
        
        
        #line 64 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal MahApps.Metro.Controls.MetroTabControl metroTabControl1;
        
        #line default
        #line hidden
        
        
        #line 93 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwTextBox txtItemCode;
        
        #line default
        #line hidden
        
        
        #line 96 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwTextBox txtItemName;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwTextBox txtBarCode;
        
        #line default
        #line hidden
        
        
        #line 102 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwComboBox fwComboBox3;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwComboBox cmbGeneric;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwTextBox txtPacking;
        
        #line default
        #line hidden
        
        
        #line 114 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal Fw.Controller.ControlsWpf.FwTextBox txtUnit;
        
        #line default
        #line hidden
        
        
        #line 360 "..\..\..\..\Master\RibbonWindow1.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.DataGrid dgvSuppliers;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Fw.View;component/master/ribbonwindow1.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\..\Master\RibbonWindow1.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Window = ((Fw.View.Master.RibbonWindow1)(target));
            return;
            case 2:
            this.Ribbon = ((Microsoft.Windows.Controls.Ribbon.Ribbon)(target));
            return;
            case 3:
            this.ribbonTab = ((Microsoft.Windows.Controls.Ribbon.RibbonTab)(target));
            return;
            case 4:
            this.btnNew = ((Microsoft.Windows.Controls.Ribbon.RibbonButton)(target));
            return;
            case 5:
            this.metroTabControl1 = ((MahApps.Metro.Controls.MetroTabControl)(target));
            return;
            case 6:
            this.txtItemCode = ((Fw.Controller.ControlsWpf.FwTextBox)(target));
            return;
            case 7:
            this.txtItemName = ((Fw.Controller.ControlsWpf.FwTextBox)(target));
            return;
            case 8:
            this.txtBarCode = ((Fw.Controller.ControlsWpf.FwTextBox)(target));
            return;
            case 9:
            this.fwComboBox3 = ((Fw.Controller.ControlsWpf.FwComboBox)(target));
            return;
            case 10:
            this.cmbGeneric = ((Fw.Controller.ControlsWpf.FwComboBox)(target));
            return;
            case 11:
            this.txtPacking = ((Fw.Controller.ControlsWpf.FwTextBox)(target));
            return;
            case 12:
            this.txtUnit = ((Fw.Controller.ControlsWpf.FwTextBox)(target));
            return;
            case 13:
            this.dgvSuppliers = ((System.Windows.Controls.DataGrid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

