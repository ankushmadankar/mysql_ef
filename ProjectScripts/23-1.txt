ALTER TABLE `fw_data`.`contactmaster` ADD COLUMN `EmailAddress` VARCHAR(90) NULL  AFTER `Branch` ;

ALTER TABLE `fw_data`.`contactmaster` ADD COLUMN `VersionNo` TIMESTAMP NULL  AFTER `EmailAddress` ;
