SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `fw_data` ;
CREATE SCHEMA IF NOT EXISTS `fw_data` DEFAULT CHARACTER SET utf8 ;
USE `fw_data` ;

-- -----------------------------------------------------
-- Table `fw_data`.`categorymaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`categorymaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`categorymaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(225) NULL DEFAULT NULL ,
  `CategoryUse` MEDIUMINT(9) NULL DEFAULT NULL ,
  `CategoryPathy` MEDIUMINT(9) NULL DEFAULT NULL ,
  `PackType` MEDIUMINT(9) NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`contactmaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`contactmaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`contactmaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(45) NULL DEFAULT NULL ,
  `Name` VARCHAR(255) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `ContactGroupType` SMALLINT(6) NULL DEFAULT NULL ,
  `Address1` VARCHAR(450) NULL DEFAULT NULL ,
  `Address2` VARCHAR(450) NULL DEFAULT NULL ,
  `Address3` VARCHAR(450) NULL DEFAULT NULL ,
  `City` MEDIUMINT(9) NULL DEFAULT NULL ,
  `State` SMALLINT(6) NULL DEFAULT NULL ,
  `Country` SMALLINT(6) NULL DEFAULT NULL ,
  `AreaPinNo` VARCHAR(45) NULL DEFAULT NULL ,
  `STDCode` VARCHAR(45) NULL DEFAULT NULL ,
  `PhoneNo1` VARCHAR(45) NULL DEFAULT NULL ,
  `PhoneNo2` VARCHAR(45) NULL DEFAULT NULL ,
  `MobileNo` VARCHAR(45) NULL DEFAULT NULL ,
  `EmailAddress` VARCHAR(90) NULL DEFAULT NULL ,
  `Fax` VARCHAR(45) NULL DEFAULT NULL ,
  `Web` VARCHAR(45) NULL DEFAULT NULL ,
  `Person` VARCHAR(45) NULL DEFAULT NULL ,
  `Designation` VARCHAR(45) NULL DEFAULT NULL ,
  `AIOCDA` VARCHAR(90) NULL DEFAULT NULL ,
  `CF` VARCHAR(90) NULL DEFAULT NULL ,
  `ZSM` VARCHAR(90) NULL DEFAULT NULL ,
  `RSM` VARCHAR(90) NULL DEFAULT NULL ,
  `MR1` VARCHAR(90) NULL DEFAULT NULL ,
  `MR2` VARCHAR(90) NULL DEFAULT NULL ,
  `MR3` VARCHAR(90) NULL DEFAULT NULL ,
  `MR4` VARCHAR(90) NULL DEFAULT NULL ,
  `TypeOfCode` SMALLINT(6) NULL DEFAULT NULL ,
  `OpeningBalance` FLOAT NULL DEFAULT NULL ,
  `ResidenceNo` VARCHAR(45) NULL DEFAULT NULL ,
  `CreaditDays` INT(11) NULL DEFAULT NULL ,
  `CreaditLimit` FLOAT NULL DEFAULT NULL ,
  `Discount` FLOAT NULL DEFAULT NULL ,
  `InterestRate` FLOAT NULL DEFAULT NULL ,
  `Doctor` VARCHAR(90) NULL DEFAULT NULL ,
  `DLicNo20` VARCHAR(90) NULL DEFAULT NULL ,
  `DLicNo21` VARCHAR(90) NULL DEFAULT NULL ,
  `CSTNo` VARCHAR(90) NULL DEFAULT NULL ,
  `STaxNo` VARCHAR(90) NULL DEFAULT NULL ,
  `LBTNo` VARCHAR(90) NULL DEFAULT NULL ,
  `Bank` VARCHAR(90) NULL DEFAULT NULL ,
  `Branch` VARCHAR(90) NULL DEFAULT NULL ,
  `VersionNo` TIMESTAMP NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 11
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`companymaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`companymaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`companymaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(45) NULL DEFAULT NULL ,
  `Name` VARCHAR(255) NULL DEFAULT NULL ,
  `ContactId` INT(11) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `Discountinous` BIT(1) NULL DEFAULT NULL ,
  `Prohibited` BIT(1) NULL DEFAULT NULL ,
  `ExpiryReceiveDays` SMALLINT(6) NULL DEFAULT NULL ,
  `DumpDays` SMALLINT(6) NULL DEFAULT NULL ,
  `AIOCDA` VARCHAR(45) NULL DEFAULT NULL ,
  `DLNo` VARCHAR(100) NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) ,
  INDEX `FK_CONTACT_COMPANY_idx` (`ContactId` ASC) ,
  CONSTRAINT `FK_CONTACT_COMPANY`
    FOREIGN KEY (`ContactId` )
    REFERENCES `fw_data`.`contactmaster` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`doctormaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`doctormaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`doctormaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `HeaderId` INT(11) NULL DEFAULT NULL ,
  `Specialty` VARCHAR(100) NULL DEFAULT NULL ,
  `Qualification` VARCHAR(100) NULL DEFAULT NULL ,
  `Timing` VARCHAR(100) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`genericmaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`genericmaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`genericmaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(45) NULL DEFAULT NULL ,
  `Name` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`itemmaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`itemmaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`itemmaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(45) NOT NULL ,
  `Name` VARCHAR(500) NULL DEFAULT NULL ,
  `BarCode` VARCHAR(100) NULL DEFAULT NULL ,
  `CreationDate` DATETIME NULL DEFAULT NULL ,
  `ManufacturerId` INT(11) NULL DEFAULT NULL ,
  `GenericId` INT(11) NULL DEFAULT NULL ,
  `CategoryId` INT(11) NULL DEFAULT NULL ,
  `Packing` VARCHAR(45) NULL DEFAULT NULL ,
  `Unit` VARCHAR(45) NULL DEFAULT NULL ,
  `RackId` MEDIUMINT(9) NULL DEFAULT NULL ,
  `CalculationList` MEDIUMINT(9) NULL DEFAULT NULL ,
  `Status` BIT(1) NULL DEFAULT NULL ,
  `Prohibited` BIT(1) NULL DEFAULT NULL ,
  `Hide` BIT(1) NULL DEFAULT NULL ,
  `Conversion` FLOAT NULL DEFAULT NULL ,
  `BoxQty` FLOAT NULL DEFAULT NULL ,
  `MinOrderQty` FLOAT NULL DEFAULT NULL ,
  `ReorderQty` FLOAT NULL DEFAULT NULL ,
  `MinQty` FLOAT NULL DEFAULT NULL ,
  `MaxQty` FLOAT NULL DEFAULT NULL ,
  `PurchaseVat` INT(11) NULL DEFAULT NULL ,
  `PurchaseCst` INT(11) NULL DEFAULT NULL ,
  `LBT` INT(11) NULL DEFAULT NULL ,
  `SaleVat` INT(11) NULL DEFAULT NULL ,
  `SaleCst` INT(11) NULL DEFAULT NULL ,
  `CentralSaleVat` INT(11) NULL DEFAULT NULL ,
  `StateSaleVat` INT(11) NULL DEFAULT NULL ,
  `CentralPurchaseVat` INT(11) NULL DEFAULT NULL ,
  `StatePurchaseVat` INT(11) NULL DEFAULT NULL ,
  `Cost` FLOAT NULL DEFAULT NULL ,
  `MRP` FLOAT NULL DEFAULT NULL ,
  `PurchaseRate` FLOAT NULL DEFAULT NULL ,
  `RateA` FLOAT NULL DEFAULT NULL ,
  `RateB` FLOAT NULL DEFAULT NULL ,
  `RateC` FLOAT NULL DEFAULT NULL ,
  `Discount` FLOAT NULL DEFAULT NULL ,
  `SDiscount` FLOAT NULL DEFAULT NULL ,
  `MinimumMargin` FLOAT NULL DEFAULT NULL ,
  `Narcotic` BIT(1) NULL DEFAULT NULL ,
  `ScheduleH` BIT(1) NULL DEFAULT NULL ,
  `ScheduleH1` BIT(1) NULL DEFAULT NULL ,
  `MedicineType` MEDIUMINT(9) NULL DEFAULT NULL ,
  `DpcoPrice` FLOAT NULL DEFAULT NULL ,
  `BestBeforeDays` INT(11) NULL DEFAULT NULL ,
  `SchemeType` MEDIUMINT(9) NULL DEFAULT NULL ,
  `SchemeQty` FLOAT NULL DEFAULT NULL ,
  `SchemeFreeQty` FLOAT NULL DEFAULT NULL ,
  `PurchaseDiscount1` FLOAT NULL DEFAULT NULL ,
  `PurchaseDiscount2` FLOAT NULL DEFAULT NULL ,
  `RateCalculation` MEDIUMINT(9) NULL DEFAULT NULL ,
  `PurVolDis` FLOAT NULL DEFAULT NULL ,
  `PurVolDisOnQty` FLOAT NULL DEFAULT NULL ,
  `SaleVolDis` FLOAT NULL DEFAULT NULL ,
  `SaleVolDisOnQty` FLOAT NULL DEFAULT NULL ,
  `TerrifHeading` VARCHAR(45) NULL DEFAULT NULL ,
  `TerrifDescription` VARCHAR(255) NULL DEFAULT NULL ,
  `AllowNegativeStock` BIT(1) NULL DEFAULT NULL ,
  `CanExpire` BIT(1) NULL DEFAULT NULL ,
  `Narration` VARCHAR(1000) NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) ,
  INDEX `Id_idx` (`GenericId` ASC) ,
  CONSTRAINT `fk_genericMaster_GenericId`
    FOREIGN KEY (`GenericId` )
    REFERENCES `fw_data`.`genericmaster` (`Id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 8
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`itemtypemaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`itemtypemaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`itemtypemaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `TypeCode` VARCHAR(45) NULL DEFAULT NULL ,
  `TypeDescription` VARCHAR(255) NULL DEFAULT NULL ,
  `ShortName` VARCHAR(45) NULL DEFAULT NULL ,
  `Discount` FLOAT NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`partymaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`partymaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`partymaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `ForeignId` INT(11) NULL DEFAULT NULL ,
  `CGST` VARCHAR(45) NULL DEFAULT NULL ,
  `CGSTDate` DATETIME NULL DEFAULT NULL ,
  `VATTIN` VARCHAR(45) NULL DEFAULT NULL ,
  `VATDate` DATETIME NULL DEFAULT NULL ,
  `SGST` VARCHAR(45) NULL DEFAULT NULL ,
  `SGSTDate` DATETIME NULL DEFAULT NULL ,
  `CSTTIN` VARCHAR(45) NULL DEFAULT NULL ,
  `CSTDate` DATETIME NULL DEFAULT NULL ,
  `DLNo1` VARCHAR(45) NULL DEFAULT NULL ,
  `DLNo1ExpDate` DATETIME NULL DEFAULT NULL ,
  `DLNo2` VARCHAR(45) NULL DEFAULT NULL ,
  `DLNo2ExpDate` DATETIME NULL DEFAULT NULL ,
  `FoodLicenceNo` VARCHAR(45) NULL DEFAULT NULL ,
  `FoodLicenceNoExpDate` DATETIME NULL DEFAULT NULL ,
  `SSID` VARCHAR(45) NULL DEFAULT NULL ,
  `LumpSum` VARCHAR(45) NULL DEFAULT NULL ,
  `Method` VARCHAR(45) NULL DEFAULT NULL ,
  `SWLicNo` VARCHAR(45) NULL DEFAULT NULL ,
  `BillingMethod` MEDIUMINT(9) NULL DEFAULT NULL ,
  `Category` MEDIUMINT(9) NULL DEFAULT NULL ,
  `NacroHBilling` MEDIUMINT(9) NULL DEFAULT NULL ,
  `Commission` FLOAT NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`picklistmaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`picklistmaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`picklistmaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Name` VARCHAR(90) NULL DEFAULT NULL ,
  `CanAddNewValue` BIT(1) NULL DEFAULT NULL ,
  `Description` VARCHAR(255) NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`picklistvalues`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`picklistvalues` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`picklistvalues` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `HeaderId` INT(11) NULL DEFAULT NULL ,
  `ValueId` SMALLINT(6) NULL DEFAULT NULL ,
  `ValueName` VARCHAR(90) NULL DEFAULT NULL ,
  `Description` VARCHAR(255) NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) ,
  UNIQUE INDEX `Id_UNIQUE` (`Id` ASC) ,
  INDEX `fk_PicklistMasterId_MasterId_idx` (`HeaderId` ASC) ,
  CONSTRAINT `fk_PicklistMasterId_MasterId`
    FOREIGN KEY (`HeaderId` )
    REFERENCES `fw_data`.`picklistmaster` (`Id` )
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 48
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`taxconfiguration`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`taxconfiguration` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`taxconfiguration` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(45) NULL DEFAULT NULL ,
  `Name` VARCHAR(255) NULL DEFAULT NULL ,
  `TaxType` MEDIUMINT(9) NULL DEFAULT NULL ,
  `Value` FLOAT NULL DEFAULT NULL ,
  `Formula` VARCHAR(100) NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `fw_data`.`taxtypemaster`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `fw_data`.`taxtypemaster` ;

CREATE  TABLE IF NOT EXISTS `fw_data`.`taxtypemaster` (
  `Id` INT(11) NOT NULL AUTO_INCREMENT ,
  `Code` VARCHAR(45) NULL DEFAULT NULL ,
  `Name` VARCHAR(255) NULL DEFAULT NULL ,
  `TaxType` MEDIUMINT(9) NULL DEFAULT NULL ,
  `Tax` FLOAT NULL DEFAULT NULL ,
  `Surcharge` FLOAT NULL DEFAULT NULL ,
  `VersionNo` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`Id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;

USE `fw_data` ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
