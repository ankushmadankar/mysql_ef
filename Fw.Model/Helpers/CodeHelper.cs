﻿using Fw.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Extentions;

namespace Fw.Model.Helpers
{
    /// <summary>
    /// Code generator which help to generate code for master or transactions.
    /// </summary>
    public static class CodeHelper
    {
        /// <summary>
        /// Generate code on basis of docType which saved in DocumentCodeMaster
        /// </summary>
        /// <param name="seriesType">Series type used.</param>
        /// <param name="docType">Doc type used.</param>
        /// <returns></returns>
        public static string GenerateNextCode(SeriesType seriesType, string docType)
        {
            return GenerateNextCode(seriesType, (DocType)Enum.Parse(typeof(DocType), docType, true));
        }

        /// <summary>
        /// Generate code on basis of docType which saved in DocumentCodeMaster
        /// </summary>
        /// <param name="seriesType">Series type used.</param>
        /// <param name="docType">Doc type used.</param>
        /// <returns></returns>
        public static string GenerateNextCode(SeriesType seriesType, DocType docType)
        {
            string query = "select * from DocumentCodeMaster where SeriesType = {0} limit 1";

            var documentMaster = new Fw.Controller.Data.MySqlConnectionManager().ExecuteQuery<CodeMasterContract>(query, seriesType.ToString()).FirstOrDefault();

            switch (seriesType)
            {
                case SeriesType.Daily:
                    if (!documentMaster.IsNull() && documentMaster.LastUpdatedDate.Date == DateTime.Now.Date)
                        return docType + "-" + DateTime.Now.ToShortDateString() + "-" + (documentMaster.LastNumber + 1).ToString();
                    else
                        return docType + DateTime.Now.ToShortDateString() + "1";

                case SeriesType.Monthly:
                    if (!documentMaster.IsNull() && documentMaster.LastUpdatedDate.Month == DateTime.Now.Month)
                        return docType + "-" + DateTime.Now.ToString("mm") + DateTime.Now.ToString("yyyy") + "-" + (documentMaster.LastNumber + 1).ToString();
                    else
                        return docType + "-" + DateTime.Now.ToString("mm") + DateTime.Now.ToString("yyyy") + "-" + "1";

                case SeriesType.Yearly:
                    if (!documentMaster.IsNull() && documentMaster.LastUpdatedDate.Year == DateTime.Now.Year)
                        return docType + "-" + DateTime.Now.ToString("yyyy") + "-" + (documentMaster.LastNumber + 1).ToString();
                    else
                        return docType + "-" + DateTime.Now.ToString("yyyy") + "-" + "1";

                case SeriesType.Number:
                    if (!documentMaster.IsNull())
                        return docType + "-" + (documentMaster.LastNumber + 1).ToString();
                    else
                        return docType + "-" + "1";

                default:
                    return string.Empty;
            }
        }

        /// <summary>
        /// Internal helper class for code master.
        /// </summary>
        private class CodeMasterContract
        {
            public DateTime LastUpdatedDate { get; set; }
            public int LastNumber { get; set; }
        }
    }
}
