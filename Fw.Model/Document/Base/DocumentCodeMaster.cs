﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Extentions;

namespace Fw.Model.Document.Base
{
    public class DocumentCodeMaster : EntityObject
    {
        #region Declaration

        private DateTime _LastUpdatedDate;
        private string _DocType;
        private int _LastNumber;
        
        #endregion

        #region Constructor

        /// <summary>
        /// Create instance of document type, default series is daily.
        /// </summary>
        /// <param name="entityManager"></param>
        public DocumentCodeMaster(EntityManager entityManager)
            : base("DocumentCodeMaster", entityManager)
        {
        }

        public DocumentCodeMaster(EntityManager entityManager, int id)
            : base("DocumentCodeMaster", entityManager, id)
        {
        }

        #endregion

        #region Properties

        [Entity("DocType", AttributeType.Physical, ColumnName = "DocType", ShowControl = true)]
        public string DocType
        {
            get
            {
                return _DocType;
            }
            set
            {
                if (_DocType == value) return;

                NotifyPropertyChanging("DocType");
                _DocType = value;
                NotifyPropertyChanged("DocType");
            }
        }

        [Entity("LastNumber", AttributeType.Physical, ColumnName = "LastNumber", ShowControl = true)]
        public int LastNumber
        {
            get
            {
                return _LastNumber;
            }
            set
            {
                if (_LastNumber == value) return;

                NotifyPropertyChanging("LastNumber");
                _LastNumber = value;
                NotifyPropertyChanged("LastNumber");
            }
        }

        [Entity("LastUpdatedDate", AttributeType.Physical, ColumnName = "LastUpdatedDate", ShowControl = true)]
        public DateTime LastUpdatedDate
        {
            get
            {
                return _LastUpdatedDate;
            }
            set
            {
                if (_LastUpdatedDate == value) return;

                NotifyPropertyChanging("LastUpdatedDate");
                _LastUpdatedDate = value;
                NotifyPropertyChanged("LastUpdatedDate");
            }
        } 
        
        #endregion
    }
}
