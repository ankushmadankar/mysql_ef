﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;

namespace Fw.Model.Configuration
{
    public class PickListValues : EntityObject, ILineEntity
    {
        #region Declaration

        private short _ValueId;
        private string _ValueName;
        private int _HeaderId;
        private string _Description;
        private readonly HeaderEntity<ILineEntity> _HeaderEntity;

        #endregion

        #region Constructor

        public PickListValues(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("PickListValues", entityManger)
        { }

        public PickListValues(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("PickListValues", entityManger, id)
        { }

        public PickListValues(Fw.Controller.DocumentEntity.EntityManager entityManger, HeaderEntity<ILineEntity> headerEntity)
            : this(entityManger)
        {
            _HeaderEntity = headerEntity;
        }

        public PickListValues(Fw.Controller.DocumentEntity.EntityManager entityManger, HeaderEntity<ILineEntity> headerEntity, int id)
            : this(entityManger, id)
        {
            _HeaderEntity = headerEntity;
        }

        #endregion

        #region Properties

        [Entity("HeaderId", AttributeType.Physical, ColumnName = "HeaderId", IsRequired = true, ShowControl = true)]
        public int HeaderId
        {
            get
            {
                return _HeaderId;
            }
            set
            {
                if (_HeaderId == value) return;

                NotifyPropertyChanging("HeaderId");
                _HeaderId = value;
                NotifyPropertyChanged("HeaderId");
            }
        }

        [Entity("ValueId", AttributeType.Physical, ColumnName = "ValueId", IsRequired = true, ShowControl = true)]
        public short ValueId
        {
            get
            {
                return _ValueId;
            }
            set
            {
                if (_ValueId == value) return;

                NotifyPropertyChanging("ValueId");
                _ValueId = value;
                NotifyPropertyChanged("ValueId");
            }
        }

        [Entity("ValueName", AttributeType.Physical, ColumnName = "ValueName", IsRequired = true, ShowControl = true)]
        public string ValueName
        {
            get
            {
                return _ValueName;
            }
            set
            {
                if (_ValueName == value) return;

                NotifyPropertyChanging("ValueName");
                _ValueName = value;
                NotifyPropertyChanged("ValueName");
            }
        }

        [Entity("Description", AttributeType.Physical, ColumnName = "Description", IsRequired = false, ShowControl = true)]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description == value) return;

                NotifyPropertyChanging("Description");
                _Description = value;
                NotifyPropertyChanged("Description");
            }

        }

        [Entity("HeaderEntity", AttributeType.Other)]
        public HeaderEntity<ILineEntity> HeaderEntity
        {
            get
            {
                return _HeaderEntity;
            }
        }

        #endregion

        protected override void OnLoadEntity()
        {
            base.OnLoadEntity();

           
        }
    }
}
