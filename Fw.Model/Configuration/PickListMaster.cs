﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Base;

namespace Fw.Model.Configuration
{
    public class PickListMaster : HeaderEntity<PickListValues>
    {
        #region Declaration

        private string _Description;
        private string _Name;
        private bool? _CanAddNewValue;

        #endregion

        #region Constructor

        public PickListMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("PickListMaster", entityManger)
        { }

        public PickListMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("PickListMaster", entityManger, id)
        { }

        #endregion

        #region Properties

        [Entity("Name", AttributeType.Physical, ColumnName = "Name", IsRequired = true, ShowControl = true)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;

                NotifyPropertyChanging("Name");
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        [Entity("Description", AttributeType.Physical, ColumnName = "Description", IsRequired = false, ShowControl = true)]
        public string Description
        {
            get
            {
                return _Description;
            }
            set
            {
                if (_Description == value) return;

                NotifyPropertyChanging("Description");
                _Description = value;
                NotifyPropertyChanged("Description");
            }

        }
        
        [Entity("CanAddNewValue", AttributeType.Physical, ColumnName = "CanAddNewValue", ShowControl = true)]
        public bool? CanAddNewValue
        {
            get
            {
                return _CanAddNewValue;
            }
            set
            {
                if (_CanAddNewValue == value) return;

                NotifyPropertyChanging("CanAddNewValue");
                _CanAddNewValue = value;
                NotifyPropertyChanged("CanAddNewValue");
            }
        }

        #endregion

        #region Overrided Methods

        protected override void Lines_AddingNew(object sender, System.ComponentModel.AddingNewEventArgs e)
        {
            if (!CanAddNewValue.GetValueOrDefault() && Id > 0)
                throw new Fw.Controller.Base.ValidationError("You can not add new line for this picklist.");

            base.Lines_AddingNew(sender, e);

            if (e.NewObject != null && e.NewObject is PickListValues)
            {
                if (Lines.Count > 0)
                    (e.NewObject as PickListValues).ValueId = (short)Lines.Max(a => a.ValueId + 1);
                else
                    (e.NewObject as PickListValues).ValueId = 1;
            }
        }
 
        #endregion
    }
}
