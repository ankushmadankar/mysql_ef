﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using Fw.Controller.Base;
using Fw.Controller.Extentions;
using Fw.Controller.DocumentEntity;
using Fw.Model.Helpers;

namespace Fw.Model.Base
{
    public class DocumentEntity : HeaderEntity<ILineEntity>
    {
        #region Declaration

        private string _DocType;
        private string _VoucherNo;
        private DateTime _VoucherDate;

        #endregion

        #region Constructor

        public DocumentEntity(string tableName, EntityManager entityManager)
            : base(tableName, entityManager)
        {
        }

        public DocumentEntity(string tableName, EntityManager entityManager, int id)
            : base(tableName, entityManager, id)
        {
        }

        #endregion

        #region Properties

        [Entity("DocType", AttributeType.Physical, ColumnName = "DocType", ShowControl = false)]
        public string DocType
        {
            get
            {
                return _DocType;
            }
            set
            {
                if (_DocType == value) return;

                NotifyPropertyChanging("DocType");
                _DocType = value;
                NotifyPropertyChanged("DocType");
            }
        }

        [Entity("VoucherNo", AttributeType.Physical, IsRequired = true, ColumnName = "VoucherNo", ShowControl = true)]
        public string VoucherNo
        {
            get
            {
                return _VoucherNo;
            }
            set
            {
                if (_VoucherNo == value) return;

                NotifyPropertyChanging("VoucherNo");
                _VoucherNo = value;
                NotifyPropertyChanged("VoucherNo");
            }
        }

        [Entity("VoucherDate", AttributeType.Physical, IsRequired = true, ColumnName = "VoucherDate", ShowControl = true)]
        public DateTime VoucherDate
        {
            get
            {
                return _VoucherDate;
            }
            set
            {
                if (_VoucherDate == value) return;

                NotifyPropertyChanging("VoucherDate");
                _VoucherDate = value;
                NotifyPropertyChanged("VoucherDate");
            }
        }

        [Entity("SeriesType", AttributeType.Other)]
        public SeriesType SeriesType { get; set; }

        #endregion

        #region Overrided Methods

        protected override void OnLoadEntity()
        {
            base.OnLoadEntity();

            if (ObjectState == EntityState.Added)
            {
                VoucherDate = DateTime.Now;
                VoucherNo = CodeHelper.GenerateNextCode(SeriesType, DocType);
            }
        }

        protected override void OnBeginSavingDataEntity(BeginSavingDataEntity e)
        {
            base.OnBeginSavingDataEntity(e);

            if (ObjectState == EntityState.Added)
            {
                VoucherDate = DateTime.Now;
                VoucherNo = CodeHelper.GenerateNextCode(SeriesType, DocType);
            }
        }

        #endregion
    }

    /// <summary>
    /// Type of series required.
    /// </summary>
    public enum SeriesType : short
    {
        Daily = 1,
        Monthly = 2,
        Yearly = 3,
        Number = 4
    }

    /// <summary>
    /// Type of document or master.
    /// </summary>
    public enum DocType
    {
        /// <summary>
        /// Item Master
        /// </summary>
        IM,
        
        /// <summary>
        /// Contact Master
        /// </summary>
        CM,
        
        /// <summary>
        /// Generic Master
        /// </summary>
        GM,
        
        /// <summary>
        /// Tax Type Master
        /// </summary>
        TM,

        /// <summary>
        /// Ledger Master
        /// </summary>
        LM,

        /// <summary>
        /// Sale Header
        /// </summary>
        SH,
        
        /// <summary>
        /// Purchase Header
        /// </summary>
        PH,
        
        /// <summary>
        /// 
        /// </summary>
        SI,
        
        /// <summary>
        /// 
        /// </summary>
        PI,
    }
}
