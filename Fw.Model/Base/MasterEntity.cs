﻿using Fw.Controller.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Extentions;
using Fw.Controller.DocumentEntity;
using Fw.Model.Helpers;

namespace Fw.Model.Base
{
    /// <summary>
    /// Master entity having code and name. Code by default set by class.
    /// </summary>
    public class MasterEntity : EntityObject
    {
        #region Declaration

        private string _Code;
        private string _Name;

        #endregion

        #region Constructor

        public MasterEntity(string tableName, EntityManager entityManager, DocType masterType)
            : base(tableName, entityManager)
        {
            MasterType = masterType;
        }

        public MasterEntity(string tableName, EntityManager entityManager, int id, DocType masterType)
            : base(tableName, entityManager, id)
        {
            MasterType = masterType;
        }

        #endregion

        #region Entity Properties

        [Entity("Code", AttributeType.Physical, IsRequired = true, ColumnName = "Code", ShowControl = true)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                if (_Code == value) return;

                NotifyPropertyChanging("Code");
                _Code = value;
                NotifyPropertyChanged("Code");
            }
        }

        [Entity("Name", AttributeType.Physical, IsRequired = true, ColumnName = "Name", ShowControl = true)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;

                NotifyPropertyChanging("Name");
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Type of master, on basis of this property code get generated.
        /// </summary>
        public DocType MasterType { get; protected set; }

        #endregion

        #region Overrided Method

        protected override void OnLoadEntity()
        {
            base.OnLoadEntity();

            if (ObjectState == EntityState.Added)
                Code = CodeHelper.GenerateNextCode(SeriesType.Number, MasterType);
        }

        protected override void OnBeginSavingDataEntity(BeginSavingDataEntity e)
        {
            base.OnBeginSavingDataEntity(e);

            if (IsExists())
                throw new ValidationError("Error", "Code already in used.");
        }

        #endregion

        #region Public Helper

        /// <summary>
        /// Check if master exists with code, master code
        /// </summary>
        /// <returns></returns>
        public bool IsExists()
        {
            return IsExists(Code);
        }

        /// <summary>
        /// Check if master exists or not by using code.
        /// </summary>
        /// <param name="code">Code to check.</param>
        /// <returns>True if exists.</returns>
        public bool IsExists(string code)
        {
            if (TableName.IsNull())
                throw new NotSupportedException("Table name must set before.");

            return (bool)MySqlConnectionManagers.ExecuteScalar("SELECT EXISTS(SELECT 1 FROM {0} WHERE Code = {1})", TableName, Code);
        }

        #endregion
    }
}
