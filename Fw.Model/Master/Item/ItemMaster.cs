﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;
using Fw.Controller.Extentions;
using Fw.Model.Base;

namespace Fw.Model.Master.Item
{
    public class ItemMaster : MasterEntity
    {
        #region Declarations

        private string _Code;
        private string _Name;
        private string _BarCode;
        private string _Packing;
        private string _Unit;
        private short? _CalculationList;
        private bool? _Status;
        private bool? _Prohibited;
        private bool? _Hide;
        private double? _Conversion;
        private DateTime? _CreationDate;
       
        private short? _Category;
        private int? _ManufacturerId;
        private int? _SupplierId;
        private double? _MinQty;
        
        private double? _ReorderQty;
        private double? _MinOrderQty;
        private double? _BoxQty;
        private double? _MaxQty;
        private int? _GenericId;
        private double? _Cost;
        private int? _PurchaseVat;
        private int? _PurchaseCst;
        private int? _SaleVat;
        private int? _SaleCst;
        private int? _CentralSaleVat;
        private int? _StateSaleVat;
        private int? _CentralPurchaseVat;
        private int? _StatePurchaseVat;
        private int? _LBT;

        private double? _Discount;
        private double? _SDiscount;
        private string _Narration;
        
        private double? _MRP;
        private double? _PurchaseRate;
        private double? _RateA;
        private double? _RateB;
        private double? _RateC;
        private bool? _Narcotic;
        private bool? _ScheduleH;
        private bool? _ScheduleH1;
        private double? _MinimumMargin;
        private int? _CategoryId;
        private short? _RackId;

        short? _MedicineType;
        double? _DpcoPrice;
        int? _BestBeforeDays;
        short? _SchemeType;
        double? _SchemeQty;
        double? _SchemeFreeQty;
        double? _PurchaseDiscount1;
        double? _PurchaseDiscount2;
        short? _RateCalculation;
        double? _PurVolDis;
        double? _PurVolDisOnQty;
        double? _SaleVolDis;
        double? _SaleVolDisOnQty;
        string _TerrifHeading;
        string _TerrifDescription;
        bool? _AllowNegativeStock;
        bool? _CanExpire;

        #endregion

        #region Contructor

        public ItemMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("ItemMaster", entityManger, DocType.IM)
        { CreationDate = DateTime.Now; }

        public ItemMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("ItemMaster", entityManger, id, DocType.IM)
        { }

        #endregion

        #region Entity Properties

        /// <summary>
        /// Get or set item code
        /// </summary>
        [Entity("Code", AttributeType.Physical, ColumnName = "Code", IsRequired = true, ShowControl = true)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                if (_Code == value) return;

                NotifyPropertyChanging("Code");
                _Code = value;
                NotifyPropertyChanged("Code");
            }
        }

        /// <summary>
        /// Get or set Item Name
        /// </summary>
        [Entity("Name", AttributeType.Physical, ColumnName = "Name", IsRequired = true, ShowControl = true)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;

                NotifyPropertyChanging("Name");
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        [Entity("BarCode", AttributeType.Physical, ColumnName = "BarCode", ShowControl = true)]
        public string BarCode
        {
            get
            {
                return _BarCode;
            }
            set
            {
                if (_BarCode == value) return;

                NotifyPropertyChanging("BarCode");
                _BarCode = value;
                NotifyPropertyChanged("BarCode");
            }
        }

        [Entity("CreationDate", AttributeType.Physical, ColumnName = "CreationDate", ShowControl = true)]
        public DateTime? CreationDate
        {
            get
            {
                return _CreationDate;
            }
            set
            {
                if (_CreationDate == value) return;

                NotifyPropertyChanging("CreationDate");
                _CreationDate = value;
                NotifyPropertyChanged("CreationDate");
            }
        }

        [Entity("ManufacturerId", AttributeType.Physical, ColumnName = "ManufacturerId", ShowControl = true)]
        public int? ManufacturerId
        {
            get
            {
                return _ManufacturerId;
            }
            set
            {
                if (_ManufacturerId == value) return;

                NotifyPropertyChanging("ManufacturerId");
                _ManufacturerId = value;
                NotifyPropertyChanged("ManufacturerId");
            }
        }

        [Entity("GenericId", AttributeType.Physical, ColumnName = "GenericId", ShowControl = true)]
        public int? GenericId
        {
            get
            {
                return _GenericId;
            }
            set
            {
                if (_GenericId == value) return;

                NotifyPropertyChanging("GenericId");
                _GenericId = value;
                NotifyPropertyChanged("GenericId");
            }
        }

        [Entity("CategoryId", AttributeType.Physical, ColumnName = "CategoryId", ShowControl = true)]
        public int? CategoryId
        {
            get
            {
                return _CategoryId;
            }
            set
            {
                if (_CategoryId == value) return;

                NotifyPropertyChanging("CategoryId");
                _CategoryId = value;
                NotifyPropertyChanged("CategoryId");
            }
        }

        [Entity("Packing", AttributeType.Physical, ColumnName = "Packing", ShowControl = true)]
        public string Packing
        {
            get
            {
                return _Packing;
            }
            set
            {
                if (_Packing == value) return;

                NotifyPropertyChanging("Packing");
                _Packing = value;
                NotifyPropertyChanged("Packing");
            }
        }

        [Entity("Unit", AttributeType.Physical, ColumnName = "Unit", ShowControl = true)]
        public string Unit
        {
            get
            {
                return _Unit;
            }
            set
            {
                if (_Unit == value) return;

                NotifyPropertyChanging("Unit");
                _Unit = value;
                NotifyPropertyChanged("Unit");
            }
        }

        [Entity("RackId", AttributeType.Physical, ColumnName = "RackId", ShowControl = true)]
        public short? RackId
        {
            get
            {
                return _RackId;
            }
            set
            {
                if (_RackId == value) return;

                NotifyPropertyChanging("RackId");
                _RackId = value;
                NotifyPropertyChanged("RackId");
            }
        }

        [Entity("CalculationList", AttributeType.Physical, ColumnName = "CalculationList", ShowControl = true)]
        public short? CalculationList
        {
            get
            {
                return _CalculationList;
            }
            set
            {
                if (_CalculationList == value) return;

                NotifyPropertyChanging("CalculationListId");
                _CalculationList = value;
                NotifyPropertyChanged("CalculationListId");
            }
        }

        [Entity("Status", AttributeType.Physical, ColumnName = "Status", ShowControl = true)]
        public bool? Status
        {
            get
            {
                return _Status;
            }
            set
            {
                if (_Status == value) return;

                NotifyPropertyChanging("Status");
                _Status = value;
                NotifyPropertyChanged("Status");
            }
        }

        [Entity("Prohibited", AttributeType.Physical, ColumnName = "Prohibited", ShowControl = true)]
        public bool? Prohibited
        {
            get
            {
                return _Prohibited;
            }
            set
            {
                if (_Prohibited == value) return;

                NotifyPropertyChanging("Prohibited");
                _Prohibited = value;
                NotifyPropertyChanged("Prohibited");
            }
        }

        [Entity("Hide", AttributeType.Physical, ColumnName = "Hide", ShowControl = true)]
        public bool? Hide
        {
            get
            {
                return _Hide;
            }
            set
            {
                if (_Hide == value) return;

                NotifyPropertyChanging("Hide");
                _Hide = value;
                NotifyPropertyChanged("Hide");
            }
        }

        [Entity("Conversion", AttributeType.Physical, ColumnName = "Conversion", ShowControl = true)]
        public double? Conversion
        {
            get
            {
                return _Conversion;
            }
            set
            {
                if (_Conversion == value) return;

                NotifyPropertyChanging("Conversion");
                _Conversion = value;
                NotifyPropertyChanged("Conversion");
            }
        }

        [Entity("BoxQty", AttributeType.Physical, ColumnName = "BoxQty", ShowControl = true)]
        public double? BoxQty
        {
            get
            {
                return _BoxQty;
            }
            set
            {
                if (_BoxQty == value) return;

                NotifyPropertyChanging("BoxQty");
                _BoxQty = value;
                NotifyPropertyChanged("BoxQty");
            }
        }

        [Entity("MinOrderQty", AttributeType.Physical, ColumnName = "MinOrderQty", ShowControl = true)]
        public double? MinOrderQty
        {
            get
            {
                return _MinOrderQty;
            }
            set
            {
                if (_MinOrderQty == value) return;

                NotifyPropertyChanging("MinOrderQty");
                _MinOrderQty = value;
                NotifyPropertyChanged("MinOrderQty");
            }
        }

        [Entity("ReorderQty", AttributeType.Physical, ColumnName = "ReorderQty", ShowControl = true)]
        public double? ReorderQty
        {
            get
            {
                return _ReorderQty;
            }
            set
            {
                if (_ReorderQty == value) return;

                NotifyPropertyChanging("ReorderQty");
                _ReorderQty = value;
                NotifyPropertyChanged("ReorderQty");
            }
        }

        [Entity("MinQty", AttributeType.Physical, ColumnName = "MinQty", ShowControl = true)]
        public double? MinQty
        {
            get
            {
                return _MinQty;
            }
            set
            {
                if (_MinQty == value) return;

                NotifyPropertyChanging("MinQty");
                _MinQty = value;
                NotifyPropertyChanged("MinQty");
            }
        }

        [Entity("MaxQty", AttributeType.Physical, ColumnName = "MaxQty", ShowControl = true)]
        public double? MaxQty
        {
            get
            {
                return _MaxQty;
            }
            set
            {
                if (_MaxQty == value) return;

                NotifyPropertyChanging("MaxQty");
                _MaxQty = value;
                NotifyPropertyChanged("MaxQty");
            }
        }

        [Entity("PurchaseVat", AttributeType.Physical, ColumnName = "PurchaseVat", ShowControl = true)]
        public int? PurchaseVat
        {
            get
            {
                return _PurchaseVat;
            }
            set
            {
                if (_PurchaseVat == value) return;

                NotifyPropertyChanging("PurchaseVat");
                _PurchaseVat = value;
                NotifyPropertyChanged("PurchaseVat");
            }
        }

        [Entity("PurchaseCst", AttributeType.Physical, ColumnName = "PurchaseCst", ShowControl = true)]
        public int? PurchaseCst
        {
            get
            {
                return _PurchaseCst;
            }
            set
            {
                if (_PurchaseCst == value) return;

                NotifyPropertyChanging("PurchaseCst");
                _PurchaseCst = value;
                NotifyPropertyChanged("PurchaseCst");
            }
        }

        [Entity("LBT", AttributeType.Physical, ColumnName = "LBT", ShowControl = true)]
        public int? LBT
        {
            get
            {
                return _LBT;
            }
            set
            {
                if (_LBT == value) return;

                NotifyPropertyChanging("LBT");
                _LBT = value;
                NotifyPropertyChanged("LBT");
            }
        }

        [Entity("SaleVat", AttributeType.Physical, ColumnName = "SaleVat", ShowControl = true)]
        public int? SaleVat
        {
            get
            {
                return _SaleVat;
            }
            set
            {
                if (_SaleVat == value) return;

                NotifyPropertyChanging("SaleVat");
                _SaleVat = value;
                NotifyPropertyChanged("SaleVat");
            }
        }

        [Entity("SaleCst", AttributeType.Physical, ColumnName = "SaleCst", ShowControl = true)]
        public int? SaleCst
        {
            get
            {
                return _SaleCst;
            }
            set
            {
                if (_SaleCst == value) return;

                NotifyPropertyChanging("SaleCst");
                _SaleCst = value;
                NotifyPropertyChanged("SaleCst");
            }
        }

        [Entity("CentralSaleVat", AttributeType.Physical, ColumnName = "CentralSaleVat", ShowControl = true)]
        public int? CentralSaleVat
        {
            get
            {
                return _CentralSaleVat;
            }
            set
            {
                if (_CentralSaleVat == value) return;

                NotifyPropertyChanging("CentralSaleVat");
                _CentralSaleVat = value;
                NotifyPropertyChanged("CentralSaleVat");
            }
        }

        [Entity("StateSaleVat", AttributeType.Physical, ColumnName = "StateSaleVat", ShowControl = true)]
        public int? StateSaleVat
        {
            get
            {
                return _StateSaleVat;
            }
            set
            {
                if (_StateSaleVat == value) return;

                NotifyPropertyChanging("StateSaleVat");
                _StateSaleVat = value;
                NotifyPropertyChanged("StateSaleVat");
            }
        }

        [Entity("CentralPurchaseVat", AttributeType.Physical, ColumnName = "CentralPurchaseVat", ShowControl = true)]
        public int? CentralPurchaseVat
        {
            get
            {
                return _CentralPurchaseVat;
            }
            set
            {
                if (_CentralPurchaseVat == value) return;

                NotifyPropertyChanging("CentralPurchaseVat");
                _CentralPurchaseVat = value;
                NotifyPropertyChanged("CentralPurchaseVat");
            }
        }

        [Entity("StatePurchaseVat", AttributeType.Physical, ColumnName = "StatePurchaseVat", ShowControl = true)]
        public int? StatePurchaseVat
        {
            get
            {
                return _StatePurchaseVat;
            }
            set
            {
                if (_StatePurchaseVat == value) return;

                NotifyPropertyChanging("StatePurchaseVat");
                _StatePurchaseVat = value;
                NotifyPropertyChanged("StatePurchaseVat");
            }
        }

        [Entity("Cost", AttributeType.Physical, ColumnName = "Cost", ShowControl = true)]
        public double? Cost
        {
            get
            {
                return _Cost;
            }
            set
            {
                if (_Cost == value) return;

                NotifyPropertyChanging("Cost");
                _Cost = value;
                NotifyPropertyChanged("Cost");
            }
        }

        [Entity("MRP", AttributeType.Physical, ColumnName = "MRP", ShowControl = true)]
        public double? MRP
        {
            get
            {
                return _MRP;
            }
            set
            {
                if (_MRP == value) return;

                NotifyPropertyChanging("MRP");
                _MRP = value;
                NotifyPropertyChanged("MRP");
            }
        }

        [Entity("PurchaseRate", AttributeType.Physical, ColumnName = "PurchaseRate", ShowControl = true)]
        public double? PurchaseRate
        {
            get
            {
                return _PurchaseRate;
            }
            set
            {
                if (_PurchaseRate == value) return;

                NotifyPropertyChanging("PurchaseRate");
                _PurchaseRate = value;
                NotifyPropertyChanged("PurchaseRate");
            }
        }

        [Entity("RateA", AttributeType.Physical, ColumnName = "RateA", ShowControl = true)]
        public double? RateA
        {
            get
            {
                return _RateA;
            }
            set
            {
                if (_RateA == value) return;

                NotifyPropertyChanging("RateA");
                _RateA = value;
                NotifyPropertyChanged("RateA");
            }
        }

        [Entity("RateB", AttributeType.Physical, ColumnName = "RateB", ShowControl = true)]
        public double? RateB
        {
            get
            {
                return _RateB;
            }
            set
            {
                if (_RateB == value) return;

                NotifyPropertyChanging("RateB");
                _RateB = value;
                NotifyPropertyChanged("RateB");
            }
        }

        [Entity("RateC", AttributeType.Physical, ColumnName = "RateC", ShowControl = true)]
        public double? RateC
        {
            get
            {
                return _RateC;
            }
            set
            {
                if (_RateC == value) return;

                NotifyPropertyChanging("RateC");
                _RateC = value;
                NotifyPropertyChanged("RateC");
            }
        }

        [Entity("Discount", AttributeType.Physical, ColumnName = "Discount", ShowControl = true)]
        public double? Discount
        {
            get
            {
                return _Discount;
            }
            set
            {
                if (_Discount == value) return;

                NotifyPropertyChanging("Discount");
                _Discount = value;
                NotifyPropertyChanged("Discount");
            }
        }

        [Entity("SDiscount", AttributeType.Physical, ColumnName = "SDiscount", ShowControl = true)]
        public double? SDiscount
        {
            get
            {
                return _SDiscount;
            }
            set
            {
                if (_SDiscount == value) return;

                NotifyPropertyChanging("SDiscount");
                _SDiscount = value;
                NotifyPropertyChanged("SDiscount");
            }
        }

        [Entity("MinimumMargin", AttributeType.Physical, ColumnName = "MinimumMargin", ShowControl = true)]
        public double? MinimumMargin
        {
            get
            {
                return _MinimumMargin;
            }
            set
            {
                if (_MinimumMargin == value) return;

                NotifyPropertyChanging("MinimumMargin");
                _MinimumMargin = value;
                NotifyPropertyChanged("MinimumMargin");
            }
        }

        [Entity("Narcotic", AttributeType.Physical, ColumnName = "Narcotic", ShowControl = true)]
        public bool? Narcotic
        {
            get
            {
                return _Narcotic;
            }
            set
            {
                if (_Narcotic == value) return;

                NotifyPropertyChanging("Narcotic");
                _Narcotic = value;
                NotifyPropertyChanged("Narcotic");
            }
        }

        [Entity("ScheduleH", AttributeType.Physical, ColumnName = "ScheduleH", ShowControl = true)]
        public bool? ScheduleH
        {
            get
            {
                return _ScheduleH;
            }
            set
            {
                if (_ScheduleH == value) return;

                NotifyPropertyChanging("ScheduleH");
                _ScheduleH = value;
                NotifyPropertyChanged("ScheduleH");
            }
        }

        [Entity("ScheduleH1", AttributeType.Physical, ColumnName = "ScheduleH1", ShowControl = true)]
        public bool? ScheduleH1
        {
            get
            {
                return _ScheduleH1;
            }
            set
            {
                if (_ScheduleH1 == value) return;

                NotifyPropertyChanging("ScheduleH1");
                _ScheduleH1 = value;
                NotifyPropertyChanged("ScheduleH1");
            }
        }

        [Entity("Narration", AttributeType.Physical, ColumnName = "Narration", ShowControl = true)]
        public string Narration
        {
            get
            {
                return _Narration;
            }
            set
            {
                if (_Narration == value) return;

                NotifyPropertyChanging("Narration");
                _Narration = value;
                NotifyPropertyChanged("Narration");
            }
        }

        [Entity("MedicineType", AttributeType.Physical, ColumnName = "MedicineType", ShowControl = true)]
        public short? MedicineType
        {
            get
            {
                return _MedicineType;
            }
            set
            {
                if (_MedicineType == value) return;

                NotifyPropertyChanging("MedicineType");
                _MedicineType = value;
                NotifyPropertyChanged("MedicineType");
            }
        }

        [Entity("DpcoPrice", AttributeType.Physical, ColumnName = "DpcoPrice", ShowControl = true)]
        public double? DpcoPrice
        {
            get
            {
                return _DpcoPrice;
            }
            set
            {
                if (_DpcoPrice == value) return;

                NotifyPropertyChanging("DpcoPrice");
                _DpcoPrice = value;
                NotifyPropertyChanged("DpcoPrice");
            }
        }

        [Entity("BestBeforeDays", AttributeType.Physical, ColumnName = "BestBeforeDays", ShowControl = true)]
        public int? BestBeforeDays
        {
            get
            {
                return _BestBeforeDays;
            }
            set
            {
                if (_BestBeforeDays == value) return;

                NotifyPropertyChanging("BestBeforeDays");
                _BestBeforeDays = value;
                NotifyPropertyChanged("BestBeforeDays");
            }
        }

        [Entity("SchemeType", AttributeType.Physical, ColumnName = "SchemeType", ShowControl = true)]
        public short? SchemeType
        {
            get
            {
                return _SchemeType;
            }
            set
            {
                if (_SchemeType == value) return;

                NotifyPropertyChanging("SchemeType");
                _SchemeType = value;
                NotifyPropertyChanged("SchemeType");
            }
        }

        [Entity("SchemeQty", AttributeType.Physical, ColumnName = "SchemeQty", ShowControl = true)]
        public double? SchemeQty
        {
            get
            {
                return _SchemeQty;
            }
            set
            {
                if (_SchemeQty == value) return;

                NotifyPropertyChanging("SchemeQty");
                _SchemeQty = value;
                NotifyPropertyChanged("SchemeQty");
            }
        }

        [Entity("SchemeFreeQty", AttributeType.Physical, ColumnName = "SchemeFreeQty", ShowControl = true)]
        public double? SchemeFreeQty
        {
            get
            {
                return _SchemeFreeQty;
            }
            set
            {
                if (_SchemeFreeQty == value) return;

                NotifyPropertyChanging("SchemeFreeQty");
                _SchemeFreeQty = value;
                NotifyPropertyChanged("SchemeFreeQty");
            }
        }

        [Entity("PurchaseDiscount1", AttributeType.Physical, ColumnName = "PurchaseDiscount1", ShowControl = true)]
        public double? PurchaseDiscount1
        {
            get
            {
                return _PurchaseDiscount1;
            }
            set
            {
                if (_PurchaseDiscount1 == value) return;

                NotifyPropertyChanging("PurchaseDiscount1");
                _PurchaseDiscount1 = value;
                NotifyPropertyChanged("PurchaseDiscount1");
            }
        }

        [Entity("PurchaseDiscount2", AttributeType.Physical, ColumnName = "PurchaseDiscount2", ShowControl = true)]
        public double? PurchaseDiscount2
        {
            get
            {
                return _PurchaseDiscount2;
            }
            set
            {
                if (_PurchaseDiscount2 == value) return;

                NotifyPropertyChanging("PurchaseDiscount2");
                _PurchaseDiscount2 = value;
                NotifyPropertyChanged("PurchaseDiscount2");
            }
        }

        [Entity("RateCalculation", AttributeType.Physical, ColumnName = "RateCalculation", ShowControl = true)]
        public short? RateCalculation
        {
            get
            {
                return _RateCalculation;
            }
            set
            {
                if (_RateCalculation == value) return;

                NotifyPropertyChanging("RateCalculation");
                _RateCalculation = value;
                NotifyPropertyChanged("RateCalculation");
            }
        }

        [Entity("PurVolDis", AttributeType.Physical, ColumnName = "PurVolDis", ShowControl = true)]
        public double? PurVolDis
        {
            get
            {
                return _PurVolDis;
            }
            set
            {
                if (_PurVolDis == value) return;

                NotifyPropertyChanging("PurVolDis");
                _PurVolDis = value;
                NotifyPropertyChanged("PurVolDis");
            }
        }

        [Entity("PurVolDisOnQty", AttributeType.Physical, ColumnName = "PurVolDisOnQty", ShowControl = true)]
        public double? PurVolDisOnQty
        {
            get
            {
                return _PurVolDisOnQty;
            }
            set
            {
                if (_PurVolDisOnQty == value) return;

                NotifyPropertyChanging("PurVolDisOnQty");
                _PurVolDisOnQty = value;
                NotifyPropertyChanged("PurVolDisOnQty");
            }
        }

        [Entity("SaleVolDis", AttributeType.Physical, ColumnName = "SaleVolDis", ShowControl = true)]
        public double? SaleVolDis
        {
            get
            {
                return _SaleVolDis;
            }
            set
            {
                if (_SaleVolDis == value) return;

                NotifyPropertyChanging("SaleVolDis");
                _SaleVolDis = value;
                NotifyPropertyChanged("SaleVolDis");
            }
        }

        [Entity("SaleVolDisOnQty", AttributeType.Physical, ColumnName = "SaleVolDisOnQty", ShowControl = true)]
        public double? SaleVolDisOnQty
        {
            get
            {
                return _SaleVolDisOnQty;
            }
            set
            {
                if (_SaleVolDisOnQty == value) return;

                NotifyPropertyChanging("SaleVolDisOnQty");
                _SaleVolDisOnQty = value;
                NotifyPropertyChanged("SaleVolDisOnQty");
            }
        }

        [Entity("TerrifHeading", AttributeType.Physical, ColumnName = "TerrifHeading", ShowControl = true)]
        public string TerrifHeading
        {
            get
            {
                return _TerrifHeading;
            }
            set
            {
                if (_TerrifHeading == value) return;

                NotifyPropertyChanging("TerrifHeading");
                _TerrifHeading = value;
                NotifyPropertyChanged("TerrifHeading");
            }
        }

        [Entity("TerrifDescription", AttributeType.Physical, ColumnName = "TerrifDescription", ShowControl = true)]
        public string TerrifDescription
        {
            get
            {
                return _TerrifDescription;
            }
            set
            {
                if (_TerrifDescription == value) return;

                NotifyPropertyChanging("TerrifDescription");
                _TerrifDescription = value;
                NotifyPropertyChanged("TerrifDescription");
            }
        }

        [Entity("AllowNegativeStock", AttributeType.Physical, ColumnName = "AllowNegativeStock", ShowControl = true)]
        public bool? AllowNegativeStock
        {
            get
            {
                return _AllowNegativeStock;
            }
            set
            {
                if (_AllowNegativeStock == value) return;

                NotifyPropertyChanging("AllowNegativeStock");
                _AllowNegativeStock = value;
                NotifyPropertyChanged("AllowNegativeStock");
            }
        }

        [Entity("CanExpire", AttributeType.Physical, ColumnName = "CanExpire", ShowControl = true)]
        public bool? CanExpire
        {
            get
            {
                return _CanExpire;
            }
            set
            {
                if (_CanExpire == value) return;

                NotifyPropertyChanging("CanExpire");
                _CanExpire = value;
                NotifyPropertyChanged("CanExpire");
            }
        }

        #endregion

        #region Static Helpers

        public static bool IsExists(int id)
        {
            return !(new Fw.Controller.Data.MySqlConnectionManager()).ExecuteScalar("SELECT EXISTS(select 1 from ItemMaster where Id = {0} limit 1)", id).IsNull();
        }

        #endregion
    }
}
