﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;

namespace Fw.Model.Master.Item
{
    /// <summary>
    /// 
    /// </summary>
    public class CategoryMaster : EntityObject
    {
        #region Declaration

        private string _Name;

        private short? _CategoryUse;
        private short? _CategoryPathy;
        private short? _PackType;

        #endregion

        #region Constructor

        public CategoryMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("CategoryMaster", entityManger)
        {  }

        public CategoryMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("CategoryMaster", entityManger, id)
        { }

        #endregion

        #region Entity Properties

        [Entity("Name", AttributeType.Physical, ColumnName = "Name", ShowControl = true, IsRequired = true)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;

                NotifyPropertyChanging("Name");
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// Picklist value is 6
        /// </summary>
        [Entity("CategoryUse", AttributeType.Physical, ColumnName = "CategoryUse", ShowControl = true)]
        public short? CategoryUse
        {
            get
            {
                return _CategoryUse;
            }
            set
            {
                if (_CategoryUse == value) return;

                NotifyPropertyChanging("CategoryUse");
                _CategoryUse = value;
                NotifyPropertyChanged("CategoryUse");
            }
        }

        /// <summary>
        /// Picklist value is 7
        /// </summary>
        [Entity("CategoryPathy", AttributeType.Physical, ColumnName = "CategoryPathy", ShowControl = true)]
        public short? CategoryPathy
        {
            get
            {
                return _CategoryPathy;
            }
            set
            {
                if (_CategoryPathy == value) return;

                NotifyPropertyChanging("CategoryPathy");
                _CategoryPathy = value;
                NotifyPropertyChanged("CategoryPathy");
            }
        }

        /// <summary>
        /// Picklist value is 8
        /// </summary>
        [Entity("PackType", AttributeType.Physical, ColumnName = "PackType", ShowControl = true)]
        public short? PackType
        {
            get
            {
                return _PackType;
            }
            set
            {
                if (_PackType == value) return;

                NotifyPropertyChanging("PackType");
                _PackType = value;
                NotifyPropertyChanged("PackType");
            }
        }

        #endregion
    }
}
