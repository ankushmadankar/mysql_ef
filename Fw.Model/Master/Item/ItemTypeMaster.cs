﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;

namespace Fw.Model.Master.Item
{
    public class ItemTypeMaster : EntityObject
    {
        #region Declaration

        private string _TypeCode;
        private string _TypeDescription;
        private string _ShortName;
        private double? _Discount; 
        
        #endregion

        #region Costructor

        public ItemTypeMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("ItemTypeMaster", entityManger)
        { }

        public ItemTypeMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("ItemTypeMaster", entityManger, id)
        { }

        #endregion

        #region Properties
        
        [Entity("TypeCode", AttributeType.Physical, ColumnName = "TypeCode", IsRequired = true, ShowControl = true)]
        public string TypeCode
        {
            get
            {
                return _TypeCode;
            }
            set
            {
                if (_TypeCode == value) return;

                NotifyPropertyChanging("TypeCode");
                _TypeCode = value;
                NotifyPropertyChanged("TypeCode");
            }
        }

        [Entity("TypeDescription", AttributeType.Physical, ColumnName = "TypeDescription", ShowControl = true)]
        public string TypeDescription
        {
            get
            {
                return _TypeDescription;
            }
            set
            {
                if (_TypeDescription == value) return;

                NotifyPropertyChanging("TypeDescription");
                _TypeDescription = value;
                NotifyPropertyChanged("TypeDescription");
            }
        }

        [Entity("ShortName", AttributeType.Physical, ColumnName = "ShortName", ShowControl = true)]
        public string ShortName
        {
            get
            {
                return _ShortName;
            }
            set
            {
                if (_ShortName == value) return;

                NotifyPropertyChanging("ShortName");
                _ShortName = value;
                NotifyPropertyChanged("ShortName");
            }
        }

        [Entity("Discount", AttributeType.Physical, ColumnName = "Discount", ShowControl = true)]
        public double? Discount
        {
            get
            {
                return _Discount;
            }
            set
            {
                if (_Discount == value) return;

                NotifyPropertyChanging("Discount");
                _Discount = value;
                NotifyPropertyChanged("Discount");
            }
        }

        #endregion
    }
}
