﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;

namespace Fw.Model.Master.Item
{
    /// <summary>
    /// 
    /// </summary>
    public class TaxConfiguration : EntityObject
    {
        #region Declaration

        private string _Name;
        private string _Code;

        private short? _TaxType;
        private double? _Value;
        private string _Formula;

        #endregion

        #region Constructor

        public TaxConfiguration(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("TaxConfiguration", entityManger)
        { }

        public TaxConfiguration(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("TaxConfiguration", entityManger, id)
        { }

        #endregion

        #region Entity Properties

        [Entity("Code", AttributeType.Physical, ColumnName = "Code", ShowControl = true, IsRequired = true)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                if (_Code == value) return;

                NotifyPropertyChanging("Code");
                _Code = value;
                NotifyPropertyChanged("Code");
            }
        }

        [Entity("Name", AttributeType.Physical, ColumnName = "Name", ShowControl = true, IsRequired = true)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;

                NotifyPropertyChanging("Name");
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        [Entity("Formula", AttributeType.Physical, ColumnName = "Formula", ShowControl = true)]
        public string Formula
        {
            get
            {
                return _Formula;
            }
            set
            {
                if (_Formula == value) return;

                NotifyPropertyChanging("Formula");
                _Formula = value;
                NotifyPropertyChanged("Formula");
            }
        }

        /// <summary>
        /// Picklist value is 9.
        /// </summary>
        [Entity("TaxType", AttributeType.Physical, ColumnName = "TaxType", ShowControl = true)]
        public short? TaxType
        {
            get
            {
                return _TaxType;
            }
            set
            {
                if (_TaxType == value) return;

                NotifyPropertyChanging("TaxType");
                _TaxType = value;
                NotifyPropertyChanged("TaxType");
            }
        }

        [Entity("Value", AttributeType.Physical, ColumnName = "Value", ShowControl = true)]
        public double? Value
        {
            get
            {
                return _Value;
            }
            set
            {
                if (_Value == value) return;

                NotifyPropertyChanging("Value");
                _Value = value;
                NotifyPropertyChanged("Value");
            }
        }

        #endregion
    }

    public enum TaxType
    {
        Percentage = 1,
        Fixed = 2
    }
}
