﻿using Fw.Controller.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Extentions;
using Fw.Model.Base;

namespace Fw.Model.Master.Item
{
    public class GenericMaster : MasterEntity
    {
        #region Declaration

        private ItemMasterContract[] _ListOfItems;

        #endregion

        #region Constructor

        public GenericMaster(Fw.Controller.DocumentEntity.EntityManager entityManager)
            : base("GenericMaster", entityManager, DocType.GM)
        { }

        public GenericMaster(Fw.Controller.DocumentEntity.EntityManager entityManager, int id)
            : base("GenericMaster", entityManager, id, DocType.GM)
        { }

        #endregion

        #region Properties

        [Entity("ListOfItems", AttributeType.Other)]
        public ItemMasterContract[] ListOfItems
        {
            get
            {
                return _ListOfItems;
            }
        }

        #endregion

        #region Override Methods

        protected override void OnLoadEntity()
        {
            base.OnLoadEntity();
            //
            RefreshItemList();
        }

        #endregion

        #region Public Methods

        public void RefreshItemList()
        {
            _ListOfItems = MySqlConnectionManagers.ExecuteQuery<ItemMasterContract>("select * from ItemMaster where GenericId = {0}", Id).ToArray();
        }

        /// <summary>
        /// Add item master in mention generics.
        /// </summary>
        /// <param name="item"></param>
        public void AddItemMaster(ItemMaster item)
        {
            if (Id == 0)
                throw new NotSupportedException("Generic yet not saved.");

            item.GenericId = Id;
        }

        /// <summary>
        /// Add item master by id.
        /// </summary>
        /// <param name="itemId">id of item master</param>
        public ItemMaster AddItemMaster(int itemId)
        {
            if (Id == 0)
                throw new ValidationError("Generic yet not saved.");

            if (!ItemMaster.IsExists(itemId))
                throw new ValidationError("Item not exists for mention id.");

            var item = new ItemMaster(EntityManager, itemId);

            item.GenericId = Id;

            return item;
        }

        #endregion
    }

    public class ItemMasterContract
    {
        public int Id { get; set; }
        public string ItemCode { get; set; }
        public string ItemName { get; set; }
        public DateTime? CreationDate { get; set; }
    }
}
