﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;
using Fw.Model.Base;

namespace Fw.Model.Master.Item
{
    public class TaxTypeMaster : MasterEntity
    {
        #region Declaration

        private short? _TaxType;
        private double? _Tax;
        private double? _Surcharge;

        #endregion

        #region Constructor

        public TaxTypeMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("TaxTypeMaster", entityManger, DocType.TM)
        { }

        public TaxTypeMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("TaxTypeMaster", entityManger, id, DocType.TM)
        { }

        #endregion

        #region Entity Properties

        /// <summary>
        /// Picklist value is 9.
        /// </summary>
        [Entity("TaxType", AttributeType.Physical, ColumnName = "TaxType", ShowControl = true, IsRequired = true)]
        public short? TaxType
        {
            get
            {
                return _TaxType;
            }
            set
            {
                if (_TaxType == value) return;

                NotifyPropertyChanging("TaxType");
                _TaxType = value;
                NotifyPropertyChanged("TaxType");
            }
        }

        [Entity("Tax", AttributeType.Physical, ColumnName = "Tax", ShowControl = true)]
        public double? Tax
        {
            get
            {
                return _Tax;
            }
            set
            {
                if (_Tax == value) return;

                NotifyPropertyChanging("Tax");
                _Tax = value;
                NotifyPropertyChanged("Tax");
            }
        }

        [Entity("Surcharge", AttributeType.Physical, ColumnName = "Surcharge", ShowControl = true)]
        public double? Surcharge
        {
            get
            {
                return _Surcharge;
            }
            set
            {
                if (_Surcharge == value) return;

                NotifyPropertyChanging("Surcharge");
                _Surcharge = value;
                NotifyPropertyChanged("Surcharge");
            }
        }

        #endregion
    }
}
