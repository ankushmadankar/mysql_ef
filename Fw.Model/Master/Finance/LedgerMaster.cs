﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;
using Fw.Model.Base;
using Fw.Model.Master.Contact;

namespace Fw.Model.Master.Finance
{
    public class LedgerMaster : MasterEntity
    {
        #region Declaration

        private int? _LedgerGroup;
        private int? _ContactId;
        private short? _BalancingMethod;
        private double? _OpeningBalance;
        private short? _Schedule;
        private int? _PartyId;

        private ExciseDetails _ExciseDetails;
        private DeliveryAddress _DeliveryAddress;
        private RTGSDetails _RTGSDetails;
        private InterestRateDetails _InterestRateDetails;
        private ContactMaster _ContactMaster;
 
        #endregion

        #region Constructor

        public LedgerMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("LedgerMaster", entityManger, DocType.LM)
        { }

        public LedgerMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("LedgerMaster", entityManger, DocType.LM)
        { }

        #endregion

        #region Entity Properties

        /// <summary>
        /// 
        /// </summary>
        [Entity("LedgerGroup", AttributeType.Physical, ColumnName = "LedgerGroup", ShowControl = true)]
        public int? LedgerGroup
        {
            get
            {
                return _LedgerGroup;
            }
            set
            {
                if (_LedgerGroup == value) return;

                NotifyPropertyChanging("LedgerGroup");
                _LedgerGroup = value;
                NotifyPropertyChanged("LedgerGroup");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Entity("ContactId", AttributeType.Physical, ColumnName = "ContactId", ShowControl = true)]
        public int? ContactId
        {
            get
            {
                return _ContactId;
            }
            set
            {
                if (_ContactId == value) return;

                NotifyPropertyChanging("ContactId");
                _ContactId = value;
                NotifyPropertyChanged("ContactId");

                if (IsEntityLoaded && _ContactMaster != null)
                {
                    _ContactMaster.Id = value.GetValueOrDefault();
                    _ContactMaster.RefreshEntity();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Entity("BalancingMethod", AttributeType.Physical, ColumnName = "BalancingMethod", ShowControl = true)]
        public short? BalancingMethod
        {
            get
            {
                return _BalancingMethod;
            }
            set
            {
                if (_BalancingMethod == value) return;

                NotifyPropertyChanging("BalancingMethod");
                _BalancingMethod = value;
                NotifyPropertyChanged("BalancingMethod");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Entity("OpeningBalance", AttributeType.Physical, ColumnName = "OpeningBalance", ShowControl = true)]
        public double? OpeningBalance
        {
            get
            {
                return _OpeningBalance;
            }
            set
            {
                if (_OpeningBalance == value) return;

                NotifyPropertyChanging("OpeningBalance");
                _OpeningBalance = value;
                NotifyPropertyChanged("OpeningBalance");
            }
        }

        [Entity("Schedule", AttributeType.Physical, ColumnName = "Schedule", ShowControl = true)]
        public short? Schedule
        {
            get
            {
                return _Schedule;
            }
            set
            {
                if (_Schedule == value) return;

                NotifyPropertyChanging("Schedule");
                _Schedule = value;
                NotifyPropertyChanged("Schedule");
            }
        }

        /// <summary>
        /// Link party master
        /// </summary>
        [Entity("PartyId", AttributeType.Physical, ColumnName = "PartyId", ShowControl = true)]
        public int? PartyId
        {
            get
            {
                return _PartyId;
            }
            set
            {
                if (_PartyId == value) return;

                NotifyPropertyChanging("PartyId");
                _PartyId = value;
                NotifyPropertyChanged("PartyId");
            }
        }

        #endregion

        #region Public Properties

        public ExciseDetails ExciseDetails
        {
            get { return _ExciseDetails; }
        }

        public DeliveryAddress DeliveryAddress
        {
            get { return _DeliveryAddress; }
        }

        public RTGSDetails RTGSDetails
        {
            get { return _RTGSDetails; }
        }

        public InterestRateDetails InterestRateDetails
        {
            get { return _InterestRateDetails; }
        }

        /// <summary>
        /// Get contact info. Contact is of type party master always.
        /// </summary>
        public ContactMaster ContactMaster
        {
            get { return _ContactMaster; }
        }

        #endregion

        #region Private Methods

        protected override void OnLoadEntity()
        {
            base.OnLoadEntity();

            LoadRelations();
        }

        private void LoadRelations()
        {
            if (ObjectState == EntityState.Added)
            {
                if (_ContactMaster == null)
                    _ContactMaster = new ContactMaster(EntityManager, ContactGroupType.Party);
                else
                    _ContactMaster.RefreshEntity();

                if (_ExciseDetails == null)
                    _ExciseDetails = new ExciseDetails(EntityManager, this);
                else
                    _ExciseDetails.RefreshEntity();

                if (_DeliveryAddress == null)
                    _DeliveryAddress = new DeliveryAddress(EntityManager, this);
                else
                    _DeliveryAddress.RefreshEntity();

                if (_RTGSDetails == null)
                    _RTGSDetails = new RTGSDetails(EntityManager, this);
                else
                    _RTGSDetails.RefreshEntity();

                if (_InterestRateDetails == null)
                    _InterestRateDetails = new InterestRateDetails(EntityManager, this);
                else
                    _InterestRateDetails.RefreshEntity();
            }
            else
            {
                if (_ContactMaster == null)
                    _ContactMaster = new ContactMaster(EntityManager, ContactId.GetValueOrDefault(), ContactGroupType.Party);
                else
                    _ContactMaster.RefreshEntity();

                if (_ExciseDetails == null)
                    _ExciseDetails = new ExciseDetails(EntityManager, GetRelationId(ExciseDetails.RelationTableName), this);
                else
                    _ExciseDetails.RefreshEntity();

                if (_DeliveryAddress == null)
                    _DeliveryAddress = new DeliveryAddress(EntityManager, GetRelationId(DeliveryAddress.RelationTableName), this);

                if (_RTGSDetails == null)
                    _RTGSDetails = new RTGSDetails(EntityManager, GetRelationId(RTGSDetails.RelationTableName), this);
                else
                    _RTGSDetails.RefreshEntity();

                if (_InterestRateDetails == null)
                    _InterestRateDetails = new InterestRateDetails(EntityManager, GetRelationId(InterestRateDetails.RelationTableName), this);
                else
                    _InterestRateDetails.RefreshEntity();
            }
        }

        private int GetRelationId(string tableName)
        {
            var objRelationId = MySqlConnectionManagers.ExecuteScalar("select Id from {0} where ForeignId = {1}", tableName, Id);

            int relationId = default(int);

            if (objRelationId != null)
                relationId = Convert.ToInt32(objRelationId);

            return relationId;
        }

        #endregion
    }
}
