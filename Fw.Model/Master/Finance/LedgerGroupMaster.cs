﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;

namespace Fw.Model.Master.Finance
{
    public class LedgerGroupMaster : EntityObject
    {
         #region Declaration

        private string _Code;
        private string _Name;

        private short? _PrimaryGroup;
        private short? _SecondaryGroup;
        private short? _TertiaryGroup;
        
        #endregion

        #region Constructor

        public LedgerGroupMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("LedgerGroupMaster", entityManger)
        { }

        public LedgerGroupMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("LedgerGroupMaster", entityManger, id)
        { }

        #endregion

        #region Entity Properties

        [Entity("Code", AttributeType.Physical, ColumnName = "Code", ShowControl = true, IsRequired = true)]
        public string Code
        {
            get
            {
                return _Code;
            }
            set
            {
                if (_Code == value) return;

                NotifyPropertyChanging("Code");
                _Code = value;
                NotifyPropertyChanged("Code");
            }
        }

        [Entity("Name", AttributeType.Physical, ColumnName = "Name", ShowControl = true, IsRequired = true)]
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                if (_Name == value) return;

                NotifyPropertyChanging("Name");
                _Name = value;
                NotifyPropertyChanged("Name");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Entity("PrimaryGroup", AttributeType.Physical, ColumnName = "PrimaryGroup", ShowControl = true)]
        public short? PrimaryGroup
        {
            get
            {
                return _PrimaryGroup;
            }
            set
            {
                if (_PrimaryGroup == value) return;

                NotifyPropertyChanging("PrimaryGroup");
                _PrimaryGroup = value;
                NotifyPropertyChanged("PrimaryGroup");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        [Entity("SecondaryGroup", AttributeType.Physical, ColumnName = "SecondaryGroup", ShowControl = true)]
        public short? SecondaryGroup
        {
            get
            {
                return _SecondaryGroup;
            }
            set
            {
                if (_SecondaryGroup == value) return;

                NotifyPropertyChanging("SecondaryGroup");
                _SecondaryGroup = value;
                NotifyPropertyChanged("SecondaryGroup");
            }
        }


        [Entity("TertiaryGroup", AttributeType.Physical, ColumnName = "TertiaryGroup", ShowControl = true)]
        public short? TertiaryGroup
        {
            get
            {
                return _TertiaryGroup;
            }
            set
            {
                if (_TertiaryGroup == value) return;

                NotifyPropertyChanging("TertiaryGroup");
                _TertiaryGroup = value;
                NotifyPropertyChanged("TertiaryGroup");
            }
        }

        #endregion
    }
}
