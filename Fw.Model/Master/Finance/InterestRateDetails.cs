﻿using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Interfaces;
using Fw.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Model.Master.Finance
{
    public class InterestRateDetails : SingleRelationEntity
    {
        IEntity _PrimaryEntity;
        int _ForeignId;

          public InterestRateDetails(EntityManager entityManger, IEntity primaryEntity)
            : base("InterestRateDetails", entityManger, primaryEntity)
        { }

          public InterestRateDetails(EntityManager entityManger, int id, IEntity primaryEntity)
              : base("InterestRateDetails", entityManger, id, primaryEntity)
        { }

          public static string RelationTableName { get { return "InterestRateDetails"; } }
    }
}
