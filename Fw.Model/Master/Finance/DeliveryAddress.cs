﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Model.Base;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Interfaces;
using Fw.Controller.Base;

namespace Fw.Model.Master.Finance
{
    public class DeliveryAddress : SingleRelationEntity
    {
        public DeliveryAddress(EntityManager entityManger, IEntity primaryEntity)
            : base("DeliveryAddress", entityManger, primaryEntity)
        { }

        public DeliveryAddress(EntityManager entityManger, int id, IEntity primaryEntity)
            : base("DeliveryAddress", entityManger, id, primaryEntity)
        { }

        public static string RelationTableName { get { return "DeliveryAddress"; } }
    }
}
