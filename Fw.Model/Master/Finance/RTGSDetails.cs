﻿using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Interfaces;
using Fw.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Model.Master.Finance
{
    public class RTGSDetails : SingleRelationEntity
    {
          public RTGSDetails(EntityManager entityManger, IEntity primaryEntity)
            : base("RTGSDetails", entityManger, primaryEntity)
        { }

          public RTGSDetails(EntityManager entityManger, int id, IEntity primaryEntity)
              : base("RTGSDetails", entityManger, id, primaryEntity)
        { }

          public static string RelationTableName { get { return "RTGSDetails"; } }
    }
}
