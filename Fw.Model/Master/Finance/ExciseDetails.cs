﻿using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Interfaces;
using Fw.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Model.Master.Finance
{
    public class ExciseDetails : SingleRelationEntity
    {

        public ExciseDetails(EntityManager entityManger, IEntity primaryEntity)
            : base("ExciseDetails", entityManger, primaryEntity)
        { }

        public ExciseDetails(EntityManager entityManger, int id, IEntity primaryEntity)
            : base("ExciseDetails", entityManger, id, primaryEntity)
        { }

        public static string RelationTableName { get { return "ExciseDetails"; } }
    }
}
