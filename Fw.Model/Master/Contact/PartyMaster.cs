﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Base;
using Fw.Controller.Interfaces;
using Fw.Controller.DocumentEntity;
using Fw.Model.Base;

namespace Fw.Model.Master.Contact
{
    /// <summary>
    /// Also knows as Manufacturer
    /// </summary>
    public class PartyMaster : SingleRelationEntity
    {
        #region Declaration

        int _ForeignId;
        string _CGST;
        string _SGST;
        DateTime? _SGSTDate;
        string _CSTTIN;
        DateTime? _CSTDate;
        string _DLNo1;
        DateTime? _DLNo1ExpDate;

        string _DLNo2;
        DateTime? _DLNo2ExpDate;
        string _FoodLicenceNo;
        DateTime? _FoodLicenceNoExpDate;

        string _VATTIN;
        DateTime? _VATDate;

        DateTime? _CGSTDate;
       
        string _SSID;
        string _LumpSum;
        string _Method;
        string _SWLicNo;
        short? _BillingMethod;
        short? _Category;
        short? _NacroHBilling;
        double? _Commission;
       
        private ContactMaster _ContactMaster;

        #endregion

        #region Constructor

        public PartyMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, IEntity primaryEntity)
            : base("PartyMaster", entityManger, primaryEntity)
        { 
        }

        public PartyMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id, IEntity primaryEntity)
            : base("PartyMaster", entityManger, id, primaryEntity)
        { }

        #endregion

        #region Entity Properties

        [Entity("ForeignId", AttributeType.Physical, ColumnName = "ForeignId", ShowControl = true)]
        public int ForeignId
        {
            get
            {
                return _ForeignId;
            }
            set
            {
                if (_ForeignId == value) return;

                NotifyPropertyChanging("ForeignId");
                _ForeignId = value;
                NotifyPropertyChanged("ForeignId");
            }
        }

        [Entity("CGST", AttributeType.Physical, ColumnName = "CGST", ShowControl = true)]
        public string CGST
        {
            get
            {
                return _CGST;
            }
            set
            {
                if (_CGST == value) return;

                NotifyPropertyChanging("CGST");
                _CGST = value;
                NotifyPropertyChanged("CGST");
            }
        }

        [Entity("CGSTDate", AttributeType.Physical, ColumnName = "CGSTDate", ShowControl = true)]
        public DateTime? CGSTDate
        {
            get
            {
                return _CGSTDate;
            }
            set
            {
                if (_CGSTDate == value) return;

                NotifyPropertyChanging("CGSTDate");
                _CGSTDate = value;
                NotifyPropertyChanged("CGSTDate");
            }
        }

        [Entity("VATTIN", AttributeType.Physical, ColumnName = "VATTIN", ShowControl = true)]
        public string VATTIN
        {
            get
            {
                return _VATTIN;
            }
            set
            {
                if (_VATTIN == value) return;

                NotifyPropertyChanging("VATTIN");
                _VATTIN = value;
                NotifyPropertyChanged("VATTIN");
            }
        }

        [Entity("VATDate", AttributeType.Physical, ColumnName = "VATDate", ShowControl = true)]
        public DateTime? VATDate
        {
            get
            {
                return _VATDate;
            }
            set
            {
                if (_VATDate == value) return;

                NotifyPropertyChanging("VATDate");
                _VATDate = value;
                NotifyPropertyChanged("VATDate");
            }
        }
        
        [Entity("SGST", AttributeType.Physical, ColumnName = "SGST", ShowControl = true)]
        public string SGST
        {
            get
            {
                return _SGST;
            }
            set
            {
                if (_SGST == value) return;

                NotifyPropertyChanging("SGST");
                _SGST = value;
                NotifyPropertyChanged("SGST");
            }
        }
        
        [Entity("SGSTDate", AttributeType.Physical, ColumnName = "SGSTDate", ShowControl = true)]
        public DateTime? SGSTDate
        {
            get
            {
                return _SGSTDate;
            }
            set
            {
                if (_SGSTDate == value) return;

                NotifyPropertyChanging("SGSTDate");
                _SGSTDate = value;
                NotifyPropertyChanged("SGSTDate");
            }
        }
        
        [Entity("CSTTIN", AttributeType.Physical, ColumnName = "CSTTIN", ShowControl = true)]
        public string CSTTIN
        {
            get
            {
                return _CSTTIN;
            }
            set
            {
                if (_CSTTIN == value) return;

                NotifyPropertyChanging("CSTTIN");
                _CSTTIN = value;
                NotifyPropertyChanged("CSTTIN");
            }
        }

        [Entity("CSTDate", AttributeType.Physical, ColumnName = "CSTDate", ShowControl = true)]
        public DateTime? CSTDate
        {
            get
            {
                return _CSTDate;
            }
            set
            {
                if (_CSTDate == value) return;

                NotifyPropertyChanging("CSTDate");
                _CSTDate = value;
                NotifyPropertyChanged("CSTDate");
            }
        }
        
        [Entity("DLNo1", AttributeType.Physical, ColumnName = "DLNo1", ShowControl = true)]
        public string DLNo1
        {
            get
            {
                return _DLNo1;
            }
            set
            {
                if (_DLNo1 == value) return;

                NotifyPropertyChanging("DLNo1");
                _DLNo1 = value;
                NotifyPropertyChanged("DLNo1");
            }
        }
       
        [Entity("DLNo1ExpDate", AttributeType.Physical, ColumnName = "DLNo1ExpDate", ShowControl = true)]
        public DateTime? DLNo1ExpDate
        {
            get
            {
                return _DLNo1ExpDate;
            }
            set
            {
                if (_DLNo1ExpDate == value) return;

                NotifyPropertyChanging("DLNo1ExpDate");
                _DLNo1ExpDate = value;
                NotifyPropertyChanged("DLNo1ExpDate");
            }
        }

        [Entity("DLNo2", AttributeType.Physical, ColumnName = "DLNo2", ShowControl = true)]
        public string DLNo2
        {
            get
            {
                return _DLNo2;
            }
            set
            {
                if (_DLNo2 == value) return;

                NotifyPropertyChanging("DLNo2");
                _DLNo2 = value;
                NotifyPropertyChanged("DLNo2");
            }
        }

        [Entity("DLNo2ExpDate", AttributeType.Physical, ColumnName = "DLNo2ExpDate", ShowControl = true)]
        public DateTime? DLNo2ExpDate
        {
            get
            {
                return _DLNo2ExpDate;
            }
            set
            {
                if (_DLNo2ExpDate == value) return;

                NotifyPropertyChanging("DLNo2ExpDate");
                _DLNo2ExpDate = value;
                NotifyPropertyChanged("DLNo2ExpDate");
            }
        }

        [Entity("FoodLicenceNo", AttributeType.Physical, ColumnName = "FoodLicenceNo", ShowControl = true)]
        public string FoodLicenceNo
        {
            get
            {
                return _FoodLicenceNo;
            }
            set
            {
                if (_FoodLicenceNo == value) return;

                NotifyPropertyChanging("FoodLicenceNo");
                _FoodLicenceNo = value;
                NotifyPropertyChanged("FoodLicenceNo");
            }
        }

        [Entity("FoodLicenceNoExpDate", AttributeType.Physical, ColumnName = "FoodLicenceNoExpDate", ShowControl = true)]
        public DateTime? FoodLicenceNoExpDate
        {
            get
            {
                return _FoodLicenceNoExpDate;
            }
            set
            {
                if (_FoodLicenceNoExpDate == value) return;

                NotifyPropertyChanging("FoodLicenceNoExpDate");
                _FoodLicenceNoExpDate = value;
                NotifyPropertyChanged("FoodLicenceNoExpDate");
            }
        }
       
        [Entity("SSID", AttributeType.Physical, ColumnName = "SSID", ShowControl = true)]
        public string SSID
        {
            get
            {
                return _SSID;
            }
            set
            {
                if (_SSID == value) return;

                NotifyPropertyChanging("SSID");
                _SSID = value;
                NotifyPropertyChanged("SSID");
            }
        }
       
        [Entity("LumpSum", AttributeType.Physical, ColumnName = "LumpSum", ShowControl = true)]
        public string LumpSum
        {
            get
            {
                return _LumpSum;
            }
            set
            {
                if (_LumpSum == value) return;

                NotifyPropertyChanging("LumpSum");
                _LumpSum = value;
                NotifyPropertyChanged("LumpSum");
            }
        }
     
        [Entity("Method", AttributeType.Physical, ColumnName = "Method", ShowControl = true)]
        public string Method
        {
            get
            {
                return _Method;
            }
            set
            {
                if (_Method == value) return;

                NotifyPropertyChanging("Method");
                _Method = value;
                NotifyPropertyChanged("Method");
            }
        }

        [Entity("SWLicNo", AttributeType.Physical, ColumnName = "SWLicNo", ShowControl = true)]
        public string SWLicNo
        {
            get
            {
                return _SWLicNo;
            }
            set
            {
                if (_SWLicNo == value) return;

                NotifyPropertyChanging("SWLicNo");
                _SWLicNo = value;
                NotifyPropertyChanged("SWLicNo");
            }
        }
       
        [Entity("BillingMethod", AttributeType.Physical, ColumnName = "BillingMethod", ShowControl = true)]
        public short? BillingMethod
        {
            get
            {
                return _BillingMethod;
            }
            set
            {
                if (_BillingMethod == value) return;

                NotifyPropertyChanging("BillingMethod");
                _BillingMethod = value;
                NotifyPropertyChanged("BillingMethod");
            }
        }
        
        [Entity("Category", AttributeType.Physical, ColumnName = "Category", ShowControl = true)]
        public short? Category
        {
            get
            {
                return _Category;
            }
            set
            {
                if (_Category == value) return;

                NotifyPropertyChanging("Category");
                _Category = value;
                NotifyPropertyChanged("Category");
            }
        }
        
        [Entity("NacroHBilling", AttributeType.Physical, ColumnName = "NacroHBilling", ShowControl = true)]
        public short? NacroHBilling
        {
            get
            {
                return _NacroHBilling;
            }
            set
            {
                if (_NacroHBilling == value) return;

                NotifyPropertyChanging("NacroHBilling");
                _NacroHBilling = value;
                NotifyPropertyChanged("NacroHBilling");
            }
        }

        [Entity("Commission", AttributeType.Physical, ColumnName = "Commission", ShowControl = true)]
        public double? Commission
        {
            get
            {
                return _Commission;
            }
            set
            {
                if (_Commission == value) return;

                NotifyPropertyChanging("Commission");
                _Commission = value;
                NotifyPropertyChanged("Commission");
            }
        }

        #endregion

        public static string RelationTableName { get { return "PartyMaster"; } }
    }
}

