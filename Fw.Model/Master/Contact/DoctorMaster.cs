﻿using Fw.Controller.Base;
using Fw.Controller.Interfaces;
using Fw.Model.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Model.Master.Contact
{
    public class DoctorMaster : SingleRelationEntity
    {
        #region Declaration

        private string _Qualification;
        private string _Specialty;
        private string _Timing;

        #endregion

        #region Constructor

        public DoctorMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, IEntity primaryEntity)
            : base("DoctorMaster", entityManger, primaryEntity)
        { }

        public DoctorMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id, IEntity primaryEntity)
            : base("DoctorMaster", entityManger, id, primaryEntity)
        { }

        #endregion

        #region Entity Properties

        [Entity("Qualification", AttributeType.Physical, ColumnName = "Qualification", ShowControl = true)]
        public string Qualification
        {
            get
            {
                return _Qualification;
            }
            set
            {
                if (_Qualification == value) return;

                NotifyPropertyChanging("Qualification");
                _Qualification = value;
                NotifyPropertyChanged("Qualification");
            }
        }

        [Entity("Specialty", AttributeType.Physical, ColumnName = "Specialty", ShowControl = true)]
        public string Specialty
        {
            get
            {
                return _Specialty;
            }
            set
            {
                if (_Specialty == value) return;

                NotifyPropertyChanging("Specialty");
                _Specialty = value;
                NotifyPropertyChanged("Specialty");
            }
        }

        [Entity("Timing", AttributeType.Physical, ColumnName = "Timing", ShowControl = true)]
        public string Timing
        {
            get
            {
                return _Timing;
            }
            set
            {
                if (_Timing == value) return;

                NotifyPropertyChanging("Timing");
                _Timing = value;
                NotifyPropertyChanged("Timing");
            }
        }

        #endregion

        public static string RelationTableName { get { return "DoctorMaster"; } }
    }
}
