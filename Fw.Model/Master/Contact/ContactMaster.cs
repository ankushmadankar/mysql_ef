﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Model.Base;
using Fw.Controller.Base;

namespace Fw.Model.Master.Contact
{
    public class ContactMaster : MasterEntity
    {
        #region Declaration

        private short _ContactGroupType;
        private DateTime _CreationDate;
        private string _Address1;
        private string _Address2;
        private string _Address3;
        private short? _City;
        private short? _Country;
        private short? _State;
        private string _AreaPinNo;
        private string _PhoneNo1;
        private string _PhoneNo2;
        private string _MobileNo;
        private string _EmailAddress;
        private string _STDCode;
        private string _Fax;
        private string _Web;
        private string _Person;
        private string _Designation;

        private PartyMaster _PartyMaster;
        private DoctorMaster _DoctorMaster;

        #endregion

        #region Contructor

        public ContactMaster(Fw.Controller.DocumentEntity.EntityManager entityManger)
            : base("ContactMaster", entityManger, DocType.CM)
        { }

        public ContactMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id)
            : base("ContactMaster", entityManger, id, DocType.CM)
        { }

        public ContactMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, ContactGroupType type)
            : base("ContactMaster", entityManger, DocType.CM)
        {
            LoadRelations(type);
        }

        public ContactMaster(Fw.Controller.DocumentEntity.EntityManager entityManger, int id, ContactGroupType type)
            : base("ContactMaster", entityManger, id, DocType.CM)
        {
            LoadRelations(type);
        }

        #endregion

        #region Entity Properties

        [Entity("CreationDate", AttributeType.Physical, ColumnName = "CreationDate", ShowControl = true)]
        public DateTime CreationDate
        {
            get
            {
                return _CreationDate;
            }
            set
            {
                if (_CreationDate == value) return;

                NotifyPropertyChanging("CreationDate");
                _CreationDate = value;
                NotifyPropertyChanged("CreationDate");
            }
        }

        [Entity("ContactGroupType", AttributeType.Physical, ColumnName = "ContactGroupType", IsRequired = true, ShowControl = true)]
        public virtual short ContactGroupType
        {
            get
            {
                return _ContactGroupType;
            }
            set
            {
                if (_ContactGroupType == value) return;

                NotifyPropertyChanging("ContactGroupType");
                _ContactGroupType = value;
                NotifyPropertyChanged("ContactGroupType");
            }
        }

        [Entity("Address1", AttributeType.Physical, ColumnName = "Address1", ShowControl = true)]
        public string Address1
        {
            get
            {
                return _Address1;
            }
            set
            {
                if (_Address1 == value) return;

                NotifyPropertyChanging("Address1");
                _Address1 = value;
                NotifyPropertyChanged("Address1");
            }
        }

        [Entity("Address2", AttributeType.Physical, ColumnName = "Address2", ShowControl = true)]
        public string Address2
        {
            get
            {
                return _Address2;
            }
            set
            {
                if (_Address2 == value) return;

                NotifyPropertyChanging("Address2");
                _Address2 = value;
                NotifyPropertyChanged("Address2");
            }
        }

        [Entity("Address3", AttributeType.Physical, ColumnName = "Address3", ShowControl = true)]
        public string Address3
        {
            get
            {
                return _Address3;
            }
            set
            {
                if (_Address3 == value) return;

                NotifyPropertyChanging("Address3");
                _Address3 = value;
                NotifyPropertyChanged("Address3");
            }
        }

        [Entity("City", AttributeType.Physical, ColumnName = "City", ShowControl = true)]
        public short? City
        {
            get
            {
                return _City;
            }
            set
            {
                if (_City == value) return;

                NotifyPropertyChanging("City");
                _City = value;
                NotifyPropertyChanged("City");
            }
        }

        [Entity("State", AttributeType.Physical, ColumnName = "State", ShowControl = true)]
        public short? State
        {
            get
            {
                return _State;
            }
            set
            {
                if (_State == value) return;

                NotifyPropertyChanging("State");
                _State = value;
                NotifyPropertyChanged("State");
            }
        }

        [Entity("Country", AttributeType.Physical, ColumnName = "Country", ShowControl = true)]
        public short? Country
        {
            get
            {
                return _Country;
            }
            set
            {
                if (_Country == value) return;

                NotifyPropertyChanging("Country");
                _Country = value;
                NotifyPropertyChanged("Country");
            }
        }

        [Entity("AreaPinNo", AttributeType.Physical, ColumnName = "AreaPinNo", Tag = "Pin no", ShowControl = true)]
        public string AreaPinNo
        {
            get
            {
                return _AreaPinNo;
            }
            set
            {
                if (_AreaPinNo == value) return;

                NotifyPropertyChanging("AreaPinNo");
                _AreaPinNo = value;
                NotifyPropertyChanged("AreaPinNo");
            }
        }

        [Entity("PhoneNo1", AttributeType.Physical, ColumnName = "PhoneNo1", ShowControl = true)]
        public string PhoneNo1
        {
            get
            {
                return _PhoneNo1;
            }
            set
            {
                if (_PhoneNo1 == value) return;

                NotifyPropertyChanging("PhoneNo1");
                _PhoneNo1 = value;
                NotifyPropertyChanged("PhoneNo1");
            }
        }

        [Entity("PhoneNo2", AttributeType.Physical, ColumnName = "PhoneNo2", ShowControl = true)]
        public string PhoneNo2
        {
            get
            {
                return _PhoneNo2;
            }
            set
            {
                if (_PhoneNo2 == value) return;

                NotifyPropertyChanging("PhoneNo2");
                _PhoneNo2 = value;
                NotifyPropertyChanged("PhoneNo2");
            }
        }

        [Entity("MobileNo", AttributeType.Physical, ColumnName = "MobileNo", ShowControl = true)]
        public string MobileNo
        {
            get
            {
                return _MobileNo;
            }
            set
            {
                if (_MobileNo == value) return;

                NotifyPropertyChanging("MobileNo");
                _MobileNo = value;
                NotifyPropertyChanged("MobileNo");
            }
        }

        [Entity("EmailAddress", AttributeType.Physical, ColumnName = "EmailAddress", ShowControl = true)]
        public string EmailAddress
        {
            get
            {
                return _EmailAddress;
            }
            set
            {
                if (_EmailAddress == value) return;

                NotifyPropertyChanging("EmailAddress");
                _EmailAddress = value;
                NotifyPropertyChanged("EmailAddress");
            }
        }

        [Entity("STDCode", AttributeType.Physical, ColumnName = "STDCode", ShowControl = true)]
        public string STDCode
        {
            get
            {
                return _STDCode;
            }
            set
            {
                if (_STDCode == value) return;

                NotifyPropertyChanging("STDCode");
                _STDCode = value;
                NotifyPropertyChanged("STDCode");
            }

        }

        [Entity("Fax", AttributeType.Physical, ColumnName = "Fax", ShowControl = true)]
        public string Fax
        {
            get
            {
                return _Fax;
            }
            set
            {
                if (_Fax == value) return;

                NotifyPropertyChanging("Fax");
                _Fax = value;
                NotifyPropertyChanged("Fax");
            }
        }

        [Entity("Web", AttributeType.Physical, ColumnName = "Web", ShowControl = true)]
        public string Web
        {
            get
            {
                return _Web;
            }
            set
            {
                if (_Web == value) return;

                NotifyPropertyChanging("Web");
                _Web = value;
                NotifyPropertyChanged("Web");
            }
        }

        [Entity("Person", AttributeType.Physical, ColumnName = "Person", ShowControl = true)]
        public string Person
        {
            get
            {
                return _Person;
            }
            set
            {
                if (_Person == value) return;

                NotifyPropertyChanging("Person");
                _Person = value;
                NotifyPropertyChanged("Person");
            }
        }

        [Entity("Designation", AttributeType.Physical, ColumnName = "Designation", ShowControl = true)]
        public string Designation
        {
            get
            {
                return _Designation;
            }
            set
            {
                if (_Designation == value) return;

                NotifyPropertyChanging("Designation");
                _Designation = value;
                NotifyPropertyChanged("Designation");
            }
        }

        #endregion

        #region Public Properties

        public PartyMaster PartyMaster { get { return _PartyMaster; } }

        public DoctorMaster DoctorMaster { get { return _DoctorMaster; } }

        #endregion

        #region Private Methods

        private void LoadRelations(ContactGroupType type)
        {
            if (ObjectState == EntityState.Added)
                ContactGroupType = (short)type;

            switch (type)
            {
                case Contact.ContactGroupType.Party:

                    if (_PartyMaster == null)
                    {
                        int id = GetRelationId(PartyMaster.RelationTableName);

                        if (id > 0)
                            _PartyMaster = new PartyMaster(EntityManager, GetRelationId(PartyMaster.RelationTableName), this);
                        else
                            _PartyMaster = new PartyMaster(EntityManager, this);
                    }
                    else
                        _PartyMaster.RefreshEntity();

                    break;

                case Contact.ContactGroupType.Doctor:

                    if (_DoctorMaster != null)
                    {
                        int id = GetRelationId(DoctorMaster.RelationTableName);

                        if (id > 0)
                            _DoctorMaster = new DoctorMaster(EntityManager, GetRelationId(DoctorMaster.RelationTableName), this);
                        else
                            _DoctorMaster = new DoctorMaster(EntityManager, this);
                    }
                    else
                        _DoctorMaster.RefreshEntity();

                    break;
            }
        }

        private int GetRelationId(string tableName)
        {
            var objRelationId = MySqlConnectionManagers.ExecuteScalar("select Id from {0} where ForeignId = {1}", tableName, Id);

            int relationId = default(int);

            if (objRelationId != null)
                relationId = Convert.ToInt32(objRelationId);

            return relationId;
        }

        #endregion
    }

    /// <summary>
    /// Contact Group to identity group in which contact is belong.
    /// </summary>
    public enum ContactGroupType : short
    {
        Customer = 1,
        Supplier = 2,
        Doctor = 3,
        Manufacturer = 4,
        Party = 5,
        Employee = 6,
        Other = 7
    }
}
