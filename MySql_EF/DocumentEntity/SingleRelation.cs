﻿using Fw.Controller.Base;
using Fw.Controller.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;

namespace Fw.Controller.DocumentEntity
{
    /// <summary>
    /// This class is able to handle one on one relation between entities.
    /// </summary>
    /// <typeparam name="IPrimaryEntity">Primary entity type which is IEntity</typeparam>
    /// <typeparam name="IForeignEntity">Foreign entity type which is IForeignRelation</typeparam>
    public class SingleRelation
    {
        #region Declarations

        private IEntity _PrimaryEntity;
        private IForeignRelation _ForeingEntity;

        #endregion

        #region Properties

        public IEntity PrimaryEntity
        {
            get { return _PrimaryEntity; }
        }

        public IForeignRelation ForeingEntity
        {
            get { return _ForeingEntity; }
        }

        #endregion

        #region Constructor

        public SingleRelation(IEntity primaryEntity, IForeignRelation foreignEntity)
        {
            if (!(primaryEntity is EntityObject) || !(foreignEntity is EntityObject))
                throw new NotSupportedException("Primary and secondary object need to be entity object.");

            _PrimaryEntity = primaryEntity;
            _ForeingEntity = foreignEntity;

            (primaryEntity as EntityObject).BeginSavingDataEntity += PrimaryEntity_BeginSavingDataEntity;
            (primaryEntity as EntityObject).EndSavingDataEntity += PrimaryEntity_EndSavingDataEntity;
        }

        #endregion

        #region Entity Events

        private void PrimaryEntity_BeginSavingDataEntity(object sender, BeginSavingDataEntity e)
        {
            if ((PrimaryEntity as EntityObject).ObjectState == EntityState.Deleted)
            {
                ForeingEntity.MarkedDelete = true;
                ForeingEntity.Save(e.Command.Connection as MySqlConnection, e.Transaction as MySqlTransaction);
            }
        }

        private void PrimaryEntity_EndSavingDataEntity(object sender, EndSavingDataEntity e)
        {
            if ((ForeingEntity as EntityObject).ObjectState == EntityState.Added)
                ForeingEntity.ForeignId = e.GeneratedId;

            if ((ForeingEntity as EntityObject).ObjectState != EntityState.Deleted || (ForeingEntity as EntityObject).ObjectState != EntityState.Detached)
                ForeingEntity.Save(e.Command.Connection as MySqlConnection, e.Transaction as MySqlTransaction);
        }

        #endregion
    }
}

