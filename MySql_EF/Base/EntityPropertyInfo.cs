﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Fw.Controller.Extentions;

namespace Fw.Controller.Base
{
    public class EntityPropertyInfo : IDisposable
    {
        private readonly string _PropetyName;
        private readonly EntityObject _EntityObject;
        private readonly object _OriginalPropertyValue;

        public EntityPropertyInfo(EntityObject entity, string propName)
        {
            if (entity.IsNull() || propName.IsNull() || entity.EntityManager.IsNull())
                throw new ArgumentNullException("Entity/Property/EntityManager can not be null.");

            _PropetyName = propName;
            _EntityObject = entity;
            _OriginalPropertyValue = Entity.GetPropValue(propName);

            AttributeInfo = entity.GetType().GetProperty(propName).GetCustomAttributes(typeof(EntityAttribute), true).FirstOrDefault() as EntityAttribute;

            UnBingEntityEvents();
            BingEntityEvents();
        }

        void EntityObject_PropertyChanging(object sender, PropertyChangingEventArgs e)
        {
            if (e.PropertyName == PropertyName && sender is EntityObject && (sender as EntityObject).IsEntityLoaded)
            {
                OldPropertyValue = (sender as EntityObject).GetPropValue(PropertyName);
                PropertyState = EntityPropertyState.Changing;
            }
        }

        void EntityObject_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == PropertyName && sender is EntityObject && (sender as EntityObject).IsEntityLoaded)
            {
                NewPropertyValue = (sender as EntityObject).GetPropValue(PropertyName);
                PropertyState = EntityPropertyState.Changed;
            }
        }

        void EntityManager_EndSavingChanges(object sender, EndSavingChangesEventArgs e)
        {
            var prop = Entity.EntityProperties.Where(a => a.PropertyName == PropertyName && a.PropertyState == EntityPropertyState.Changed).FirstOrDefault();

            if (prop.IsNull()) return;

            prop.PropertyState = EntityPropertyState.Added;
        }

        public string PropertyName { get { return _PropetyName; } }
        public EntityObject Entity { get { return _EntityObject; } }

        public object OriginalPropertyValue { get { return _OriginalPropertyValue; } }
        public object OldPropertyValue { get; private set; }
        public object NewPropertyValue { get; private set; }
        public EntityPropertyState PropertyState { get; private set; }
        public EntityAttribute AttributeInfo { get; private set; }

        private void BingEntityEvents()
        {
            Entity.PropertyChanging += new PropertyChangingEventHandler(EntityObject_PropertyChanging);
            Entity.PropertyChanged += new PropertyChangedEventHandler(EntityObject_PropertyChanged);
            Entity.EntityManager.EndSavingChanges += new EndSavingChangesEventHadler(EntityManager_EndSavingChanges);
        }

        private void UnBingEntityEvents()
        {
            Entity.PropertyChanging -= new PropertyChangingEventHandler(EntityObject_PropertyChanging);
            Entity.PropertyChanged -= new PropertyChangedEventHandler(EntityObject_PropertyChanged);
            Entity.EntityManager.EndSavingChanges -= new EndSavingChangesEventHadler(EntityManager_EndSavingChanges);

        }

        public void Dispose()
        {
            UnBingEntityEvents();
        }
    }

    public enum EntityPropertyState : short
    {
        Unchange = 0,
        Changing,
        Changed,
        Added
    }
}
