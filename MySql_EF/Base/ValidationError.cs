﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Fw.Controller.Base
{
    public class ValidationError : Exception
    {
        public ValidationError()
            : this("Error","")
        {
        }

        public ValidationError(string title, string message)
            : base(message)
        {
            MessageBox.Show(message, title, MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        public ValidationError(string message)
            : this("Error", message)
        {
        }
    }
}
