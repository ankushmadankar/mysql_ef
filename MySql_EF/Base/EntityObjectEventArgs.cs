﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Fw.Controller.Data;

namespace Fw.Controller.Base
{
    public class EndSavingChangesEventArgs : EventArgs
    {
        public EndSavingChangesEventArgs() { }
        public EndSavingChangesEventArgs(int generateId, IDbCommand command, IDbTransaction transaction)
        {
            GeneratedId = generateId;
            Command = command;
            Transaction = transaction;
        }

        public int GeneratedId { get; private set; }
        public IDbCommand Command { get; private set; }
        public IDbTransaction Transaction { get; private set; }
    }

    public class EndSavingDataEntity : EventArgs
    {
        public EndSavingDataEntity(int generateId, IDbCommand command, IDbTransaction transaction)
        {
            GeneratedId = generateId;
            Command = command;
            Transaction = transaction;
        }

        public int GeneratedId { get; private set; }
        public IDbCommand Command { get; private set; }
        public IDbTransaction Transaction { get; private set; }
    }

    public class BeginSavingDataEntity : EventArgs
    {
        public BeginSavingDataEntity(IDbCommand command, IDbTransaction transaction)
        {
            Command = command;
            Transaction = transaction;
        }

        public IDbCommand Command { get; private set; }
        public IDbTransaction Transaction { get; private set; }
    }

    public class NonSavableReasons : EventArgs
    {
        public Dictionary<string, string> Reasons { get; set; }
    }
}