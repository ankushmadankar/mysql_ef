﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Fw.Controller.Interfaces
{
    public interface ICustomControl
    {
        bool IsMandatory { get; set; }

        string LabelName { get; set; }

        string SourcePropertyName { get; set; }

        string BindingPropertyName { get; }
    }
}
