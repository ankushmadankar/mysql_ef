﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.Interfaces
{
    public interface ILineEntity : IEntity
    {
        int HeaderId { get; set; }

        Fw.Controller.DocumentEntity.HeaderEntity<ILineEntity> HeaderEntity { get; }
    }
}
