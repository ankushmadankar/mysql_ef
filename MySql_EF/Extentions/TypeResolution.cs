﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.ComponentModel.Design;
using System.Collections.Concurrent;

namespace Fw.Controller.Extentions
{
    public class TypeResolution : ITypeResolutionService
    {

        #region Declarations

        private static readonly TypeResolution _TypeResolutionService;
        private static readonly ConcurrentDictionary<string, Assembly> _Assemblies;
        private static readonly ConcurrentDictionary<string, Type> _Types;

        #endregion

        #region Public Static Functions

        static TypeResolution()
        {
            _Types = new ConcurrentDictionary<string, Type>();
            _Assemblies = new ConcurrentDictionary<string, Assembly>();
            _TypeResolutionService = new TypeResolution();
        }

        public static TypeResolution TypeResolutionService
        {
            get
            {
                return _TypeResolutionService;
            }
        }

        public Type GetType(string assemblyName, string typeName)
        {
            if (!string.IsNullOrEmpty(assemblyName))
                TypeResolutionService.ReferenceAssembly(assemblyName);
            return TypeResolutionService.GetType(typeName, false, false);
        }

        public object GetObject(string assemblyName, string typeName)
        {
            if (string.IsNullOrEmpty(typeName))
                return null;
            //
            if (!string.IsNullOrEmpty(assemblyName))
                TypeResolutionService.ReferenceAssembly(assemblyName);
            return CreateInstance(GetType(typeName, false, false));
        }

        public object GetObject(string assemblyName, string typeName, object param1)
        {
            if (string.IsNullOrEmpty(typeName))
                return null;
            //
            if (!string.IsNullOrEmpty(assemblyName))
                TypeResolutionService.ReferenceAssembly(assemblyName);

            return CreateInstance(GetType(typeName, false, false), param1);
        }

        public object GetObject(string assemblyName, string typeName, object param1, object param2)
        {
            if (string.IsNullOrEmpty(typeName))
                return null;
            //
            if (!string.IsNullOrEmpty(assemblyName))
                TypeResolutionService.ReferenceAssembly(assemblyName);

            return CreateInstance(GetType(typeName, false, false), param1, param2);
        }

        public object GetObject(string assemblyName, string typeName, Type[] types, params object[] args)
        {
            if (string.IsNullOrEmpty(typeName))
                return null;
            //
            if (!string.IsNullOrEmpty(assemblyName))
                TypeResolutionService.ReferenceAssembly(assemblyName);

            return CreateInstance(GetType(typeName, false, false), types, args);
        }

        #endregion

        #region Public Functions

        public void ReferenceAssembly(string name)
        {
            if (!_Assemblies.ContainsKey(name))
                _Assemblies.TryAdd(name, Assembly.Load(name));
        }

        #endregion

        #region Implementation of ITypeResolutionService

        public void ReferenceAssembly(AssemblyName name)
        {
            if (!_Assemblies.ContainsKey(name.Name))
                _Assemblies.TryAdd(name.Name, Assembly.Load(name));
        }

        public Assembly GetAssembly(AssemblyName name, bool throwOnError)
        {
            var assembly = (Assembly)null;
            //
            if (_Assemblies != null && _Assemblies.TryGetValue(name.Name, out assembly))
                return assembly;
            //
            if ((assembly == null) && throwOnError)
                throw new Exception(String.Format("Assembly {0} not found in referenced assemblies.", name.Name));
            //
            return assembly;
        }

        public Type GetType(string name, bool throwOnError, bool ignoreCase)
        {
            var type = (Type)null;
            //
            if (_Types.TryGetValue(name, out type))
                return type;
            //
            type = Type.GetType(name, throwOnError, ignoreCase);
            //
            // If that didn't work, then we check our referenced assemblies' types.
            if ((type == null) && (_Assemblies != null))
            {
                Assembly[] assemblies = _Assemblies.Values.ToArray<Assembly>();
                //
                foreach (var a in assemblies)
                {
                    type = a.GetType(name, throwOnError, ignoreCase);
                    if (type != null)
                        break;
                }
            }

            // No luck.
            if ((type == null) && throwOnError)
                throw new Exception(String.Format("The type {0} could not be found. If it is an unqualified name, then its assembly has not been referenced.", name));
            //
            _Types.TryAdd(name, type);
            //
            return type;
        }

        public string GetPathOfAssembly(AssemblyName name)
        {
            var assembly = GetAssembly(name, false);
            if (assembly != null)
                return assembly.Location;
            //
            return null;
        }

        Assembly ITypeResolutionService.GetAssembly(AssemblyName name)
        {
            return GetAssembly(name, false);
        }
        
        Type ITypeResolutionService.GetType(string name, bool throwOnError)
        {
            return GetType(name, throwOnError, false);
        }
        
        Type ITypeResolutionService.GetType(string name)
        {
            return GetType(name, false, false);
        }

        /// <summary>
        /// Invoke Method from any class object with method name and parameters, pass null parameter for non parametrize method.
        /// </summary>
        /// <param name="classObj">Class object</param>
        /// <param name="methodName">Method Name</param>
        /// <param name="paramaters">Parameters, pass null for non parameter method call</param>
        /// <returns>Return object value from method</returns>
        public object InvokeMethod(object classObj, string methodName, params object[] paramaters)
        {
            if (classObj == null || string.IsNullOrEmpty(methodName))
                throw new ArgumentException("Please provide valid argument for Invoking the method.");

            return classObj.GetType().GetMethod(methodName).Invoke(classObj, paramaters);
        }

        #endregion

        #region Helper Functions

        private static object CreateInstance(Type type)
        {
            if (type == null)
                return null;

            var cf = type.GetConstructor(System.Type.EmptyTypes);
            if (cf != null)
                return Activator.CreateInstance(type);
            else
                return null;
        }

        private static object CreateInstance(Type type, object param1)
        {
            if (type == null)
                return null;

            var cf = type.GetConstructor(new Type[] { param1.GetType() });
            if (cf != null)
                return Activator.CreateInstance(type, new object[] { param1 });
            else
                return null;
        }

        private static object CreateInstance(Type type, object param1, object param2)
        {
            if (type == null)
                return null;

            var cf = type.GetConstructor(new Type[] { param1.GetType(), param2.GetType() });
            if (cf != null)
                return Activator.CreateInstance(type, new object[] { param1, param2 });
            else
                return null;
        }

        private static object CreateInstance(Type type, Type[] types, params object[] args)
        {
            if (type == null)
                return null;
            //
            if (types == null)
                return CreateInstance(type);
            //
            var cf = type.GetConstructor(types);
            if (cf != null)
                return Activator.CreateInstance(type, args);
            else
                return null;
        }

        #endregion
    }
}
