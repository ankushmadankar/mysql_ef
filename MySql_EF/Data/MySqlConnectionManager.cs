﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Extentions;
using System.Data;
using MySql.Data.MySqlClient;
using Fw.Controller.Interfaces;
using System.Reflection;

namespace Fw.Controller.Data
{
    public class MySqlConnectionManager
    {
        #region Global Variable

        private MySqlConnection _ObjConn = default(MySqlConnection);

        #endregion

        #region DB Actions

        #region Connection

        /// <summary>
        /// Open connection for SQL operations by connection string or by default connection string
        /// </summary>
        /// <Author>Ankush Madankar</Author>
        /// <param name="connectionString"></param>
        /// <returns>MySqlConnection object with open condition.</returns>
        public MySqlConnection OpenConnection(string connectionString = null)
        {
            _ObjConn = new MySqlConnection();

            if (!string.IsNullOrEmpty(connectionString))
            {
                _ObjConn.ConnectionString = connectionString;
                _ObjConn.Open();
            }
            else
            {
                if (_ObjConn.State == ConnectionState.Open) return _ObjConn;

                _ObjConn.ConnectionString = ConnectionString;
                //_ObjConn.ConnectionString = _DefaultConnectionString;
                _ObjConn.Open();
            }

            return _ObjConn;
        }

        /// <summary>
        /// Close connection.
        /// </summary>
        /// <param name="objConn"></param>
        public void CloseConnection(IDbConnection objConn)
        {
            if (objConn == null || (objConn != null && objConn.State == ConnectionState.Closed)) return;

            objConn.Close();
        }

        /// <summary>
        /// Close connection
        /// </summary>
        public void CloseConnection()
        {
            if (_ObjConn == null || (_ObjConn != null && _ObjConn.State == ConnectionState.Closed)) return;

            _ObjConn.Close();
        }

        #endregion

        #region Execute Query

        /// <summary>
        /// Execute non return query on database like update, insert, delete.
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="objConn"></param>
        public int ExecuteQuery(string commandText)
        {
            return ExecuteQueryInternal(commandText, CommandType.Text, null);
        }

        /// <summary>
        /// Execute non return query on database like update, insert, delete.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteQuery(string commandText, params object[] parameters)
        {
            return ExecuteQueryInternal(commandText, CommandType.Text, parameters);
        }

        /// <summary>
        /// Execute non return query on database like update, insert, delete.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public int ExecuteQuery(string commandText, CommandType commandType, params object[] parameters)
        {
            return ExecuteQueryInternal(commandText, commandType, parameters);
        }

        /// <summary>
        /// Execute select query and convert return dataRows into object collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <returns></returns>
        public IEnumerable<T> ExecuteQuery<T>(string query)
        {
            return Fill(query, CommandType.Text).ConvertToClassList<T>();
        }

        /// <summary>
        /// Execute select query and convert return into object collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IEnumerable<T> ExecuteQuery<T>(string query, params object[] parameters)
        {
            return Fill(query, parameters).ConvertToClassList<T>();
        }

        /// <summary>
        /// Execute select query and convert return dataRows into object collection
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="query"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public IEnumerable<T> ExecuteQuery<T>(string query, CommandType commandType, params object[] parameters)
        {
            return Fill(query, commandType, parameters).ConvertToClassList<T>();
        }

        /// <summary>
        /// For object those having contructor with parameters.
        /// </summary>
        /// <typeparam name="T">Type of object to convert</typeparam>
        /// <param name="query">Query to execute</param>
        /// <param name="commandType">Command type</param>
        /// <param name="parameters">Constructor parameters.</param>
        /// <returns></returns>
        public IEnumerable<T> ExecuteQuery<T>(string query, CommandType commandType, object[] classContructorParam, params object[] parameters)
        {
            return Fill(query, commandType, parameters).ConvertToClassList<T>(classContructorParam);
        }

        #endregion

        #region Fill dataTable

        /// <summary>
        /// Return filled DataTable with select sql query.
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="objConn"></param>
        /// <returns>DataTable having select query</returns>
        public DataTable Fill(string commandText)
        {
            return FillInternal(commandText, CommandType.Text, null).Tables[0];
        }

        /// <summary>
        /// Return filled DataTable with select sql query.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Fill(string commandText, params object[] parameters)
        {
            return FillInternal(commandText, CommandType.Text, parameters).Tables[0];
        }

        /// <summary>
        /// Return filled DataTable with select sql query.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataTable Fill(string commandText, CommandType commandType, params object[] parameters)
        {
            return FillInternal(commandText, commandType, parameters).Tables[0];
        }

        #endregion

        #region Fill dataSet

        /// <summary>
        /// Return filled DataSet with select sql query.
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="objConn"></param>
        /// <returns>DataSet</returns>
        public DataSet FillDataSet(string sqlQuery)
        {
            return FillInternal(sqlQuery, CommandType.Text, null);
        }

        /// <summary>
        /// Return filled DataSet with select sql query.
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataSet FillDataSet(string sqlQuery, params object[] parameters)
        {
            return FillInternal(sqlQuery, CommandType.Text, parameters);
        }

        /// <summary>
        /// Return filled DataSet with select sql query.
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public DataSet FillDataSet(string sqlQuery, CommandType commandType, params object[] parameters)
        {
            return FillInternal(sqlQuery, commandType, parameters);
        }

        #endregion

        #region Execute Scalar

        /// <summary>
        /// First row first column value of select query
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="objConn"></param>
        /// <returns></returns>
        public object ExecuteScalar(string commandText)
        {
            return ExecuteScalarInternal(commandText, CommandType.Text, null);
        }

        /// <summary>
        /// Execute Scalar, Get value of first column of first row.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object ExecuteScalar(string commandText, params object[] parameters)
        {
            return ExecuteScalarInternal(commandText, CommandType.Text, parameters);
        }

        /// <summary>
        /// Execute Scalar, Get value of first column of first row.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        public object ExecuteScalar(string commandText, CommandType commandType, params object[] parameters)
        {
            return ExecuteScalarInternal(commandText, commandType, parameters);
        }

        #endregion

        #region Other DB operations

        /// <summary>
        /// Fill dataTable with Sql Query and get dataAdaptor for Update, Delete process
        /// </summary>
        /// <param name="sqlQuery"></param>
        /// <param name="dataTable"></param>
        /// <param name="objConn"></param>
        /// <param name="closeConnection"></param>
        /// <returns></returns>
        public IDbDataAdapter FillDataTableAndGetAdaptorBySQLQuery(string sqlQuery, ref DataTable dataTable, IDbConnection objConn = null, bool closeConnection = false)
        {
            if (string.IsNullOrEmpty(sqlQuery)) return null;

            dataTable = new DataTable();
            var _dataAdapter = GetDbDataAdapter();
            var _cmd = GetDbCommand();
            bool _thisConnection = false;

            if (objConn == null)
            {
                objConn = this.OpenConnection();
                _thisConnection = true;
            }

            _cmd.Connection = objConn;
            _cmd.CommandText = sqlQuery;
            _dataAdapter.SelectCommand = _cmd;

            //Check if dataset applicable
            _dataAdapter.Fill(dataTable.DataSet);

            if (_thisConnection || closeConnection)
                this.CloseConnection(objConn);

            return _dataAdapter;
        }

        #endregion

        #region DB flexiblity

        public IDbCommand GetDbCommand()
        {
            return (new MySqlCommand());
        }

        public IDbConnection GetDbConnection()
        {
            return (new MySqlConnection());
        }

        public IDbDataAdapter GetDbDataAdapter()
        {
            return (new MySqlDataAdapter());
        }

        #endregion

        #endregion

        #region Private Helper Methods

        /// <summary>
        /// Get connection string for which app.config define with key "DefaultMySqlConnectionString".
        /// </summary>
        public string ConnectionString
        {
            get
            {
                //return "server=localhost;database=fw_data;uid=root;pwd=1234;";
                return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultMySqlConnectionString"].ConnectionString;
            }
        }

        /// <summary>
        /// Execute command internal by command text and command type
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private int ExecuteQueryInternal(string commandText, CommandType commandType, params object[] parameters)
        {
            if (commandText.IsNull())
                throw new ArgumentNullException("Sql query should not be null.");

            try
            {
                using (var command = GetDbCommand())
                {
                    command.CommandType = commandType;
                    command.CommandText = commandText;
                    if (parameters != null && parameters.Length > 0)
                        LoadCommandParameters(command, parameters);
                    //
                    command.Connection = OpenConnection();
                    //
                    return command.ExecuteNonQuery();
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Return filled DataTable with select sql query.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="objConn"></param>
        /// <returns>DataTable having select query</returns>
        private DataSet FillInternal(string commandText, CommandType commandType, params object[] parameters)
        {
            if (commandText.IsNull())
                throw new ArgumentNullException("Sql query should not be null.");

            var _dataTable = new DataSet();
            var _dataAdapter = GetDbDataAdapter();
            var _cmd = GetDbCommand();

            try
            {
                _cmd.CommandType = commandType;
                _cmd.CommandText = commandText;

                if (parameters != null && parameters.Length > 0)
                    LoadCommandParameters(_cmd, parameters);

                _cmd.Connection = OpenConnection();

                _dataAdapter.SelectCommand = _cmd;
                _dataAdapter.Fill(_dataTable);
            }
            catch
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }

            return _dataTable;
        }

        /// <summary>
        /// Execute Scalar and return single value.
        /// </summary>
        /// <param name="commandText"></param>
        /// <param name="commandType"></param>
        /// <param name="parameters"></param>
        /// <returns></returns>
        private object ExecuteScalarInternal(string commandText, CommandType commandType, params object[] parameters)
        {
            if (commandText.IsNull())
                throw new ArgumentNullException("Sql query should not be null.");

            try
            {
                using (var command = GetDbCommand())
                {
                    command.CommandType = commandType;
                    command.CommandText = commandText;

                    if (parameters != null && parameters.Length > 0)
                        LoadCommandParameters(command, parameters);

                    command.Connection = OpenConnection();
                    return command.ExecuteScalar();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                CloseConnection();
            }
        }

        /// <summary>
        /// Load parameter for query
        /// </summary>
        /// <param name="dbCommand"></param>
        /// <param name="storeParameters"></param>
        private void LoadCommandParameters(IDbCommand dbCommand, object[] storeParameters)
        {
            if (dbCommand.IsNull())
                throw new ArgumentNullException("dbCommand", "DbCommand cannot be null while loading parameters");

            if (dbCommand.CommandType == CommandType.Text)
            {
                for (var i = 0; i < storeParameters.Length; i++)
                {
                    dbCommand.CommandText = dbCommand.CommandText.Replace(string.Format("{" + "{{0}}" + "}", i), string.Format("@param{0}", i));
                    (dbCommand as MySqlCommand).Parameters.AddWithValue(string.Format("@param{0}", i), storeParameters[i] == null ? DBNull.Value : storeParameters[i]);
                }
            }
        }

        #endregion
    }
}
