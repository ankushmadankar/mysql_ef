﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using Fw.Controller.DocumentEntity;

namespace Fw.Controller.EntityCommand
{
    public class CloseEntityCommand : ICommand
    {
         private EntityManager _EntityManager;

         public CloseEntityCommand(EntityManager entityManger)
        {
            if (entityManger == null)
                throw new ArgumentNullException("Entity or entity manager can't be null.");

            _EntityManager = entityManger;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            //if (_EntityManager.EntityForm != null)
            //    _EntityManager.EntityForm.Close();
        }
    }
}
