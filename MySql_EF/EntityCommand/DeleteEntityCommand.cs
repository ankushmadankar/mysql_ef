﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using Fw.Controller.Extentions;
using System.Windows.Input;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Base;

namespace Fw.Controller.EntityCommand
{
    public class DeleteEntityCommand : ICommand
    {
        private EntityManager _EntityManager;

        public DeleteEntityCommand(EntityManager entityManger)
        {
            if (entityManger.IsNull())
                throw new ArgumentNullException("Entity or entity manager can't be null.");

            _EntityManager = entityManger;
        }

        public bool CanExecute(object parameter)
        {
            foreach (var entity in _EntityManager.Entities)
                if (entity.Id == 0) return false;

            return true;
        }

        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            foreach (var entity in _EntityManager.Entities)
                entity.MarkedDelete = true;

            _EntityManager.SaveChanges();
        }
    }
}
