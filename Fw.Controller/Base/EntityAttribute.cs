﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.Base
{
    [AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
    public class EntityAttribute : Attribute
    {
        public AttributeType Type { private set; get; }
        public string Name { private set; get; }

        public EntityAttribute(string name, AttributeType type)
        {
            Type = type;
            Name = name;
        }

        public bool IsRequired { get; set; }

        public bool ShowControl { get; set; }

        public string ColumnName { get; set; }

        public string Tag { get; set; }
    }

    public enum AttributeType : short
    {
        Physical = 1,
        Virtual = 2,
        Custom = 3,
        AutoSavable = 4,
        Other = 5
    }
}
