﻿
namespace Fw.Controller.Base
{
    public delegate void SavingChangesEventHadler(object sender);
    public delegate void EndSavingChangesEventHadler(object sender, EndSavingChangesEventArgs e);
    public delegate void EndCommitChangesEventHadler(object sender);
    public delegate void EndRollbackChangesEventHadler(object sender);

    public delegate void ManagerRefreshEventHandler(object sender);

    public delegate void BeginSavingDataEntityEventHadler(object sender, BeginSavingDataEntity e);
    public delegate void EndSavingDataEntityEventHadler(object sender, EndSavingDataEntity e);

    public delegate void OnInitEventHandler(object sender);
    public delegate void OnLoadEventHandler(object sender);

    public delegate void IsSavableEventHandler(object sender, NonSavableReasons reasons);
}