﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Reflection;
using Fw.Controller.Data;
using Fw.Controller.Extentions;
using Fw.Controller.Interfaces;
using MySql.Data.MySqlClient;
using Fw.Controller.DocumentEntity;

namespace Fw.Controller.Base
{
    /// <summary>
    /// Entity object responsible for data object communication with database.
    /// </summary>
    [Serializable()]
    public class EntityObject : IEntity, ICloneable, IDisposable
    {
        #region Declarations

        private readonly string _TableName;
        private MySqlConnectionManager _MySqlConnectionManager;
        private bool _MarkDelete;
        private int _Id;
        private EntityState _ObjectState;

        private readonly EntityManager _EntityManager;
        private EntityObject _OriginalEntity;
        private Dictionary<string, SingleRelation> _SingleRelations;

        #endregion

        #region Constructor

        /// <summary>
        /// To boot new entity (New entity).
        /// </summary>
        /// <param name="tableName">Name of table to save.</param>
        /// <param name="entityManager">Manager by which entity gets saved.</param>
        public EntityObject(string tableName, EntityManager entityManager)
        {
            _TableName = tableName;
            //
            _EntityManager = entityManager;
            _EntityManager.AddEntity(this);
            //
            SetObjectProperties();
        }

        /// <summary>
        /// Edit entity by using id value (Entity entity).
        /// </summary>
        /// <param name="tableName">Name of table to save.</param>
        /// <param name="entityManager">Manager by which entity gets saved.</param>
        /// <param name="id"></param>
        public EntityObject(string tableName, EntityManager entityManager, int id)
        {
            _TableName = tableName;
            //
            _EntityManager = entityManager;
            _EntityManager.AddEntity(this);
            //
            Id = id;
            //
            SetObjectProperties();
        }

        #endregion

        #region Events Members

        public event PropertyChangedEventHandler PropertyChanged;
        public event PropertyChangingEventHandler PropertyChanging;

        public event EndSavingDataEntityEventHadler EndSavingDataEntity;
        public event BeginSavingDataEntityEventHadler BeginSavingDataEntity;

        public event OnInitEventHandler OnInit;
        public event OnLoadEventHandler OnLoad;

        protected virtual void OnBeginSavingDataEntity(BeginSavingDataEntity e)
        {
            BeginSavingDataEntityEventHadler _handler = BeginSavingDataEntity;
            //
            if (_handler != null) _handler(this, e);
        }

        protected virtual void OnEndSavingDataEntity(EndSavingDataEntity e)
        {
            EndSavingDataEntityEventHadler _handler = EndSavingDataEntity;
            //
            if (_handler != null) _handler(this, e);
        }

        protected virtual void OnInitEntity()
        {
            OnInitEventHandler _handler = OnInit;

            if (_handler != null)
                _handler(this);

            IsEntityLoaded = false;
        }

        protected virtual void OnLoadEntity()
        {
            OnLoadEventHandler _handler = OnLoad;

            LoadEntityProperties();

            IsEntityLoaded = true;

            if (_handler != null)
                _handler(this);

            if (ObjectState != EntityState.Added)
                _OriginalEntity = Clone() as EntityObject;
        }

        #endregion

        #region Public Properties

        [Entity("Id", AttributeType.AutoSavable, ColumnName = "Id", IsRequired = true, ShowControl = false)]
        public int Id
        {
            get
            {
                return _Id;
            }
            set
            {
                NotifyPropertyChanging("Id");
                _Id = value;
                NotifyPropertyChanged("Id");
            }
        }

        [Entity("VersionNo", AttributeType.AutoSavable, ColumnName = "VersionNo", ShowControl = false)]
        public DateTime? VersionNo { get; set; }

        [Entity("TableName", AttributeType.Other, ShowControl = false)]
        public string TableName { get { return _TableName; } }

        [Entity("MySqlConnectionManagers", AttributeType.Other)]
        public MySqlConnectionManager MySqlConnectionManagers
        {
            get { return _MySqlConnectionManager ?? (_MySqlConnectionManager = new MySqlConnectionManager()); }
        }

        [Entity("ObjectState", AttributeType.Other)]
        public EntityState ObjectState
        {
            get
            {
                if (_ObjectState == EntityState.Detached) return _ObjectState;

                if (Id == 0)
                    _ObjectState = EntityState.Added;
                else if (MarkedDelete)
                    _ObjectState = EntityState.Deleted;

                var savableProp = GetSavableProperties();

                if (!savableProp.IsNull() && savableProp.Count() > 0)
                    _ObjectState = EntityState.Edited;

                _ObjectState = EntityState.Unchanged;

                return _ObjectState;
            }
        }

        [Entity("MarkDelete", AttributeType.Other, ShowControl = false)]
        public bool MarkedDelete
        {
            get { return _MarkDelete; }
            set
            {
                if (Id == 0 && value)
                    throw new InvalidOperationException("Can not set mark delete since object yet dont have id or added.");

                _MarkDelete = value;
            }
        }

        [Entity("IsEntityLoaded", AttributeType.Other, ShowControl = false)]
        public bool IsEntityLoaded
        {
            get;
            private set;
        }

        [Entity("EntityProperties", AttributeType.Other, ShowControl = false)]
        public EntityPropertyInfo[] EntityProperties { get; private set; }

        [Entity("EntityManager", AttributeType.Other, ShowControl = false)]
        public EntityManager EntityManager
        {
            get { return _EntityManager; }
        }

        [Entity("OriginalEntity", AttributeType.Other, ShowControl = false)]
        public EntityObject OriginalEntity
        {
            get { return _OriginalEntity; }
        }

        /// <summary>
        /// Maintan the collection of single relation.
        /// </summary>
        [Entity("SingleRelations", AttributeType.Other, ShowControl = false)]
        public Dictionary<string, SingleRelation> SingleRelations
        {
            get
            {
                return _SingleRelations.IsNull() ? (_SingleRelations = new Dictionary<string, SingleRelation>()) : _SingleRelations;
            }
        }

        #endregion

        #region Public Methods

        #region INotifyPropertyChanges Members

        protected void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        protected void NotifyPropertyChanging(string propertyName)
        {
            if (PropertyChanging != null)
            {
                PropertyChanging(this, new PropertyChangingEventArgs(propertyName));
            }
        }

        #endregion

        #region Entity Actions

        public void Save(MySqlConnection conn, MySqlTransaction transaction)
        {
            SaveInternal(conn, transaction);
        }

        public void RefreshEntity()
        {
            IsEntityLoaded = false;

            LoadEntity();
        }

        public void LoadEntity()
        {
            if (IsEntityLoaded) return;

            SetObjectProperties();
        }

        #endregion

        #region Public Helper Methods

        public void ExecuteStoreCommand(string query, params object[] parameters)
        {
            if (query.IsNull())
                throw new ArgumentNullException("Query string can't be null or empty.");

            MySqlConnectionManagers.ExecuteQuery(query, parameters);
        }

        public IEnumerable<T> ExecuteStoreQuery<T>(string query, params object[] parameters)
        {
            if (query.IsNull())
                throw new ArgumentNullException("Query string can't be null or empty.");

            return MySqlConnectionManagers.ExecuteQuery<T>(query, parameters);
        }

        protected virtual bool ValidateEntity(NonSavableReasons nonSavableReasons)
        {
            var propInfo = GetType().GetProperties();

            foreach (var prop in propInfo)
            {
                var custProps = prop.GetCustomAttributes(typeof(EntityAttribute), true);

                foreach (var custPro in custProps.Select(a => (a as EntityAttribute)).Where(a => a.IsRequired))
                {
                    if (prop.GetValue(this, null).IsNull())
                    {
                        var key = TableName + "." + custPro.Name;

                        if (!nonSavableReasons.Reasons.Keys.Contains(key))
                            nonSavableReasons.Reasons.Add(TableName + "." + custPro.Name, custPro.Tag ?? custPro.Name + " is mandatory.");
                    }
                    //throw new InvalidOperationException("Property value " + (custPro as EntityAttribute).Name + " not having value.");
                }
            }

            return nonSavableReasons.Reasons.Count == 0;
        }

        public bool CanSave(NonSavableReasons nonSavableReasons)
        {
            return ValidateEntity(nonSavableReasons);
        }

        #endregion

        #region IClonable Members

        /// <summary>
        /// Nake copy of object. Take care must need while manager handling.
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion

        #region IDisposable Members

        public virtual void Dispose()
        {
            if (!EntityProperties.IsNull())
                foreach (var property in EntityProperties)
                    property.Dispose();

            MySqlConnectionManagers.CloseConnection();

            if (!_EntityManager.IsNull())
                _EntityManager.RemoveEntity(this);
        }

        #endregion

        #endregion

        #region Protected Methods

        /// <summary>
        /// Add single relation, you can access relation with foreignEntity tableName.
        /// </summary>
        /// <param name="primaryEntity"></param>
        /// <param name="foreignEntity"></param>
        protected void AddSingleRelation(IEntity primaryEntity, IForeignRelation foreignEntity)
        {
            if (primaryEntity.IsNull() || foreignEntity.IsNull())
                throw new ArgumentNullException("Primary and foreign entity need to specify.");

            if (!SingleRelations.ContainsKey(foreignEntity.TableName))
                SingleRelations.Add(foreignEntity.TableName, new SingleRelation(primaryEntity, foreignEntity));
        }

        #endregion

        #region Helpers Functions

        private string GetUpdateQuery(ref Dictionary<string, object> props)
        {
            string _updateQuery = string.Empty;

            Type objType = GetType();
            var propertyList = new List<PropertyInfo>(objType.GetProperties());

            _updateQuery += "update " + (string.IsNullOrEmpty(TableName) ? objType.ToString().Split('.').LastOrDefault() : TableName) + " set";

            foreach (var savableProp in GetSavableProperties())
            {
                if (savableProp.AttributeInfo.Type == AttributeType.Physical)
                {
                    string _name = savableProp.AttributeInfo.ColumnName ?? savableProp.AttributeInfo.Name;

                    _updateQuery += " " + _name + " = ?,";

                    props.Add(_name, savableProp.NewPropertyValue);
                }
            }

            _updateQuery = _updateQuery.TrimEnd(',');
            _updateQuery += " where " + "ID" + " = ?";

            return _updateQuery;
        }

        private void SaveInternal(MySqlConnection conn, MySqlTransaction transaction)
        {
            if (Id == 0 && MarkedDelete)
                throw new NotSupportedException("Entity not added yet. Please save document first.");

            if (!IsEntityLoaded)
                throw new NotSupportedException("Entity yet not loaded. Call LoadEntity first.");

            if(ObjectState == EntityState.Detached)
                throw new NotSupportedException("Entity not exists or deleted before.");
            //
            //CheckIfVersionNoValid();
            //
            var cmd = new MySqlCommand();
            //
            cmd.Connection = conn;
            cmd.Transaction = transaction as MySql.Data.MySqlClient.MySqlTransaction;
            cmd.CommandType = CommandType.Text;
            //
            OnBeginSavingDataEntity(new BeginSavingDataEntity(cmd, transaction));
            //
            //var cmd = new MySqlCommand();
            //
            if (Id > 0 && !MarkedDelete && GetSavableProperties().Count() == 0)
            {
                OnEndSavingDataEntity(new EndSavingDataEntity(0, cmd, transaction)); return;
            }
            //
            var _table = default(DataTable);
            var _dataAdapter = default(MySqlDataAdapter);

            if (Id > 0)
            {
                if (MarkedDelete)
                {
                    cmd.CommandText = "delete from " + TableName + " where ID = " + Id;
                }
                else
                {
                    var _props = new Dictionary<string, object>();

                    cmd.CommandText = GetUpdateQuery(ref _props);

                    foreach (string value in _props.Keys)
                    {
                        object _addValue;

                        _props.TryGetValue(value, out _addValue);
                        cmd.Parameters.AddWithValue("@" + value, _addValue ?? DBNull.Value);
                    }
                    //
                    cmd.Parameters.AddWithValue("@" + "ID", Id);
                    //
                }
            }
            else
            {
                _table = GetDataTableByObject();

                _dataAdapter = new MySqlDataAdapter();

                cmd.CommandText = "select * from " + TableName + " limit 1";
            }

            if (conn.State != ConnectionState.Open)
                conn.Open();

            try
            {
                int generatedId = default(int);

                if (Id > 0)
                {
                    if (cmd.ExecuteNonQuery() == 0)
                        throw new DBConcurrencyException("Entity already updated or deleted.");
                }
                else
                {
                    if (_dataAdapter != null && _table != null)
                    {
                        _dataAdapter.SelectCommand = cmd;

                        _dataAdapter.Fill(new DataSet());

                        _dataAdapter.InsertCommand = new MySqlCommandBuilder(_dataAdapter).GetInsertCommand();
                        _dataAdapter.Update(_table);

                        //cmd.CommandText = "select ID from " + TableName + " order by ID desc limit 1";
                        //select Random_number from number where Number_id=Last_insert_id();
                        cmd.CommandText = "select Id from " + TableName + " where Id = LAST_INSERT_ID()";

                        generatedId = int.Parse(cmd.ExecuteScalar().ToString());

                        if (generatedId == 0)
                            throw new NotSupportedException("Not record inserted or no Id found in insert.");

                        Id = generatedId;
                    }
                }

                OnEndSavingDataEntity(new EndSavingDataEntity(generatedId, cmd, transaction));

                if (MarkedDelete)
                {
                    MarkedDelete = false;
                    _ObjectState = EntityState.Detached;
                }
            }
            catch
            {
                if (_dataAdapter != null && _table != null)
                    Id = 0;

                throw;
            }
        }

        private void CheckIfVersionNoValid()
        {
            if (Id == 0) return;

            var value = MySqlConnectionManagers.ExecuteQuery<byte[]>("select TOP 1 VersionNo from {0} where ID = {1}", TableName, Id).FirstOrDefault();

            if (value.IsNull() || !value.Equals(VersionNo))
                throw new InvalidOperationException("Entity already updated or deleted.");
        }

        private void SetObjectPropertiesInternal(object objClass, DataTable dataTable)
        {
            if (objClass.IsNull() || dataTable.IsNull())
                throw new ArgumentNullException("class object or dataTable can not be null.");

            List<PropertyInfo> propertyList = objClass.GetType().GetProperties().ToList();

            foreach (DataColumn dc in dataTable.Columns)
            {
                var _prop = propertyList.Where(a => a.Name == dc.ColumnName).Select(a => a).FirstOrDefault();

                if (_prop == null) continue;

                if (dataTable.Rows.Count > 0)
                    _prop.SetValue(objClass, (DBNull.Value == dataTable.Rows[0][dc]) ? _prop.PropertyType.GetDefault() : Convert.ChangeType(dataTable.Rows[0][dc], Nullable.GetUnderlyingType(_prop.PropertyType) ?? _prop.PropertyType), null);
                else
                    _prop.SetValue(objClass, (_prop.PropertyType == typeof(DateTime)) ? DateTime.Now.GetDateWithoutMilliseconds() : _prop.PropertyType.GetDefault(), null);//To set null values.
            }
        }

        /// <summary>
        /// Convert class object properties into DataTable
        /// </summary>
        /// <param name="obj">Class object</param>
        /// <returns></returns>
        private DataTable GetDataTableByObject()
        {
            Type objType = GetType();

            string tableName = TableName.IsNull() ? objType.ToString().Split('.').LastOrDefault() : TableName;

            DataTable result = new DataTable(tableName);
            List<PropertyInfo> propertyList = new List<PropertyInfo>(objType.GetProperties());

            result = MySqlConnectionManagers.Fill(string.Format("select * from {0} where 1=2", tableName));

            result.TableName = tableName;

            //foreach (PropertyInfo prop in propertyList)
            //{
            //    var custprop = prop.GetCustomAttributes(typeof(EntityAttribute), true).FirstOrDefault(a => (a is EntityAttribute)
            //        && ((a as EntityAttribute).Type == AttributeType.Physical
            //        || (a as EntityAttribute).Type == AttributeType.AutoSavable
            //        || (a as EntityAttribute).Type == AttributeType.Custom)) as EntityAttribute;

            //    if (custprop.IsNull()) continue;

            //    result.Columns.Add(custprop.ColumnName.IsNull() ? custprop.Name : custprop.ColumnName, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            //}

            DataRow dRow = result.NewRow();

            foreach (PropertyInfo prop in propertyList)
            {
                var custprop = prop.GetCustomAttributes(typeof(EntityAttribute), true).FirstOrDefault(a => (a is EntityAttribute)
                   && ((a as EntityAttribute).Type == AttributeType.Physical
                   || (a as EntityAttribute).Type == AttributeType.AutoSavable
                   || (a as EntityAttribute).Type == AttributeType.Custom)) as EntityAttribute;

                if (custprop.IsNull()) continue;

                if (result.Columns[custprop.ColumnName.IsNull() ? custprop.Name : custprop.ColumnName] == null)
                    throw new Exception("DataTableConverter.AddRow: " + "Column name does not exist: " + prop.Name);

                object propValue = prop.GetValue(this, null);

                dRow[custprop.ColumnName.IsNull() ? custprop.Name : custprop.ColumnName] = propValue ?? DBNull.Value;
            }
            result.Rows.Add(dRow);

            return result;
        }

        /// <summary>
        /// Get table schema for which properties in objClass defined.
        /// </summary>
        /// <param name="objClass"></param>
        /// <returns></returns>
        public DataTable CreateTable(object objClass)
        {
            Type objType = objClass.GetType();
            DataTable result = new DataTable(objType.ToString().Split('.').LastOrDefault());
            List<PropertyInfo> propertyList = new List<PropertyInfo>(objType.GetProperties());

            foreach (PropertyInfo prop in propertyList)
            {
                object propValue = prop.GetValue(objClass, null);
                result.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
            return result;
        }

        /// <summary>
        /// Add row in defined objClass and created DataTable for objClass.
        /// </summary>
        /// <param name="table">Table schema which created from objClass</param>
        /// <param name="data">objClass obj which need to added in DataTable</param>
        public void AddRow(ref DataTable table, object data)
        {
            Type objType = data.GetType();
            string className = objType.ToString().Split('.').LastOrDefault();

            if (!table.TableName.Equals(className))
            {
                throw new Exception("DataTableConverter.AddRow: " +
                                    "TableName not equal to className.");
            }

            DataRow dRow = table.NewRow();
            List<PropertyInfo> propertyList = new List<PropertyInfo>(objType.GetProperties());

            foreach (PropertyInfo prop in propertyList)
            {
                if (table.Columns[prop.Name] == null)
                    throw new Exception("DataTableConverter.AddRow: " + "Column name does not exist: " + prop.Name);

                object propValue = prop.GetValue(data, null);

                dRow[prop.Name] = propValue ?? DBNull.Value;
            }
            table.Rows.Add(dRow);
        }

        /// <summary>
        /// Load entity properties info as per EntityPropertyInfo implemention.
        /// </summary>
        private void LoadEntityProperties()
        {
            var properties = GetType().GetProperties().Select(a => a.Name).ToArray();

            if (properties.IsNull()) return;

            if (!EntityProperties.IsNull())
                foreach (var property in EntityProperties)
                    property.Dispose();

            EntityProperties = new EntityPropertyInfo[properties.Count()];

            for (int i = 0; i < properties.Count(); i++)
                EntityProperties[i] = new EntityPropertyInfo(this, properties[i]);
        }

        /// <summary>
        /// Find those properties which are edited(Changed).
        /// </summary>
        /// <returns></returns>
        public IEnumerable<EntityPropertyInfo> GetSavableProperties()
        {
            if (EntityProperties.IsNull() || EntityProperties.Count() == 0 || !IsEntityLoaded) yield break;

            foreach (var entityProp in EntityProperties.Where(a => a.PropertyState == EntityPropertyState.Changed && !a.AttributeInfo.IsNull() && a.AttributeInfo.Type != AttributeType.Other))
                yield return entityProp;
        }

        /// <summary>
        /// Set entity object properties on basis of id.
        /// </summary>
        private void SetObjectProperties()
        {
            string _query = "select * from " + TableName + " where ID = " + Id;

            OnInitEntity();
            SetObjectPropertiesInternal(this, MySqlConnectionManagers.Fill(_query));
            OnLoadEntity();
        }

        #endregion
    }
}
