﻿using Fw.Controller.Base;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.Base
{
    /// <summary>
    /// Class is responsible to manage single relation with entity.
    /// </summary>
    public class SingleRelationEntity : EntityObject, IForeignRelation
    {
        #region Declaration

        private IEntity _PrimaryEntity;
        private int _ForeignId; 
        
        #endregion

        #region Constructor

        public SingleRelationEntity(string tableName, EntityManager entityManger, IEntity primaryEntity)
            : base(tableName, entityManger)
        {
            BindEntity(primaryEntity);
        }

        public SingleRelationEntity(string tableName, EntityManager entityManger, int id, IEntity primaryEntity)
            : base(tableName, entityManger, id)
        {
            BindEntity(primaryEntity);
        }
 
        #endregion

        #region Entity Propeties

        [Entity("ForeignId", AttributeType.Physical, ColumnName = "ForeignId", ShowControl = false)]
        public int ForeignId
        {
            get
            {
                return _ForeignId;
            }
            set
            {
                if (_ForeignId == value) return;

                NotifyPropertyChanging("ForeignId");
                _ForeignId = value;
                NotifyPropertyChanged("ForeignId");
            }
        }

        

        #endregion

        #region Public/IForeignRelation Properties

        /// <summary>
        /// Get primary entity object, can be convert to other type of object.
        /// </summary>
        public IEntity PrimaryEntity
        {
            get { return _PrimaryEntity; }
        } 

        #endregion

        #region Entity Events

        private void PrimaryEntity_BeginSavingDataEntity(object sender, BeginSavingDataEntity e)
        {
            if ((PrimaryEntity as EntityObject).ObjectState == EntityState.Deleted)
            {
                MarkedDelete = true;
                Save(e.Command.Connection as MySqlConnection, e.Transaction as MySqlTransaction);
            }
        }

        private void PrimaryEntity_EndSavingDataEntity(object sender, EndSavingDataEntity e)
        {
            if (ObjectState == EntityState.Added)
                ForeignId = e.GeneratedId;

            if (ObjectState != EntityState.Deleted || ObjectState != EntityState.Detached)
                Save(e.Command.Connection as MySqlConnection, e.Transaction as MySqlTransaction);
        }

        private void BindEntity(IEntity primaryEntity)
        {
            _PrimaryEntity = primaryEntity;

            if (!(primaryEntity is EntityObject))
                throw new NotSupportedException("Primary and secondary object need to be entity object.");

            (primaryEntity as EntityObject).BeginSavingDataEntity += PrimaryEntity_BeginSavingDataEntity;
            (primaryEntity as EntityObject).EndSavingDataEntity += PrimaryEntity_EndSavingDataEntity;
        }

        #endregion
    }
}
