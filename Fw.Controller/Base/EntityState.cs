﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.Base
{
    public enum EntityState
    {
        /// <summary>
        /// Entity id = 0
        /// </summary>
        Added,
        /// <summary>
        /// Entity added(Saved) and one of physical property changes.
        /// </summary>
        Edited,
        /// <summary>
        /// Entity added and marked as deleted.
        /// </summary>
        Deleted,
        /// <summary>
        /// Entity added(Saved) and none property changed.
        /// </summary>
        Unchanged,
        /// <summary>
        /// When entity deleted sucessufully.
        /// </summary>
        Detached
    }
}
