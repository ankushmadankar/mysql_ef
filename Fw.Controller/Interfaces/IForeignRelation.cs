﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.Interfaces
{
    /// <summary>
    /// For maintain foreign relation having Foreign id.
    /// </summary>
    public interface IForeignRelation : IEntity
    {
        int ForeignId { get; set; }

        IEntity PrimaryEntity { get; }
    }
}
