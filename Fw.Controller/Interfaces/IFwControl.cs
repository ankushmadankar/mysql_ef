﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;

namespace Fw.Controller.Interfaces
{
    /// <summary>
    /// Framework control interface basically those are wpf controls and use in validations and databinding.
    /// <para></para>
    /// Implement only in case if control can bind with entity object.
    /// </summary>
    public interface IFwControl
    {
        /// <summary>
        /// True to validate control value not set to null.
        /// </summary>
        bool IsMandatory { get; set; }

        /// <summary>
        /// Logical name while showing validation to user.
        /// </summary>
        string LabelName { get; set; }

        /// <summary>
        /// Class property name for which control binded.(e.g "ItemName" for class ItemMaster)
        /// </summary>
        string SourcePropertyName { get; set; }

        /// <summary>
        /// Control property to bind.
        /// </summary>
        DependencyProperty ControlProperty { get; }

        /// <summary>
        /// Mode of binding.
        /// </summary>
        BindingMode BindingMode { get; }
    }
}
