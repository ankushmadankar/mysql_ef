﻿namespace Fw.Controller.Interfaces
{
    public interface IFormDocument
    {
        /// <summary>
        /// Validate document if document savable or not.
        /// </summary>
        /// <returns>bool value, if document suppose to save then true else false</returns>
        bool IsSavable();
        /// <summary>
        /// Create new document
        /// </summary>
        void NewDocument();
        /// <summary>
        /// Performe save operation after IsSave() return true value.
        /// </summary>
        void SaveDocument();
        /// <summary>
        /// Close document and dispose respective object.
        /// </summary>
        void CloseDocument();
        /// <summary>
        /// Delete Document
        /// </summary>
        void DeleteRequest();
        /// <summary>
        /// Do refresh and load all latest changes from database.
        /// </summary>
        void RefreshDocument();
        /// <summary>
        /// Set Document Property on primary field change
        /// </summary>
        void SetDocumentProperties();
        /// <summary>
        /// Load document default data which required on Form load
        /// </summary>
        void LoadDocumentDefaultData();
    }
}
