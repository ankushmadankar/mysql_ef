﻿using System;
using System.ComponentModel;

namespace Fw.Controller.Interfaces
{
    /// <summary>
    /// Basic structure of entity object.
    /// </summary>
    public interface IEntity : INotifyPropertyChanged, INotifyPropertyChanging
    {
        /// <summary>
        /// Primary key for column Id.
        /// </summary>
        int Id { get; set; }

        /// <summary>
        /// Name of table in which data to be save.
        /// </summary>
        string TableName { get; }

        /// <summary>
        /// Version no to handle concerency.
        /// </summary>
        DateTime? VersionNo { get; set; }
      
        /// <summary>
        /// 
        /// </summary>
        bool MarkedDelete { get; set; }
        //bool IsEntityLoaded { get; }

        /// <summary>
        /// Load entity.
        /// </summary>
        void LoadEntity();

        /// <summary>
        /// Refresh data for entity.
        /// </summary>
        void RefreshEntity();

        /// <summary>
        /// Save entity with trasaction and connection.
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="transaction"></param>
        void Save(MySql.Data.MySqlClient.MySqlConnection connection, MySql.Data.MySqlClient.MySqlTransaction transaction);
    }
}
