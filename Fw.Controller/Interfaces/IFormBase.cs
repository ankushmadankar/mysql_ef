﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using System.Windows;

namespace Fw.Controller.Interfaces
{
    public interface IFormBase
    {
        bool IsFormCloseOnEsc { get; set; }

        bool ValidateForm();
        void LoadControlsData();

        void BindDocument(Menu defaultActionMenu);
        void BindDocument();
        
        IEnumerable<T> ControlsOfType<T>(DependencyObject depObj) where T : DependencyObject;
        IEnumerable<IFwControl> Controls(DependencyObject depObj);
    }
}
