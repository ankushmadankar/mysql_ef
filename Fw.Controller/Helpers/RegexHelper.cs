﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace Fw.Controller.Helpers
{
    public class RegexHelper
    {
        #region Declaration

        public static Regex _Regex = null;

        #endregion

        #region Static methods

        public static bool IsValidEmail(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidInteger(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^[0-9]+$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidDecimal(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^-?[0-9]\d*(.\d+)?$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidMobile(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^\d{10}$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidContactNo(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^\+?[0-9-]+$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidDate(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^([1-9]|0[1-9]|1\d|2\d|3[0-1])[- / .]([1-9]|0[1-9]|1[0-2])[- / .](1[9]\d\d|2[0]\d\d)$");

            if (_Regex.IsMatch(value))
            {
                string[] _dateArrat = value.Split(new char[] { '-', '/', '.' }, StringSplitOptions.RemoveEmptyEntries);
                int _year = Convert.ToInt32(value.Substring((value.Length - 4)));

                if (!CheckDayOfMonth(Convert.ToInt32(_dateArrat[1]), Convert.ToInt32(_dateArrat[0]), Convert.ToInt32(_dateArrat[2])))
                    return false;
                else
                    return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidProcessingFormula(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^(\d|[.]|[D]|[W]|[C]|[(]|[)]|[-]|[+]|[*]|[/]|[%])+$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidPhoneNoWithArea(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"\d{3}-\d{3}-\d{4}");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidSSN(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"\d{3}-\d{2}-\d{4}");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidIpAddress(string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            short cnt = 0;
            bool _error = false;
            int len = value.Length;
            for (short i = 0; i < len; i++)
                if (value[i] == '.')
                {
                    cnt++;
                    if (i + 1 < len)
                        if (value[i + 1] == '.')
                        {
                            _error = true;
                            break;
                        }
                }
                else
                    return true;
            if ((cnt < 3 || value[len - 1] == '.') || _error)
                return false;
            else
                return true;
        }

        public static bool IsValidCreditCardNo(string value)
        {
            string pattern = @" (^((?<Number>\d{4})(?:[\s-]?)){4}(?!\d))$ | \d{16}$";

            var match = Regex.Match(value.Trim(), pattern, RegexOptions.IgnorePatternWhitespace).Groups["Number"].Captures.OfType<Capture>().Aggregate(string.Empty, (seed, current) => seed + current);
            
            if (String.IsNullOrEmpty(match))
                return false;
            else
                return true;
        }

        #endregion

        #region Private methods

        private static bool CheckDayOfMonth(int mon, int day, int year = 0)
        {
            bool ret = true;
            if (day == 0) ret = false;
            switch (mon)
            {
                case 1:
                    if (day > 31)
                        ret = false;
                    break;
                case 2:
                    if (year == 0)
                    {
                        System.DateTime moment = DateTime.Now;
                        year = moment.Year;
                    }
                    int d = ((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28;
                    if (day > d)
                        ret = false;
                    break;
                case 3:
                    if (day > 31)
                        ret = false;
                    break;
                case 4:
                    if (day > 30)
                        ret = false;
                    break;
                case 5:
                    if (day > 31)
                        ret = false;
                    break;
                case 6:
                    if (day > 30)
                        ret = false;
                    break;
                case 7:
                    if (day > 31)
                        ret = false;
                    break;
                case 8:
                    if (day > 31)
                        ret = false;
                    break;
                case 9:
                    if (day > 30)
                        ret = false;
                    break;
                case 10:
                    if (day > 31)
                        ret = false;
                    break;
                case 11:
                    if (day > 30)
                        ret = false;
                    break;
                case 12:
                    if (day > 31)
                        ret = false;
                    break;
                default:
                    ret = false;
                    break;
            }
            return ret;
        }

        #endregion
    }
}
