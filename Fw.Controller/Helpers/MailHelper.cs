﻿using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Net.NetworkInformation;

namespace Fw.Controller.Helpers
{
    public class MailHelper
    {
        public static void SendMailByConfiguration(string fromAddress, string fromPassword, string toAddress, string ispProvider, string subject, string body, string attachmentFilename = null, byte[] attachmentData = null)
        {
            if (!IsNetworkLikelyAvailable())
                throw new NetworkInformationException();

            if (string.IsNullOrEmpty(fromAddress) || string.IsNullOrEmpty(fromPassword) || string.IsNullOrEmpty(toAddress) || string.IsNullOrEmpty(ispProvider)) return;

            string _mailServer;
            int _port;

            switch (ispProvider.ToLower())
            {
                case "gmail":
                    _mailServer = "smtp.gmail.com";
                    _port = 587;
                    break;
                case "outlook":
                    _mailServer = "smtp.live.com";
                    _port = 587;
                    break;
                case "yahoo":
                    _mailServer = "smtp.mail.yahoo.com";
                    _port = 465;
                    break;
                case "yahoo.co.in":
                    _mailServer = "smtp.mail.yahoo.co.in";
                    _port = 25;
                    break;
                case "rediffmail":
                    _mailServer = "smtp.rediffmail.com";
                    _port = 25;
                    break;
                case "office":
                    _mailServer = "pod51021.outlook.com";
                    _port = 587;
                    break;
                default:
                    _mailServer = string.Empty;
                    _port = 0;
                    break;
            }

            if (string.IsNullOrEmpty(_mailServer) || _port == 0) return;

            var smtp = new SmtpClient();
            {
                smtp.Host = _mailServer;
                smtp.Port = _port;

                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
                //smtp.Timeout = 20000;
            }

            MailMessage mail = GetMailInfo(fromAddress, toAddress, subject, body, attachmentFilename, attachmentData);

            if (mail != null)
            {
                smtp.Send(mail);
            }
        }

        private static bool IsNetworkLikelyAvailable()
        {
            //return NetworkInterface
            //  .GetAllNetworkInterfaces()
            //  .Any(x => x.OperationalStatus == OperationalStatus.Up);

            return NetworkInterface.GetIsNetworkAvailable();
        }

        private static MailMessage GetMailInfo(string fromAddress, string toAddress, string subject, string body, string attachmentFilename, byte[] attachmentData)
        {
            MailMessage mail = new MailMessage();

            mail.From = new MailAddress(fromAddress);
            Attachment attachment = null;
            mail.To.Add(toAddress);
            mail.Subject = subject;
            mail.Body = body;

            if (!string.IsNullOrEmpty(attachmentFilename) && attachmentData != null)
            {
                attachment = new Attachment(new MemoryStream(attachmentData), attachmentFilename);
                //attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                ContentDisposition disposition = attachment.ContentDisposition;
                disposition.FileName = attachmentFilename;
                disposition.Size = attachmentData.Length;
                disposition.DispositionType = DispositionTypeNames.Attachment;
                mail.Attachments.Add(attachment);
            }

            return mail;
        }

        public static byte[] CompressByteArray(byte[] data)
        {
            var _output = new MemoryStream();
            using (var gzip = new GZipStream(_output, CompressionMode.Compress, true))
            {
                gzip.Write(data, 0, data.Length);
                gzip.Close();
            }
            return _output.ToArray();
        }

        public static byte[] DeCompressByteArray(byte[] data)
        {
            var _output = new MemoryStream();
            var _input = new MemoryStream();
            _input.Write(data, 0, data.Length);
            _input.Position = 0;

            using (var gzip = new GZipStream(_input, CompressionMode.Decompress, true))
            {
                var buff = new byte[64];
                var read = gzip.Read(buff, 0, buff.Length);

                while (read > 0)
                {
                    _output.Write(buff, 0, read);
                    read = gzip.Read(buff, 0, buff.Length);
                }

                gzip.Close();
            }
            return _output.ToArray();
        }
    }
}
