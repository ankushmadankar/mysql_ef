﻿using System;
using Fw.Controller.Data;

namespace Fw.Controller.Security
{
    public class Class1
    {
        #region Declaration

        private static MySqlConnectionManager _MySqlConnectionManager;

        #endregion

        #region Private Propeties

        private static MySqlConnectionManager MySqlConnectionManagers
        {
            get
            {
                if (_MySqlConnectionManager == null)
                {
                    _MySqlConnectionManager = new MySqlConnectionManager();
                    return _MySqlConnectionManager;
                }
                else
                    return _MySqlConnectionManager;
            }
        }

        #endregion

        #region Static Security Method

        public static string MyPassword()
        {
            string _password = string.Empty;
            _password = "" + Convert.ToString(DateTime.Today.Day) + Convert.ToString(DateTime.Today.Month) + Convert.ToString(DateTime.Today.Year);
            return CrypTo.EncryptString(_password);
        }

        public static void LogMACAddress(bool isFake = false)
        {
            string _mac = CrypTo.EncryptString(MACHelper.GetStringMACAddress());
            string _query = string.Format("select ID from MACAddress where MACAddress= '{0}'", _mac);
            int _exists = MySqlConnectionManagers.Fill(_query).Rows.Count;
            _query = string.Empty;

            if (_exists == 0 && !isFake)
                _query = string.Format("insert into MACAddress(MACAddress,CreationDate,IsFake) values ('{0}','{1}',{2})", _mac, DateTime.Now, false);

            if (_exists > 0 && isFake)
                _query = string.Format("Delete * from MACAddress where MACAddress = '{0}'", _mac);

            if (!string.IsNullOrEmpty(_query))
                MySqlConnectionManagers.ExecuteScalar(_query);
        }

        public static bool MACAvailable()
        {
            string _query = string.Format("select * from MACAddress where MACAddress = '{0}' limit 1", CrypTo.EncryptString(MACHelper.GetStringMACAddress()));

            int _exist = MySqlConnectionManagers.Fill(_query).Rows.Count;
            if (_exist == 0) return false;

            return true;
        }

        public static bool CheckUserNameAndPassword(string userName, string password)
        {
            string _query = string.Format("select * from UserMaster where UserName = '{0}' and Password = '{1}' limit 1", userName, CrypTo.EncryptString(password));

            int _exist = MySqlConnectionManagers.Fill(_query).Rows.Count;
            if (_exist == 0) return false;

            return true;
        }

        public static bool IsMe(string userName, string password)
        {
            if (userName == "ankush" && CrypTo.EncryptString(password) == MyPassword())
            {
                LogMACAddress(false);
                return true;
            }

            return false;
        }

        #endregion
    }
}
