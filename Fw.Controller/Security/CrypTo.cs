﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace Fw.Controller.Security
{
    [Serializable]
    public class CrypTo
    {
        #region Declaration

        /// <summary>
        /// Defines a wrapper object to access the cryptographic service provider (CSP) version of the 
        /// System.Security.Cryptography.TripleDES algorithm. 
        /// </summary>
        static TripleDESCryptoServiceProvider _Provider = new TripleDESCryptoServiceProvider();

        #endregion

        #region Constructor

        static CrypTo()
        {
            _Provider.Key = new byte[] { 111, 222, 132, 85, 171, 41, 165, 135, 218, 183, 42, 192, 113, 111, 138, 12 };
            _Provider.IV = new byte[] { 162, 213, 12, 41, 232, 162, 71, 214 };
        }

        #endregion

        #region Methods

        public static string EncryptString(string originalString)
        {
            MemoryStream _dataStream = null;
            ICryptoTransform _encryptor = null;
            CryptoStream _encryptedStream = null;
            StreamWriter _theWriter = null;

            if (originalString == String.Empty)
                return originalString;
            else
            {
                /// Creates a symmetric encryptor object with the current System.Security.Cryptography.SymmetricAlgorithm.Key
                /// and initialization vector System.Security.Cryptography.SymmetricAlgorithm.IV
                _encryptor = _Provider.CreateEncryptor();

                ///Creates a store whose backing store is memory.
                _dataStream = new MemoryStream();
                try
                {
                    ///Defines a stream that links data streams to cryptograpic transformations.
                    _encryptedStream = new CryptoStream(_dataStream, _encryptor, CryptoStreamMode.Write);
                    ///Initializes a new instance of the System.IO.StreamWriter class 
                    /// for the specified stream, using UTF-8 encoding and the default buffer size.
                    _theWriter = new StreamWriter(_encryptedStream);

                    ///Writes a string to stream.
                    _theWriter.Write(originalString);

                    ///Clears all the data for current writer
                    _theWriter.Flush();

                    ///Updates the underlying data source or repository with the current state of the buffer, 
                    /// then clears the buffer.
                    _encryptedStream.FlushFinalBlock();

                    ///Sets the current position within the stream
                    _dataStream.Position = 0;

                    ///Creates a new instance of byte array.
                    byte[] _enCryptedData = new byte[_dataStream.Length];

                    ///Reads a block of bytes from the current stream and writes the data to buffer.
                    _dataStream.Read(_enCryptedData, 0, (int)_dataStream.Length);

                    ///Converts the value of a subset of an array of 8-bit unsigned integers to 
                    ///its equivalent System.String representation consisting of base 64 digits. 
                    /// Parameters specify the subset as an offset in the input array, 
                    /// and the number of elements in the array to convert.
                    return Convert.ToBase64String(_enCryptedData, 0, (int)_dataStream.Length);

                }
                finally
                {
                    ///Closes current writer streamwriter.
                    if (_theWriter != null)
                        _theWriter.Close();

                    ///Closes current stram and releases resources associated with current stream
                    if (_encryptedStream != null)
                        _encryptedStream.Close();

                    ///Closes stream for reading or writing.
                    if (_dataStream != null)
                        _dataStream.Close();
                }
            }
        }

        public static string DecryptString(string encryptedString)
        {
            if (encryptedString == String.Empty)
                return encryptedString;
            else
            {

                MemoryStream _dataStream = null;
                CryptoStream _encryptedStream = null;
                ICryptoTransform _deCryptor = null;
                int _strLen;

                ///Converts the specified System.String which encodes binary data as base 64 digits, 
                /// to an equivalent 8-bit unsigned integer array.

                byte[] _encryptedData = Convert.FromBase64String(encryptedString);

                ///Creates a store whose backing store is memory.
                _dataStream = new MemoryStream();
                try
                {
                    ///Creates a symmetric decryptor object with the current 
                    /// System.Security.Cryptography.SymmetricAlgorithm.Key and initialization vector 
                    /// (System.Security.Cryptography.SymmetricAlgorithm.IV).
                    _deCryptor = _Provider.CreateDecryptor();

                    ///Defines a stream that links data streams to cryptograpic transformations.
                    _encryptedStream = new CryptoStream(_dataStream, _deCryptor, CryptoStreamMode.Write);

                    ///Writes a sequence of bytes to the current System.Security.Cryptography.CryptoStream
                    /// and advances the current position within this 
                    /// stream by the number of bytes written.
                    _encryptedStream.Write(_encryptedData, 0, (int)_encryptedData.Length);

                    ///Updates the underlying data source or repository with the current state of the buffer, 
                    /// then clears the buffer.
                    _encryptedStream.FlushFinalBlock();

                    ///Sets the current position within the stream
                    _dataStream.Position = 0;

                    ///Gets the length of the stream in bytes
                    _strLen = (int)_dataStream.Length;

                    ///Reads a block of bytes from the current stream and writes the data to buffer.
                    _dataStream.Read(_encryptedData, 0, _strLen);

                    string _retStr = null;

                    ///Iterate through each character & construct a decrrypted string

                    for (int i = 0; i < _strLen; i++)
                    {
                        _retStr += Convert.ToChar(_encryptedData[i]);
                    }
                    return _retStr;
                }
                finally
                {
                    if (_encryptedStream != null)
                        _encryptedStream.Close();

                    if (_dataStream != null)
                        _dataStream.Close();
                }
            }
        }

        #endregion
    }
}
