﻿using System;
using Fw.Controller.Data;

namespace Fw.Controller.Extentions
{
    public static class ApplicationErrorLogger
    {
        private static MySqlConnectionManager _MySqlConnectionManager;

        private static MySqlConnectionManager MySqlConnectionManagers
        {
            get
            {
                if (_MySqlConnectionManager == null)
                    _MySqlConnectionManager = new MySqlConnectionManager();

                return _MySqlConnectionManager;
            }
        }

        public static void LogApplicationError(Exception ex)
        {
            string _query = "insert into ApplicationErroLog(MsgTittle,MsgDetails,LogDateTime) values('{0}','{1}','{2}')";
            try
            {
                MySqlConnectionManagers.ExecuteScalar(_query, ex.Message, ex.ToString(), DateTime.Now);
            }
            catch { }
        }
    }
}
