﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.Extentions
{
    public static class AmountInWordsConverter
    {
        #region Declarations

        private const short MILLION = 1;
        private const short LAKHS = 2;

        #endregion

        #region Public Functions

        public static string GetAmountInWords(object amount, short currencyNotation, string currencyPrefix, string currenceySuffix)
        {
            int _tempIndex, _tempIndexExpo;
            long _rupees = 0, _paise = 0;
            string _strRupees, _strPaise, _str, _expostr, _amountStr = null;
            try
            {
                amount = Math.Abs(Convert.ToDouble(amount));

                _amountStr = amount.ToString();

                if (_amountStr.Contains("E"))
                {
                    _tempIndexExpo = _amountStr.IndexOf("E");
                    _tempIndex = _amountStr.IndexOf(".");

                    //length of the value between decimal point and exponetial symbol E
                    int length = (_tempIndexExpo - _tempIndex) - 1;

                    //to get the exponential value 
                    _expostr = _amountStr.Substring(_tempIndexExpo + 2);

                    if (!_amountStr.Contains("-"))
                    {
                        //to concatenate strings before and after decimal point except [decimal point 
                        //and value after Exponential E].
                        if (_tempIndex > 0)
                        {
                            _str = _amountStr.Substring(0, _tempIndex);
                            _str = _str + _amountStr.Substring(_tempIndex + 1, length);
                        }
                        else
                            _str = _amountStr.Substring(0, _tempIndexExpo);

                        int _strLength = _str.Length;
                        if (_strLength < 20)
                        {
                            int maxbeforeexpo = 20 - _str.Length;
                            _str = _str.PadRight(maxbeforeexpo, '0');//To adjust the length of string as zeros are truncted by ToString function
                            if (_strLength != _str.Length)		//as in special cases PadRight does not pads the value,so no need to decrease exponent
                                _expostr = Convert.ToString(Convert.ToInt32(_expostr) - maxbeforeexpo);
                        }

                        _rupees = Convert.ToInt64(_str);

                        if (currencyNotation == LAKHS)
                        {
                            _strRupees = (_rupees == 0) ? "zero " : InWordsUsingLacsNotation(_rupees);
                            _amountStr = _strRupees + "With Exponent " + currencyPrefix + InWordsUsingLacsNotation(Convert.ToInt64(_expostr)) + " , and " + currenceySuffix + "zero only.";
                        }
                        else
                        {
                            _strRupees = (_rupees == 0) ? " zero " : InWordsUsingMillionNotation(_rupees);
                            _amountStr = _strRupees + "With Exponent " + currencyPrefix + InWordsUsingMillionNotation(Convert.ToInt64(_expostr)) + " , and " + currenceySuffix + " zero only.";
                        }
                    }
                    else
                    {
                        //in this case amount will be in paise only
                        if (_tempIndex > 0)
                        {
                            _str = _amountStr.Substring(0, _tempIndex);
                            _str = _str + _amountStr.Substring(_tempIndex + 1, length);
                        }
                        else
                            _str = _amountStr.Substring(0, _tempIndexExpo);
                        // _amountStr = "Rupees zero and ";
                        _amountStr = currencyPrefix + " zero and ";

                        short expovalue = Convert.ToInt16(_expostr);

                        if (expovalue > 2)//value greater than zero tells that there are more than 2 zeros at start of paise 							
                        {
                            while (expovalue > 1)
                            {
                                _amountStr = _amountStr + "zero ";
                                expovalue--;
                            }

                            if (currencyNotation == LAKHS)
                                _amountStr = _amountStr + currenceySuffix + InWordsUsingLacsNotation(Convert.ToInt16(_str)) + " only";
                            else
                                _amountStr = _amountStr + currenceySuffix + InWordsUsingMillionNotation(Convert.ToInt16(_str)) + " only";
                        }
                        else
                        {
                            if (currencyNotation == LAKHS)
                                _amountStr = _amountStr + currenceySuffix + InWordsUsingLacsNotation(Convert.ToInt16(_str)) + " only";
                            else
                                _amountStr = _amountStr + currenceySuffix + InWordsUsingMillionNotation(Convert.ToInt16(_str)) + " only";
                        }
                    }
                }
                else
                {
                    if (_amountStr.Contains("."))
                    {
                        // Calculating Rupees Amount
                        _tempIndex = _amountStr.IndexOf(".");
                        _str = _amountStr.Substring(0, _tempIndex);

                        if (_str == null)
                            _rupees = 0;
                        else
                            _rupees = Convert.ToInt64(_str);

                        // Calculating Paise Amount
                        _str = _amountStr.Substring(_tempIndex + 1, _amountStr.Length - _tempIndex - 1);

                        if (_str == null)
                            _paise = 0;
                        else
                        {
                            _str = (_str.Length == 1) ? (_str + "0") : _str;
                            _str = (_str.Length > 2) ? (_str.Substring(0, 2)) : _str;

                            _paise = Convert.ToInt64(_str);
                        }
                    }
                    else
                        _rupees = Convert.ToInt64(amount);

                    if (currencyNotation == LAKHS)
                    {
                        _strRupees = (_rupees == 0) ? "zero " : InWordsUsingLacsNotation(_rupees);
                        _strPaise = (_paise == 0) ? GetZeroNotation() : InWordsUsingLacsNotation(_paise);
                    }
                    else
                    {
                        _strRupees = (_rupees == 0) ? " zero " : InWordsUsingMillionNotation(_rupees);
                       // _strPaise = (_paise == 0) ? " zero " : InWordsUsingMillionNotation(_paise);
                        _strPaise = (_paise == 0) ? GetZeroNotation() : InWordsUsingMillionNotation(_paise);
                    }


                  //  _amountStr = currencyPrefix + " " + _strRupees + " and " + currenceySuffix + " " + _strPaise + " only.";
                    _amountStr = currencyPrefix + " " + _strRupees + GetSuffixNotation(currenceySuffix, _strPaise) + " only.";
                }
            }
            catch (ArithmeticException)
            {
            }
            return _amountStr.ToUpper();
        }

        #endregion

        #region Helper Functions

        private static string GetSuffixNotation(string currenceySuffix,string paise)
        {
            if (!string.IsNullOrEmpty(paise))
                return (" and " + currenceySuffix + " " + paise);
            return string.Empty;
        }

        private static string GetZeroNotation()
        {
            return string.Empty;
        }

        private static string InWordsUsingLacsNotation(long amount)
        {
            long TransactionAmount0 = 99;
            long TransactionAmount1 = 999;
            long TransactionAmount2 = 99999;
            long TransactionAmount3 = 9999999;

            string TransactionUnitSingular1 = "hundred";
            string TransactionUnitSingular2 = "thousand";
            string TransactionUnitSingular3 = "lakh";
            string TransactionUnitSingular4 = "Crore";

            string TransactionUnitPlural1 = "hundred";
            string TransactionUnitPlural2 = "thousand";
            string TransactionUnitPlural3 = "lakh";
            string TransactionUnitPlural4 = "Crore";

            long _amount = 0;
            if (amount >= 1 && amount <= 19)
            {
                return GetWord(amount);
            }
            else if (amount <= TransactionAmount0)
            {
                return GetWord(Convert.ToInt64(Math.Truncate(Convert.ToDouble(amount) / 10)) * 10) + GetWord(amount % 10);
            }
            else if (amount <= TransactionAmount1)
            {
                _amount = Convert.ToInt64(Math.Truncate(Convert.ToDouble(amount) / (TransactionAmount0 + 1)));
                return InWordsUsingLacsNotation(_amount)
                    + " " + (Convert.ToString(_amount) == "1" ? TransactionUnitSingular1 : TransactionUnitPlural1)
                    + " " + InWordsUsingLacsNotation(amount % (TransactionAmount0 + 1));
            }
            else if (amount <= TransactionAmount2)
            {
                _amount = Convert.ToInt64(Math.Truncate(Convert.ToDouble(amount) / (TransactionAmount1 + 1)));
                return InWordsUsingLacsNotation(_amount)
                    + " " + (Convert.ToString(_amount) == "1" ? TransactionUnitSingular2 : TransactionUnitPlural2)
                    + " " + InWordsUsingLacsNotation(amount % (TransactionAmount1 + 1));
            }
            else if (amount <= TransactionAmount3)
            {
                _amount = Convert.ToInt64(Math.Truncate(Convert.ToDouble(amount) / (TransactionAmount2 + 1)));
                return InWordsUsingLacsNotation(_amount)
                    + " " + (Convert.ToString(_amount) == "1" ? TransactionUnitSingular3 : TransactionUnitPlural3)
                    + " " + InWordsUsingLacsNotation(amount % (TransactionAmount2 + 1));
            }
            else
            {
                _amount = Convert.ToInt64(Math.Truncate(Convert.ToDouble(amount) / (TransactionAmount3 + 1)));
                return InWordsUsingLacsNotation(_amount)
                    + " " + (Convert.ToString(_amount) == "1" ? TransactionUnitSingular4 : TransactionUnitPlural4)
                    + " " + InWordsUsingLacsNotation(amount % (TransactionAmount3 + 1));
            }
        }

        private static string InWordsUsingMillionNotation(long inputNum)
        {
            int dig1, dig2, dig3, level = 0, lasttwo, threeDigits;
            string dollars, x = "", s = "";

            dollars = "";

            //they had zero for ones and tens but that gave ninety zero for 90 
            string[] ones = { "", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            string[] tens = { "", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };
            string[] thou = { "", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion" };

            bool isNegative = false;
            if (inputNum < 0)
            {
                isNegative = true;
                inputNum *= -1;
            }
            if (inputNum == 0)
                return "zero";

            s = inputNum.ToString();

            while (s.Length > 0)
            {
                //Get the three rightmost characters 
                x = (s.Length < 3) ? s : s.Substring(s.Length - 3, 3);

                // Separate the three digits 
                threeDigits = int.Parse(x);
                lasttwo = threeDigits % 100;
                dig1 = threeDigits / 100;
                dig2 = lasttwo / 10;
                dig3 = (threeDigits % 10);

                // append a "thousand" where appropriate 
                if (level > 0 && dig1 + dig2 + dig3 > 0)
                {
                    dollars = thou[level] + " " + dollars;
                    dollars = dollars.Trim();
                }

                // check that the last two digits is not a zero 
                if (lasttwo > 0)
                {
                    if (lasttwo < 20)
                    {
                        // if less than 20, use "ones" only 
                        dollars = ones[lasttwo] + " " + dollars;
                    }
                    else
                    {
                        // otherwise, use both "tens" and "ones" array 
                        dollars = tens[dig2] + " " + ones[dig3] + " " + dollars;
                    }
                    if (s.Length < 3)
                    {
                        if (isNegative) { dollars = "negative " + dollars; }
                        return dollars;
                    }
                }

                // if a hundreds part is there, translate it 
                if (dig1 > 0)
                {
                    dollars = ones[dig1] + " hundred " + dollars;
                    s = (s.Length - 3) > 0 ? s.Substring(0, s.Length - 3) : "";
                    level++;
                }
                else
                {
                    if (s.Length > 3)
                    {
                        s = s.Substring(0, s.Length - 3);
                        level++;
                    }
                }
            }

            if (isNegative) { dollars = "negative " + dollars; }
            return dollars;
        }

        private static string GetWord(long number)
        {
            switch (number)
            {
                case 1:
                    return "one ";
                case 2:
                    return "two ";
                case 3:
                    return "three ";
                case 4:
                    return "four ";
                case 5:
                    return "five ";
                case 6:
                    return "six ";
                case 7:
                    return "seven ";
                case 8:
                    return "eight ";
                case 9:
                    return "nine ";
                case 10:
                    return "ten ";
                case 11:
                    return "eleven ";
                case 12:
                    return "twelve ";
                case 13:
                    return "thirteen ";
                case 14:
                    return "fourteen ";
                case 15:
                    return "fifteen ";
                case 16:
                    return "sixteen ";
                case 17:
                    return "seventeen ";
                case 18:
                    return "eighteen ";
                case 19:
                    return "nineteen ";
                case 20:
                    return "twenty ";
                case 30:
                    return "thirty ";
                case 40:
                    return "forty ";
                case 50:
                    return "fifty ";
                case 60:
                    return "sixty ";
                case 70:
                    return "seventy ";
                case 80:
                    return "eighty ";
                case 90:
                    return "ninety ";
            }

            return "";
        }

        #endregion
    }
}
