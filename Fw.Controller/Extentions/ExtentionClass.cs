﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Windows.Media;

namespace Fw.Controller.Extentions
{
    public static class ExtentionClass
    {
        #region Declaration

        public static Regex _Regex = null;

        #endregion

        #region Static extended methods

        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' },
                             StringSplitOptions.RemoveEmptyEntries).Length;
        }

        public static string SpliteCamelCaseString(this String str)
        {
            return str.IsNull() ? string.Empty : System.Text.RegularExpressions.Regex.Replace(str, "([a-z](?=[A-Z])|[A-Z](?=[A-Z][a-z]))", "$1 ");
            //return string.IsNullOrEmpty(str) ? string.Empty : string.Concat(str.Select(x => Char.IsUpper(x) ? " " + x : x.ToString())).TrimStart(' ');
        }

        public static bool IsValidEmail(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return true;

            _Regex = new Regex(@"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidMultipleEmail(this string value)
        {
            if (value.IsNull()) return true;

            var _emails = value.Split(new string[] { ",", ";", ":" }, StringSplitOptions.RemoveEmptyEntries).ToArray();

            return _emails.All(str => str.IsValidEmail());
        }

        public static bool IsValidInteger(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^[0-9]+$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidMultipleIntegers(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            return value.Split(new char[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries).Where(a => !(new Regex(@"^[0-9]+$")).IsMatch(a) || !a.IsValidInteger()).Select(a => false).FirstOrDefault();
        }

        public static bool IsValidDecimal(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^-?[0-9]\d*(.\d+)?$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidMobile(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^\d{10}$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidContactNo(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^\+?[0-9-]+$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidDate(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^([1-9]|0[1-9]|1\d|2\d|3[0-1])[- / .]([1-9]|0[1-9]|1[0-2])[- / .](1[9]\d\d|2[0]\d\d)$");

            if (_Regex.IsMatch(value))
            {
                string[] _dateArrat = value.Split(new char[] { '-', '/', '.' }, StringSplitOptions.RemoveEmptyEntries);

                if (!CheckDayOfMonth(Convert.ToInt32(_dateArrat[1]), Convert.ToInt32(_dateArrat[0]), Convert.ToInt32(_dateArrat[2])))
                    return false;
                else
                    return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsValidProcessingFormula(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"^(\d|[.]|[D]|[W]|[C]|[(]|[)]|[-]|[+]|[*]|[/]|[%])+$");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidPhoneNoWithArea(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"\d{3}-\d{3}-\d{4}");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidSSN(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            _Regex = new Regex(@"\d{3}-\d{2}-\d{4}");

            if (_Regex.IsMatch(value))
                return true;
            else
                return false;
        }

        public static bool IsValidIpAddress(this string value)
        {
            if (string.IsNullOrEmpty(value)) return true;

            short cnt = 0;
            bool _error = false;
            int len = value.Length;
            for (short i = 0; i < len; i++)
                if (value[i] == '.')
                {
                    cnt++;
                    if (i + 1 < len)
                        if (value[i + 1] == '.')
                        {
                            _error = true;
                            break;
                        }
                }
                else
                    return true;
            if ((cnt < 3 || value[len - 1] == '.') || _error)
                return false;
            else
                return true;
        }

        public static bool IsValidCreditCardNo(this string value)
        {
            string pattern = @" (^((?<Number>\d{4})(?:[\s-]?)){4}(?!\d))$ | \d{16}$";

            var match = Regex.Match(value.Trim(), pattern, RegexOptions.IgnorePatternWhitespace).Groups["Number"].Captures.OfType<Capture>().Aggregate(string.Empty, (seed, current) => seed + current);

            if (String.IsNullOrEmpty(match))
                return false;
            else
                return true;
        }

        public static object GetDefault(this Type type)
        {
            if (type.IsValueType)
            {
                return Activator.CreateInstance(type);
            }
            return null;
        }

        public static Object GetPropValue(this Object obj, String name)
        {
            foreach (String part in name.Split('.'))
            {
                if (obj == null) { return null; }

                Type type = obj.GetType();
                var info = type.GetProperty(part);
                if (info == null) { return null; }

                obj = info.GetValue(obj, null);
            }
            return obj;
        }

        public static T GetPropValue<T>(this Object obj, String name)
        {
            Object retval = GetPropValue(obj, name);
            if (retval == null) { return default(T); }

            // throws InvalidCastException if types are incompatible
            return (T)retval;
        }

        /// <summary>
        /// Convert value of object into string, if object is null then get empty string value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectType"></param>
        /// <returns>string value</returns>
        public static string MakeString<T>(this T objectType)
        {
            if (objectType == null) return string.Empty;

            return objectType.ToString();
        }

        public static DateTime GetDateWithoutMilliseconds(this DateTime d)
        {
            return new DateTime(d.Year, d.Month, d.Day, d.Hour, d.Minute, d.Second);
        }

        /// <summary>
        /// Perform a deep Copy of the object.
        /// </summary>
        /// <typeparam name="T">The type of object being copied.</typeparam>
        /// <param name="source">The object instance to copy.</param>
        /// <returns>The copied object.</returns>
        public static T Clone<T>(this T source)
        {
            if (!typeof(T).IsSerializable)
            {
                throw new ArgumentException("The type must be serializable.", "source");
            }

            // Don't serialize a null object, simply return the default for that object
            if (Object.ReferenceEquals(source, null))
            {
                return default(T);
            }

            IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            Stream stream = new MemoryStream();
            using (stream)
            {
                formatter.Serialize(stream, source);
                stream.Seek(0, SeekOrigin.Begin);
                return (T)formatter.Deserialize(stream);
            }
        }

        /// <summary>
        /// Check if object value is null, DBNull, Empty in-case of string or MinDate in case of DateTime.
        /// </summary>
        /// <param name="obj">Object value for check.</param>
        /// <returns>Return true if object satisfy min requirement</returns>
        public static bool IsNull(this object obj)
        {
            if (obj == null || obj == DBNull.Value)
                return true;

            //Add min check for Value Types.
            if (obj is DateTime)
            {
                DateTime _dateValue = DateTime.MinValue;

                if (DateTime.TryParse(obj.ToString(), out _dateValue) && _dateValue == DateTime.MinValue)
                    return true;
            }

            if (obj.GetType().IsValueType) return false;

            //Add min check for reference type.
            if (obj is string)
                return string.IsNullOrEmpty(obj.ToString());

            return false;
        }

        /// <summary>
        /// Convert list of class objects into dataTable.
        /// </summary>
        /// <typeparam name="T">Type of enumerable list collection type</typeparam>
        /// <param name="collection">Collection of class object</param>
        /// <param name="tableName">Table Name created by list.</param>
        /// <returns></returns>
        public static DataTable ToDataTable<T>(this IEnumerable<T> collection, string tableName = null)
        {
            DataTable dt = new DataTable(tableName ?? typeof(T).Name);
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties(BindingFlags.Public | BindingFlags.Instance);

            //Inspect the properties and create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                //Type ColumnType = pi.PropertyType;
                Type _columnType = Nullable.GetUnderlyingType(pi.PropertyType) ?? pi.PropertyType;

                if ((_columnType.IsGenericType))
                {
                    _columnType = _columnType.GetGenericArguments()[0];
                }
                dt.Columns.Add(pi.Name, _columnType);
            }

            //Populate the data table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    if (pi.GetValue(item, null) != null)
                    {
                        dr[pi.Name] = pi.GetValue(item, null);
                    }
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }

        /// <summary>
        /// This will convert data table rows into list of class of type T
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static IEnumerable<T> ConvertToClassList<T>(this DataTable dt)
        {
            if (dt.IsNull())
                throw new ArgumentNullException("Data table is null");

            List<T> lst = new List<T>();
            Type tClass = typeof(T);
            var pClass = tClass.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();

            T cn;
            foreach (DataRow item in dt.Rows)
            {
                cn = (T)Activator.CreateInstance(tClass);

                if (cn is ValueType || cn is string)
                    cn = (T)item[0];
                else
                {
                    foreach (var pc in pClass)
                    {
                        // Can comment try catch block. 
                        DataColumn d = dc.Find(c => c.ColumnName == pc.Name);
                        if (d != null)
                            pc.SetValue(cn, (DBNull.Value == item[pc.Name]) ? pc.PropertyType.GetDefault() : Convert.ChangeType(item[pc.Name], Nullable.GetUnderlyingType(pc.PropertyType) ?? pc.PropertyType), null);
                    }
                }
                lst.Add(cn);
            }
            return lst;
        }

        public static IEnumerable<T> ConvertToClassList<T>(this DataTable dt, params object[] classContructorParam)
        {
            if (dt.IsNull())
                throw new ArgumentNullException("Data table is null");

            List<T> lst = new List<T>();
            Type tClass = typeof(T);
            var pClass = tClass.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();

            T cn;
            foreach (DataRow item in dt.Rows)
            {
                cn = (T)Activator.CreateInstance(tClass, classContructorParam);

                if (cn is ValueType || cn is string)
                    cn = (T)item[0];
                else
                {
                    foreach (var pc in pClass)
                    {
                        // Can comment try catch block. 
                        DataColumn d = dc.Find(c => c.ColumnName == pc.Name);
                        if (d != null)
                            pc.SetValue(cn, (DBNull.Value == item[pc.Name]) ? pc.PropertyType.GetDefault() : Convert.ChangeType(item[pc.Name], Nullable.GetUnderlyingType(pc.PropertyType) ?? pc.PropertyType), null);
                    }
                }
                lst.Add(cn);
            }
            return lst;
        }

        /// <summary>
        /// Compare string with case ordinal or non-ordinal
        /// </summary>
        /// <param name="source"></param>
        /// <param name="toCheck"></param>
        /// <param name="comp"></param>
        /// <returns></returns>
        public static bool Contains(this string source, string toCheck, StringComparison comp)
        {
            return source.IndexOf(toCheck, comp) >= 0;
        }

        #endregion

        #region Private methods

        private static bool CheckDayOfMonth(int mon, int day, int year = 0)
        {
            bool ret = true;
            if (day == 0) ret = false;
            switch (mon)
            {
                case 1:
                    if (day > 31)
                        ret = false;
                    break;
                case 2:
                    if (year == 0)
                    {
                        System.DateTime moment = DateTime.Now;
                        year = moment.Year;
                    }
                    int d = ((year % 4 == 0) && ((!(year % 100 == 0)) || (year % 400 == 0))) ? 29 : 28;
                    if (day > d)
                        ret = false;
                    break;
                case 3:
                    if (day > 31)
                        ret = false;
                    break;
                case 4:
                    if (day > 30)
                        ret = false;
                    break;
                case 5:
                    if (day > 31)
                        ret = false;
                    break;
                case 6:
                    if (day > 30)
                        ret = false;
                    break;
                case 7:
                    if (day > 31)
                        ret = false;
                    break;
                case 8:
                    if (day > 31)
                        ret = false;
                    break;
                case 9:
                    if (day > 30)
                        ret = false;
                    break;
                case 10:
                    if (day > 31)
                        ret = false;
                    break;
                case 11:
                    if (day > 30)
                        ret = false;
                    break;
                case 12:
                    if (day > 31)
                        ret = false;
                    break;
                default:
                    ret = false;
                    break;
            }
            return ret;
        }

        #endregion
    }

    public static class VisualHelper
    {
        public static T FindVisualChild<T>(this Visual parent) where T : Visual
        {
            List<T> childs = new List<T>();

            return GetVisualChild(parent, true, ref childs);
        }

        public static IEnumerable<T> FindVisualChilds<T>(this Visual parent) where T : Visual
        {
            List<T> childs = new List<T>();
            GetVisualChild(parent, false, ref childs);
            return childs;
        }

        public static T FindVisualChildByName<T>(this Visual parent, string name) where T : Visual
        {
            if (string.IsNullOrWhiteSpace(name))
                return null;

            List<T> childs = new List<T>();

            return GetVisualChild(parent, true, ref childs, true, name);
        }

        private static T GetVisualChild<T>(this Visual parent, bool getOnlyOnechild, ref List<T> list, bool findByName = false, string childName = "") where T : Visual
        {
            T child = default(T);

            for (int index = 0; index < VisualTreeHelper.GetChildrenCount(parent); index++)
            {
                Visual visualChild = (Visual)VisualTreeHelper.GetChild(parent, index);
                child = visualChild as T;

                if (child == null)
                    child = GetVisualChild<T>(visualChild, getOnlyOnechild, ref list);//Find Recursively

                if (child != null)
                {
                    if (getOnlyOnechild)
                    {
                        if (findByName)
                        {
                            var element = child as System.Windows.FrameworkElement;
                            if (element != null && element.Name == childName)
                                break;
                        }
                        else
                            break;
                    }
                    else
                        list.Add(child);
                }
            }
            return child;
        }
    }
}
