﻿using System.ComponentModel;
using System.Linq;
using System.Windows.Controls;
using Fw.Controller.Interfaces;
using System.Windows.Input;

namespace Fw.Controller.ControlsWpf
{
    public partial class FwComboBox : ComboBox, IFwControl
    {
        private Data.MySqlConnectionManager _MySqlConnectionManager = null;

        public FwComboBox()
        {
            InitializeComponent();
        }

        private Data.MySqlConnectionManager MySqlConnectionManagers
        {
            get
            {
                if (_MySqlConnectionManager == null)
                {
                    _MySqlConnectionManager = new Data.MySqlConnectionManager();
                    return _MySqlConnectionManager;
                }
                else
                    return _MySqlConnectionManager;
            }
        }

        [Category("FwProperties")]
        public bool IsMandatory { get; set; }

        [Category("FwProperties")]
        public string LabelName { get; set; }

        [Category("FwProperties")]
        public string SourcePropertyName { get; set; }

        public System.Windows.DependencyProperty ControlProperty
        {
            get { return ComboBox.SelectedValueProperty; }
        }

        public System.Windows.Data.BindingMode BindingMode
        {
            get { return System.Windows.Data.BindingMode.TwoWay; }
        }

        [Category("FwProperties")]
        public string SourceQuery { get; set; }

        [Category("FwProperties")]
        public string FwDisplayMember { get; set; }

        [Category("FwProperties")]
        public string FwValueMember { get; set; }

        [Category("FwProperties")]
        public int PickListMasterID { get; set; }

        public void RefreshDataContext()
        {
            if (string.IsNullOrEmpty(SourceQuery) && string.IsNullOrEmpty(FwValueMember) && string.IsNullOrEmpty(FwDisplayMember) && PickListMasterID == 0) return;

            if (PickListMasterID != 0)
            {
                SourceQuery = string.Format("select ValueID,ValueName from PicklistValues where HeaderId = {0}", PickListMasterID);
                FwValueMember = "ValueID";
                FwDisplayMember = "ValueName";
            }

            DisplayMemberPath = "DisplayMember";
            SelectedValuePath = "ValueMember";
            //
            var source = MySqlConnectionManagers.Fill(SourceQuery).Rows.Cast<System.Data.DataRow>().Select(a => new
            {
                ValueMember = a[FwValueMember],
                DisplayMember = a[FwDisplayMember].ToString()
            }).ToList();

            ItemsSource = source;
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            base.OnKeyDown(e);

            if (e.Key == Key.Delete)
                SelectedIndex = -1;

            //if ((Keyboard.IsKeyDown(Key.LeftCtrl) || Keyboard.IsKeyDown(Key.RightCtrl)) && Keyboard.IsKeyDown(Key.Space))
            //    IsDropDownOpen = !IsDropDownOpen;

            if (Keyboard.IsKeyDown(Key.Space) && !IsDropDownOpen)
                IsDropDownOpen = true;
        }

        public override void OnApplyTemplate()
        {
            base.OnApplyTemplate();

            //this.Style = Resources["MetroComboBox"] as System.Windows.Style;
        }
    }
}
