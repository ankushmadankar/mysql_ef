﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Controls;

namespace Fw.Controller.ControlsWpf
{
    public partial class FwDatePicker : DatePicker, Fw.Controller.Interfaces.IFwControl
    {
        public FwDatePicker()
        {
            InitializeComponent();
        }

        [Category("FwProperties")]
        public bool IsMandatory { get; set; }

        [Category("FwProperties")]
        public string LabelName { get; set; }

        [Category("FwProperties")]
        public string SourcePropertyName { get; set; }

        public System.Windows.DependencyProperty ControlProperty
        {
            get { return DatePicker.SelectedDateProperty; }
        }

        public System.Windows.Data.BindingMode BindingMode
        {
            get { return System.Windows.Data.BindingMode.TwoWay; }
        }
    }
}
