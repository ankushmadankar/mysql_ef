﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Controls;
using Fw.Controller.Interfaces;

namespace Fw.Controller.ControlsWpf
{
    public partial class FwTextBox : TextBox, IFwControl
    {
        public FwTextBox()
        {
            InitializeComponent();
        }

        [Category("FwProperties")]
        public bool IsMandatory { get; set; }

        [Category("FwProperties")]
        public string LabelName { get; set; }

        [Category("FwProperties")]
        public string SourcePropertyName { get; set; }

        [DefaultValue(FwTextMask.None)]
        [Category("FwProperties")]
        public FwTextMask Mask { get; set; }

        public System.Windows.DependencyProperty ControlProperty
        {
            get { return TextBox.TextProperty; }
        }

        public System.Windows.Data.BindingMode BindingMode
        {
            get { return System.Windows.Data.BindingMode.TwoWay; }
        }

        protected override void OnPreviewTextInput(System.Windows.Input.TextCompositionEventArgs e)
        {
            base.OnPreviewTextInput(e);

            char inputChar = Convert.ToChar(e.Text);

            switch (Mask)
            {
                case FwTextMask.Digits:
                    if (!char.IsDigit(inputChar))
                    {
                        e.Handled = true;
                    }
                    break;

                case FwTextMask.Decimal:
                    if (!IsDecimal(inputChar))
                    {
                        e.Handled = true;
                    }
                    break;

                case FwTextMask.Letters:
                    if (!char.IsLetter(inputChar))
                    {
                        e.Handled = true;
                    }
                    break;

                case FwTextMask.Punctuation:
                    if (!char.IsPunctuation(inputChar))
                    {
                        e.Handled = true;
                    }
                    break;

                case FwTextMask.Symbol:
                    if (!char.IsSymbol(inputChar))
                    {
                        e.Handled = true;
                    }
                    break;

                case FwTextMask.Code:
                    if (!(char.IsLetterOrDigit(inputChar) || inputChar == '-' || inputChar == '_'))
                    {
                        e.Handled = true;
                    }
                    break;
                default:
                    break;
            }
        }

        private bool IsDecimal(char ch)
        {
            //enable to using Keyboard Ctrl+C and Keyboard Ctrl+V
            if (ch == (char)3 || ch == (char)22 || ch == (char)24 || ch == (char)26)
                return true;

            if (Char.IsDigit(ch) || ch == '.' || ch == 8)
            {
                // if select all reset vars
                if (this.SelectionLength == this.Text.Length)
                {
                    if (ch != (char)22)
                        this.Text = null;
                }

                string str = this.Text;

                if (ch == '.')
                {
                    int indx = str.IndexOf('.', 0);
                    if (indx > 0)
                        return false;

                    if (string.IsNullOrEmpty(str) || str.Length == 0)
                        return false;
                }

                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum FwTextMask
    {
        /// <summary>
        /// Any string value.
        /// </summary>
        None,
        /// <summary>
        /// Numbers from 0 to 9.
        /// </summary>
        Digits,
        /// <summary>
        /// Numbers from 0 to 9 and only one .
        /// </summary>
        Decimal,
        /// <summary>
        /// Letters only.
        /// </summary>
        Letters,
        /// <summary>
        /// Punctuation only.
        /// </summary>
        Punctuation,
        /// <summary>
        /// Sysbol only.
        /// </summary>
        Symbol,
        /// <summary>
        /// Allow letters,digits, _ and - only.
        /// </summary>
        Code
    }
}