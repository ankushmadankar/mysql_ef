﻿//using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Diagnostics;
//using System.Linq;
//using System.Text;
//using Fw.Controller.Extentions;
//using DevExpress.Xpf.Grid.LookUp;
//using Fw.Controller.Interfaces;
//using System.Data;

//namespace Fw.Controller.ControlsWpf
//{
//    public partial class FwLookUpEdit : LookUpEdit, IFwControl
//    {
//        private Data.MySqlConnectionManager _MySqlConnectionManager = null;
//        private DataTable _DataSource;

//        public FwLookUpEdit()
//        {
//            InitializeComponent();
//        }

//        //public FwLookUpEdit(IContainer container)
//        //{
//        //    container.Add(this);

//        //    InitializeComponent();
//        //}

//        public override void OnApplyTemplate()
//        {
//            //this.Style = Resources["ResourceName"] as System.Windows.Style;
//            //DependencyObject d = GetTemplateChild("PART_KeyboardPopupButton");
//            //if (d != null)
//            //{
//            //    (d as Button).Click += new RoutedEventHandler(KeyboardPopupButton_Click);
//            //}
//            //var d = this.GetTemplateChild("Popup");

//            //if (!d.IsNull())
//            //    (d as System.Windows.Controls.Primitives.Popup). = "";

//            if (StyleSettings.IsNull())
//                StyleSettings = new SearchLookUpEditStyleSettings();

//            if (!IsMandatory)
//                AllowNullInput = true;

//            base.OnApplyTemplate();
//        }

//        private Data.MySqlConnectionManager MySqlConnectionManagers
//        {
//            get
//            {
//                if (_MySqlConnectionManager == null)
//                {
//                    _MySqlConnectionManager = new Data.MySqlConnectionManager();
//                    return _MySqlConnectionManager;
//                }
//                else
//                    return _MySqlConnectionManager;
//            }
//        }

//        [Category("FwProperties")]
//        public bool IsMandatory { get; set; }

//        [Category("FwProperties")]
//        public string LabelName { get; set; }

//        [Category("FwProperties")]
//        public string SourcePropertyName { get; set; }

//        public System.Windows.DependencyProperty ControlProperty
//        {
//            get { return LookUpEdit.EditValueProperty; }
//        }

//        public System.Windows.Data.BindingMode BindingMode
//        {
//            get { return System.Windows.Data.BindingMode.TwoWay; }
//        }

//        [Category("FwProperties")]
//        public string SourceQuery { get; set; }

//        [Category("FwProperties")]
//        public string FwDisplayMember { get; set; }

//        [Category("FwProperties")]
//        public string FwValueMember { get; set; }

//        [Category("FwProperties")]
//        public string DisplayColumnsName { get; set; }

//        [DefaultValue(true)]
//        [Category("FwProperties")]
//        public bool IsBestFit { get; set; }

//        public void RefreshDataContext()
//        {
//            if (string.IsNullOrEmpty(SourceQuery) && string.IsNullOrEmpty(FwDisplayMember)) return;

//            _DataSource = null;

//            DisplayMember = FwDisplayMember;
//            ValueMember = !FwValueMember.IsNull() ? FwValueMember : "Id";
//            //
//            ItemsSource = DataSource;

//            if (!AutoPopulateColumns)
//                PopupContentTemplate = (System.Windows.Controls.ControlTemplate)System.Windows.Markup.XamlReader.Parse(GetPopUpTemplate());
//        }

//        private string[] DisplayMembersArray
//        {
//            get
//            {
//                if (SourceQuery.IsNull()) return null;

//                if (DisplayColumnsName.IsNull())
//                {
//                    var columnsName = DataSource.Columns.Cast<DataColumn>().Select(a => a.ColumnName).ToList();

//                    if (!columnsName.IsNull() && columnsName.Count() > 0)
//                    {
//                        if (columnsName.Contains("Id", StringComparer.OrdinalIgnoreCase))
//                            columnsName.Remove("Id");

//                        return columnsName.ToArray();
//                    }
//                    else
//                        return null;
//                }
//                else
//                {
//                    return DisplayColumnsName.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries);
//                }
//            }
//        }

//        private DataTable DataSource
//        {
//            get { return _DataSource ?? (_DataSource = MySqlConnectionManagers.Fill(SourceQuery)); }
//        }

//        private string GetPopUpTemplate()
//        {
//            string template = "<ControlTemplate xmlns=\"http://schemas.microsoft.com/winfx/2006/xaml/presentation\" " +
//                "xmlns:dxg=\"http://schemas.devexpress.com/winfx/2008/xaml/grid\"> " +
//            "<dxg:GridControl Name=\"PART_GridControl\"> " +
//                "<dxg:GridControl.Columns> " +
//                    GetGridColumnField() +
//                "</dxg:GridControl.Columns> " +
//                "<dxg:GridControl.View> " +
//                    "<dxg:TableView ShowAutoFilterRow=\"True\" /> " +
//                "</dxg:GridControl.View> " +
//            "</dxg:GridControl> " +
//            "</ControlTemplate> ";
//            return template;
//        }

//        private string GetGridColumnField()
//        {
//            string result = Environment.NewLine;

//            foreach (var a in DisplayMembersArray)
//                result += "<dxg:GridColumn FieldName=\"" + a + "\" Header=\"" + a.SpliteCamelCaseString() + "\" AutoFilterCondition=\"Contains\" AllowBestFit=\"True\" /> " + Environment.NewLine;

//            return result.TrimEnd(Environment.NewLine.ToCharArray());
//        }

//        protected override void OnPopupOpened()
//        {
//            if (IsBestFit)
//            {
//                var grid = GetGridControl();

//                if (!grid.IsNull())
//                    ((DevExpress.Xpf.Grid.TableView)grid.View).BestFitColumns();
//            }

//            base.OnPopupOpened();
//        }
//    }
//}
