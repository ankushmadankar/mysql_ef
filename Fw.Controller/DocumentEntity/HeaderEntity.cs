﻿using Fw.Controller.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Fw.Controller.Extentions;
using Fw.Controller.Base;

namespace Fw.Controller.DocumentEntity
{
    public class HeaderEntity<LineType> : EntityObject
        where LineType : ILineEntity
    {
        private CustomBindingList<LineType> _Lines;
        private CustomBindingList<LineType> _DeletedLines;

        public HeaderEntity(string tableName, EntityManager entityManager)
            : base(tableName, entityManager)
        {
        }

        public HeaderEntity(string tableName, EntityManager entityManager, int id)
            : base(tableName, entityManager, id)
        {
        }

        public BindingList<LineType> Lines
        {
            get
            {
                if (_Lines.IsNull())
                    LoadLines();

                return _Lines;
            }
        }

        protected override void OnBeginSavingDataEntity(BeginSavingDataEntity e)
        {
            base.OnBeginSavingDataEntity(e);

            if (!_DeletedLines.IsNull())
                foreach (var line in _DeletedLines)
                    line.Save(e.Command.Connection as MySql.Data.MySqlClient.MySqlConnection, e.Transaction as MySql.Data.MySqlClient.MySqlTransaction);
        }

        protected override void OnEndSavingDataEntity(EndSavingDataEntity e)
        {
            base.OnEndSavingDataEntity(e);

            foreach (var line in Lines)
            {
                //if (ObjectState == EntityState.Added)
                if (e.GeneratedId > 0)
                    line.HeaderId = e.GeneratedId;
                else if (line.Id == 0 && Id > 0)
                    line.HeaderId = Id;

                if (MarkedDelete)
                    line.MarkedDelete = true;

                line.Save(e.Command.Connection as MySql.Data.MySqlClient.MySqlConnection, e.Transaction as MySql.Data.MySqlClient.MySqlTransaction);
            }
        }

        /// <summary>
        /// Load lines which automatically refresh data for lines.
        /// </summary>
        public void LoadLines()
        {
            DisposeLines();

            _Lines = new CustomBindingList<LineType>();
            //
            _Lines.AddingNew -= Lines_AddingNew;
            _Lines.ListChanged -= Lines_ListChanged;
            //
            _Lines.AddingNew += Lines_AddingNew;
            _Lines.ListChanged += Lines_ListChanged;
            //
            _Lines.BeforeRemove += new CustomBindingList<LineType>.ElementRemovedHandler(Lines_BeforeRemove);
        }

        /// <summary>
        /// Refresh lines if lines already loaded.
        /// </summary>
        public void RefreshLines()
        {
            if (_Lines.Count == 0) return;

            _Lines.ToList().ForEach(a => a.RefreshEntity());
        }

        public void InitLines()
        {
            var lineEntity = (LineType)Activator.CreateInstance(typeof(LineType), EntityManager);

            var table = MySqlConnectionManagers.ExecuteQuery<int>(string.Format("select Id from {0} where HeaderId = {1}", lineEntity.TableName, Id));

            EntityManager.RemoveEntity(lineEntity as EntityObject);

            foreach (var id in table)
            {
                lineEntity = (LineType)Activator.CreateInstance(typeof(LineType), EntityManager, this as HeaderEntity<ILineEntity>, id);

                Lines.Add(lineEntity);
            }
        }

        public void DisposeLines()
        {
            if (_Lines.IsNull() || _Lines.Count == 0) return;

            _Lines.Where(a => a is EntityObject).ToList().ForEach(line => EntityManager.RemoveEntity(line as EntityObject));
        }

        protected virtual void Lines_BeforeRemove(LineType deletedItem)
        {
            if (deletedItem.Id > 0)
            {
                if (_DeletedLines.IsNull())
                    _DeletedLines = new CustomBindingList<LineType>();

                deletedItem.MarkedDelete = true;

                _DeletedLines.Add(deletedItem);
            }
        }

        protected virtual void Lines_ListChanged(object sender, ListChangedEventArgs e)
        {
            //_Lines = (sender as CustomBindingList<LineType>);
        }

        protected virtual void Lines_AddingNew(object sender, AddingNewEventArgs e)
        {
            if (e.NewObject.IsNull())
                e.NewObject = (LineType)Activator.CreateInstance(typeof(LineType), EntityManager, this as HeaderEntity<ILineEntity>, 0);

            (e.NewObject as ILineEntity).HeaderId = Id;
        }

        protected override void OnLoadEntity()
        {
            LoadLines();
            InitLines();
            base.OnLoadEntity();
        }

        public override void Dispose()
        {
            base.Dispose();

            DisposeLines();
        }
    }

    public class CustomBindingList<T> : BindingList<T>
    {
        protected override void RemoveItem(int itemIndex)
        {
            //itemIndex = index of item which is going to be removed
            //get item from binding list at itemIndex position
            T deletedItem = this.Items[itemIndex];

            if (BeforeRemove != null)
            {
                //raise event containing item which is going to be removed
                BeforeRemove(deletedItem);
            }

            //remove item from list
            base.RemoveItem(itemIndex);
        }

        public delegate void ElementRemovedHandler(T deletedItem);
        public event ElementRemovedHandler BeforeRemove;
    }
}
