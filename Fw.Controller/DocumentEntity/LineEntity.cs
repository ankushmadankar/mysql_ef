﻿using Fw.Controller.Base;
using Fw.Controller.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Fw.Controller.DocumentEntity
{
    public class LineEntity : EntityObject, ILineEntity
    {
        private int _HeaderId;
        private readonly HeaderEntity<ILineEntity> _HeaderEntity;

        public LineEntity(string tableName, EntityManager entityManger, HeaderEntity<ILineEntity> headerEntity)
            : base(tableName, entityManger)
        {
            _HeaderEntity = headerEntity;
        }

        public LineEntity(string tableName, EntityManager entityManger, HeaderEntity<ILineEntity> headerEntity, int id)
            : base(tableName, entityManger, id)
        {
            _HeaderEntity = headerEntity;
        }

        public HeaderEntity<ILineEntity> HeaderEntity
        {
            get
            {
                return _HeaderEntity;
            }
        }

        public int HeaderId
        {
            get
            {
                return _HeaderId;
            }
            set
            {
                if (_HeaderId == value) return;

                NotifyPropertyChanging("HeaderId");
                _HeaderId = value;
                NotifyPropertyChanged("HeaderId");
            }
        }
    }
}
