﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Extentions;
using MySql.Data.MySqlClient;
using System.Data;
using Fw.Controller.Base;
using Fw.Controller.Interfaces;
using System.Windows;
using System.Windows.Media;
using System.Windows.Data;
using System.Windows.Controls;
using Fw.Controller.EntityCommand;

namespace Fw.Controller.DocumentEntity
{
    public class EntityManager
    {
        private readonly ContentControl _EntityForm;
        private readonly System.Windows.Controls.Menu _ActionMenu;

        private NewEntityCommand _NewEntityCommand;
        private EditEntityCommand _EditEntityCommand;
        private SaveEntityCommand _SaveEntityCommand;
        private RefreshEntityCommand _RefreshEntityCommand;
        private DeleteEntityCommand _DeleteEntityCommand;
        private CloseEntityCommand _CloseEntityCommand;

        public event SavingChangesEventHadler SavingChanges;
        public event EndSavingChangesEventHadler EndSavingChanges;
        public event EndCommitChangesEventHadler EndCommitChanges;
        public event EndRollbackChangesEventHadler EndRollbackChanges;
        public event IsSavableEventHandler IsSavable;

        public event ManagerRefreshEventHandler ManagerRefresh;

        private List<EntityObject> _Entities;

        public ContentControl EntityForm
        {
            get
            {
                return _EntityForm;
            }
        }

        public System.Windows.Controls.Menu ActionMenu
        {
            get
            {
                return _ActionMenu;
            }
        }

        public bool IsLoaded
        {
            get
            {
                return (_Entities.Count == 0) ? true : _Entities.All(a => a.IsEntityLoaded);
            }
        }

        [Entity("NewEntityCommand", AttributeType.Other, ShowControl = false)]
        public NewEntityCommand NewEntityCommand
        {
            get
            {
                return (_NewEntityCommand ?? (_NewEntityCommand = new NewEntityCommand(this)));
            }
        }

        [Entity("SaveEntityCommand", AttributeType.Other, ShowControl = false)]
        public SaveEntityCommand SaveEntityCommand
        {
            get
            {
                return (_SaveEntityCommand ?? (_SaveEntityCommand = new SaveEntityCommand(this)));
            }
        }

        [Entity("RefreshEntityCommand", AttributeType.Other, ShowControl = false)]
        public RefreshEntityCommand RefreshEntityCommand
        {
            get
            {
                return (_RefreshEntityCommand ?? (_RefreshEntityCommand = new RefreshEntityCommand(this)));
            }
        }

        [Entity("EditEntityCommand", AttributeType.Other, ShowControl = false)]
        public EditEntityCommand EditEntityCommand
        {
            get
            {
                return (_EditEntityCommand ?? (_EditEntityCommand = new EditEntityCommand(this)));
            }
        }

        [Entity("DeleteEntityCommand", AttributeType.Other, ShowControl = false)]
        public DeleteEntityCommand DeleteEntityCommand
        {
            get
            {
                return (_DeleteEntityCommand ?? (_DeleteEntityCommand = new DeleteEntityCommand(this)));
            }
        }

        [Entity("CloseEntityCommand", AttributeType.Other, ShowControl = false)]
        public CloseEntityCommand CloseEntityCommand
        {
            get
            {
                return (_CloseEntityCommand ?? (_CloseEntityCommand = new CloseEntityCommand(this)));
            }
        }

        public EntityManager()
        {
        }

        public EntityManager(ContentControl window)
        {
            if (window.IsNull())
                throw new ArgumentNullException("Entity form cant be null.");
            _EntityForm = window;
            window.Loaded += new System.Windows.RoutedEventHandler(EntityForm_Loaded);
        }

        public EntityManager(ContentControl form, System.Windows.Controls.Menu actionMenu)
        {
            //    if (form.IsNull())
            //        throw new ArgumentNullException("Entity form cant be null.");

            //    _EntityForm = form;
            //    _ActionMenu = actionMenu;

            //    _EntityForm.Loaded += new System.Windows.RoutedEventHandler(EntityForm_Loaded);
        }

        private void EntityForm_Loaded(object sender, System.Windows.RoutedEventArgs e)
        {
            BindEntities();
            LoadControlsData();
            ApplyTemplate();
        }

        protected void OnEndCommitChanges()
        {
            EndCommitChangesEventHadler _handler = EndCommitChanges;
            //
            if (_handler != null) _handler(this);
        }

        protected void OnSavingChanges()
        {
            SavingChangesEventHadler _handler = SavingChanges;
            //
            if (_handler != null) _handler(this);
        }

        protected void OnEndSavingChanges(EndSavingChangesEventArgs e)
        {
            EndSavingChangesEventHadler _handler = EndSavingChanges;
            //
            if (_handler != null) _handler(this, e);
        }

        protected void OnEndRollbackChanges()
        {
            EndRollbackChangesEventHadler _handler = EndRollbackChanges;
            //
            if (_handler != null) _handler(this);

        }

        protected bool OnIsSavable(NonSavableReasons nonSavableReasons)
        {
            IsSavableEventHandler _handler = IsSavable;

            if (_handler != null)
                _handler(this, nonSavableReasons);

            ValidateEntities(nonSavableReasons);

            return nonSavableReasons.Reasons.IsNull() || nonSavableReasons.Reasons.Count == 0;
        }

        protected void OnManagerRefresh()
        {
            if (ManagerRefresh != null)
                ManagerRefresh(this);
        }

        public IEnumerable<EntityObject> Entities
        {
            get { return _Entities ?? (_Entities = new List<EntityObject>()); }
        }

        public bool SaveChanges()
        {
            if (Entities.Count() == 0)
                throw new InvalidOperationException("No entity added before calling save operation.");

            var nonSavableReasons = new NonSavableReasons { Reasons = new Dictionary<string, string>() };

            if (!OnIsSavable(nonSavableReasons))
            {
                string validationString = String.Join(Environment.NewLine, nonSavableReasons.Reasons.Select(a => a.Value).ToArray());
                System.Windows.MessageBox.Show(validationString, @"Error", System.Windows.MessageBoxButton.OK, System.Windows.MessageBoxImage.Error);
                return false;
            }
            var conn = new MySqlConnection(new Data.MySqlConnectionManager().ConnectionString);

            conn.Open();

            var transaction = conn.BeginTransaction();

            try
            {
                OnSavingChanges();

                foreach (var entity in Entities)
                {
                    if ((entity is ILineEntity && !(entity as ILineEntity).HeaderEntity.IsNull()) 
                        || ((entity is IForeignRelation) && !(entity as IForeignRelation).PrimaryEntity.IsNull())) continue;

                    entity.Save(conn, transaction);
                }

                OnEndSavingChanges(new EndSavingChangesEventArgs());

                transaction.Commit();

                OnEndCommitChanges();

                MessageBox.Show("Entity saved sucessfully.", "Information", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch
            {
                transaction.Rollback();

                OnEndRollbackChanges();

                throw;
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

            return true;
        }

        public void NewEntity()
        {
            if (EntityForm.IsNull()) return;

            var entityForm = Activator.CreateInstance(EntityForm.GetType());

            //var entityForm = Activator.CreateInstance(EntityForm.GetType(), );

            if (entityForm is System.Windows.Window)
                (entityForm as System.Windows.Window).Show();
        }

        public bool AddEntity(EntityObject entity)
        {
            if (IsEntityAlreadyAdded(entity)) return false;

            _Entities.Add(entity);

            return true;
        }

        public bool RemoveEntity(EntityObject entity)
        {
            if (_Entities.Count == 0) return false;

            if (_Entities.Contains(entity))
                _Entities.Remove(entity);
            else
                return false;

            return true;
        }

        public bool IsEntityAlreadyAdded(EntityObject entity)
        {
            if (entity.IsNull())
                throw new ArgumentNullException("Entity is null");

            return Entities.Any(a => a.Equals(entity));
        }

        public void ValidateEntities(NonSavableReasons nonSavableReasons)
        {
            if (Entities.Count() == 0) return;

            foreach (var entity in Entities)
                entity.CanSave(nonSavableReasons);
        }

        private void SaveInternal()
        {
            var conn = new MySqlConnection(new Data.MySqlConnectionManager().ConnectionString);

            try
            {
                conn.Open();

                var transaction = conn.BeginTransaction();

                transaction.Commit();
            }
            catch { }
            finally
            {
                if (conn.State == ConnectionState.Open)
                    conn.Close();
            }

        }

        public void BindEntities()
        {
            if (EntityForm.IsNull())
                throw new NotSupportedException("Entity form is null.");

            foreach (var control in ControlsOfType<DependencyObject>(EntityForm).Where(a => (a is IFwControl) && !(a as IFwControl).SourcePropertyName.IsNull()))
            {
                var defaultBinding = GetDefaultBinding((control as IFwControl).SourcePropertyName, (control as IFwControl).BindingMode);
                //
                (control as FrameworkElement).SetBinding((control as IFwControl).ControlProperty, defaultBinding);
            }

            if (ActionMenu.IsNull()) return;

            foreach (var item in ActionMenu.Items.Cast<MenuItem>().Where(a => !a.Name.IsNull()))
            {
                switch (item.Name)
                {
                    case "btnNewMenu":
                        item.SetBinding(MenuItem.CommandProperty, GetDefaultBinding("NewEntityCommand", BindingMode.Default, true));
                        break;
                    case "btnSaveMenu":
                        item.SetBinding(MenuItem.CommandProperty, GetDefaultBinding("SaveEntityCommand", BindingMode.Default, true));
                        break;
                    case "btnDeleteMenu":
                        item.SetBinding(MenuItem.CommandProperty, GetDefaultBinding("DeleteEntityCommand", BindingMode.Default, true));
                        break;
                    case "btnRefreshMenu":
                        item.SetBinding(MenuItem.CommandProperty, GetDefaultBinding("RefreshEntityCommand", BindingMode.Default, true));
                        break;
                    case "btnCloseMenu":
                        item.SetBinding(MenuItem.CommandProperty, GetDefaultBinding("CloseEntityCommand", BindingMode.Default, true));
                        break;
                }
            }
        }

        public void LoadControlsData()
        {
            foreach (var combo in Controls(EntityForm).OfType<Fw.Controller.ControlsWpf.FwComboBox>().Where(a => !string.IsNullOrEmpty(a.SourceQuery) || a.PickListMasterID > 0).ToArray())
                combo.RefreshDataContext();

            //foreach (CustomDataGridView grid in this.Controls.OfType<CustomDataGridView>().Where(a => a.GridDataBindingType != CustomDataGridView.BindingType.None).ToArray())
            //    grid.RefreshGrid();
        }

        public void RefreshEntities()
        {
            foreach (var entity in Entities.ToArray())
                entity.RefreshEntity();

            OnManagerRefresh();
        }

        private IEnumerable<T> ControlsOfType<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                foreach (var element in LogicalTreeHelper.GetChildren(depObj))
                {
                    if (element != null && element is T)
                    {
                        yield return (T)element;
                    }

                    if (element is DependencyObject)
                        foreach (T childOfChild in ControlsOfType<T>(element as DependencyObject))
                        {
                            yield return childOfChild;
                        }
                }

                //for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                //{
                //    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                //    if (child != null && child is T)
                //    {
                //        yield return (T)child;
                //    }

                //    foreach (T childOfChild in ControlsOfType<T>(child))
                //    {
                //        yield return childOfChild;
                //    }
                //}
            }
        }

        private IEnumerable<IFwControl> Controls(DependencyObject depObj)
        {
            if (depObj != null)
            {
                for (int i = 0; i < System.Windows.Media.VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    var child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is IFwControl)
                    {
                        yield return (IFwControl)child;
                    }

                    foreach (var childOfChild in Controls(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        /// Find ValidationError for binded controls by using IsValid method for dependencyObject
        /// </summary>
        /// <param name="parent"></param>
        /// <returns></returns>
        public bool IsValid(DependencyObject parent)
        {
            if (Validation.GetHasError(parent))
                return false;

            // Validate all the bindings on the children
            for (int i = 0; i != VisualTreeHelper.GetChildrenCount(parent); ++i)
            {
                DependencyObject child = VisualTreeHelper.GetChild(parent, i);
                if (!IsValid(child)) { return false; }
            }

            return true;
        }

        /// <summary>
        /// Get binding for control and class.
        /// </summary>
        /// <param name="propertyPath">Class property name.</param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private Binding GetDefaultBinding(string propertyPath, BindingMode mode = BindingMode.TwoWay, bool isAction = false)
        {
            EntityObject entity = Entities.FirstOrDefault();

            if (propertyPath.Split('.').Count() > 1)
                entity = Entities.Where(a => a.ToString().Split('.').Last() == propertyPath.Split('.').First()).First();

            Binding binding = new Binding();
            //
            if (isAction)
                binding.Source = this;
            else
                binding.Source = entity;
            //
            binding.Path = new PropertyPath(propertyPath.Split('.').Last());
            binding.Mode = mode;
            binding.UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged;
            binding.ValidatesOnDataErrors = true;
            //
            return binding;
        }

        private void ApplyTemplate()
        {
            //foreach (var control in ControlsOfType<ComboBox>(_EntityForm))
            //{
            //    control.Style = _EntityForm.Resources["MetroComboBox"] as Style;
            //}
        }
    }
}
