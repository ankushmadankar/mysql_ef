﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using System.Windows.Input;
using Fw.Controller.Extentions;
using Fw.Controller.DocumentEntity;

namespace Fw.Controller.EntityCommand
{
    public class NewEntityCommand : ICommand
    {
        private EntityManager _EntityManager;

        public NewEntityCommand(EntityManager entity)
        {
            if (entity.IsNull())
                throw new ArgumentNullException("Entity Manager object can't be null.");

            _EntityManager = entity;
        }

        public bool CanExecute(object parameter)
        {
            return _EntityManager.IsLoaded;
        }

        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            _EntityManager.NewEntity();
        }
    }
}
