﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using Fw.Controller.Extentions;
using System.Windows.Input;
using Fw.Controller.DocumentEntity;
using Fw.Controller.Base;

namespace Fw.Controller.EntityCommand
{
    public class SaveEntityCommand : ICommand
    {
        private readonly EntityManager _EntityManager;

        public SaveEntityCommand(EntityManager entityManger)
        {
            if (entityManger.IsNull())
                throw new ArgumentNullException("Entity manager object can't be null.");

            _EntityManager = entityManger;
        }

        public bool CanExecute(object parameter)
        {
            foreach (var entity in _EntityManager.Entities)
                if (entity.GetSavableProperties().Count() > 0)
                    return true;

            return false;
        }

        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            _EntityManager.SaveChanges();
        }
    }
}
