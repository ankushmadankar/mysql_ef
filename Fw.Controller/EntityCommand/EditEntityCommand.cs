﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using System.Windows.Input;
using Fw.Controller.Extentions;
using Fw.Controller.DocumentEntity;

namespace Fw.Controller.EntityCommand
{
    public class EditEntityCommand : ICommand
    {
        private EntityManager _EntityManager;

        public EditEntityCommand(EntityManager entityManger)
        {
            if (entityManger.IsNull())
                throw new ArgumentNullException("Entity object can't be null.");

            _EntityManager = entityManger;
        }

        public bool CanExecute(object parameter)
        {
            foreach (var entity in _EntityManager.Entities)
                if (entity.IsEntityLoaded)
                    return false;

            return true;
        }

        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            foreach (var entity in _EntityManager.Entities)
                entity.LoadEntity();
        }
    }
}
