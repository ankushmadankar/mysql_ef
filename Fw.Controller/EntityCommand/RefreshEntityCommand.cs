﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fw.Controller.Interfaces;
using Fw.Controller.Extentions;
using System.Windows.Input;
using Fw.Controller.DocumentEntity;

namespace Fw.Controller.EntityCommand
{
    public class RefreshEntityCommand : ICommand
    {
        private EntityManager _EntityManager;

        public RefreshEntityCommand(EntityManager entityManager)
        {
            if (entityManager.IsNull())
                throw new ArgumentNullException("Entity object can't be null.");

            _EntityManager = entityManager;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event System.EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }

        public void Execute(object parameter)
        {
            _EntityManager.RefreshEntities();
        }
    }
}
